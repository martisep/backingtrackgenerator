#include "rockstudycorpusloader.h"

#include <fstream>
#include "songPart.h"

void RockStudyCorpusLoader::loadCorpus(std::string dir, Corpus* corpus, ParamObject *paramObject)
{
    std::vector<std::string> files = std::vector<std::string>();
    getdir(dir,files);

    for (std::string file : files) {
        rules.clear();
        // get grammar rules from current file
        findRules(dir + "/" + file);

        Sentence sentence;

        //find rule for S (song) and recursively expand to terminals Vr, Ch, Br,...
        expandNonterminal("S", sentence, corpus);

        if(!sentence.empty() && sentence.atLeast(paramObject->getMinNumberOfSections()))
            corpus->addSentence(sentence);
    }
}

void RockStudyCorpusLoader::findRules(std::string filePath){
    std::ifstream myFile;

    myFile.open(filePath);
    if(!myFile.is_open()){
        std::cerr << "Unable to open file " << filePath;
        return;
    }

    std::string line;

    while ( getline (myFile,line) )
    {
        std::string firstWord;

        //remove comments
        line = line.substr(0, line.find("%"));

        if(line.empty())
            continue;

        std::stringstream ssline(line);
        ssline >> firstWord;

        //new rule (rules in corpus are in format Word1: $Word2 $Word3 ...)
        if(firstWord.find(':') != std::string::npos){
            std::string leftSide = firstWord.substr(0,firstWord.length()-1);
            rules[leftSide] = line.substr(firstWord.length()+1);
        }
    }
    myFile.close();
}

void RockStudyCorpusLoader::expandNonterminal(std::string nonterminal, Sentence& sentence, Corpus* corpus){
    auto it = rules.find(nonterminal);
    if(it != rules.end()){
        std::stringstream rightSide(it->second);
        std::string word;
        while(rightSide >> word){

            //ignore outstanding chord symbols, only take $ nonterminals
            if(word.compare(0,1,"$") != 0){
                continue;
            }

            //number of repetition of section, specified by suffix "*N" (default is 1)
            int repeat = 1;

            if(word.find("*") != std::string::npos){
                try{
                    repeat = std::stoi(word.substr(word.find("*")+1));
                }
                catch(const std::invalid_argument&){}
                catch(const std::out_of_range&){}

                word = word.substr(0,word.find("*"));
            }

            //use static function of SongPart to parse string to TSongPart
            SongPart::TSongPart sp = SongPart::parseSongPart(word);

            //if the word is recognized as a legit SongPart, add a new Term to the Sentence being constructed
            if(sp != SongPart::NOPART){
                for(int i = 0; i < repeat; i++){
                    Terminal t = Terminal(std::unique_ptr<SongPart>(new SongPart(sp)));
                    Terminal* ptr = corpus->addTerminal(t);
                    sentence.push_back(ptr);
                }
            } else{
                //else it is a nonterminal representing something else, expand it
                expandNonterminal(word, sentence, corpus);
            }
        }
    }
}

