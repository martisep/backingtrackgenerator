#ifndef ROCKSTUDYSONGPARTSCORPUSLOADER_H
#define ROCKSTUDYSONGPARTSCORPUSLOADER_H

#include "corpusloader.h"
#include "unordered_map"

// implementation of CorpusLoader for parsing Reference Segmentations Corpus
class RockStudyCorpusLoader : public CorpusLoader
{
public:
    RockStudyCorpusLoader(){}
    virtual ~RockStudyCorpusLoader(){}
    virtual void loadCorpus(std::string dir, Corpus* corpus, ParamObject* paramObject);

private:
    //recursively expands given nonterminal acording to deduced grammar rules, gradually constructs new Sentence and saves found terminals to corpus
    void expandNonterminal(std::string nonterminal, Sentence& sentence, Corpus *corpus);

    //finds all rules in one song
    void findRules(std::string s);


    //song is saved in corpus as a set of grammar rules, with section names as nonterminals on the left side and chords / other sections on right side
    std::unordered_map<std::string, std::string> rules;
};

#endif // ROCKSTUDYSONGPARTSCORPUSLOADER_H
