#include "imaginarybookcorpusloader.h"
#include <fstream>
#include <iostream>

void ImaginaryBookCorpusLoader::loadCorpus(std::string dir, Corpus *corpus, ParamObject *paramObject)
{
    std::vector<std::string> files = std::vector<std::string>();
    std::ifstream myFile;
    std::string line;
    getdir(dir,files);

    for (auto& file : files) {
        myFile.open(dir + "/" + file);

        unsigned short measureLength;
        Chord::Tone songKey;
        Chord currentChord;
        Sentence currentSentence;

        //parsing of the file
        if(myFile.is_open()){
            while ( getline (myFile,line) )
            {
                if(line.empty())
                    continue;

                //meter
                if(line.compare(0,6,"(meter") == 0){
                    try{
                        std::stringstream ss(line.substr(6,line.length()-2));
                        unsigned short numerator, denominator;
                        ss >> numerator >> denominator;

                        if(denominator == 0){
                            throw std::invalid_argument("");
                        }
                        measureLength = numerator * 16 / denominator;
                    }
                    catch(const std::invalid_argument&){
                        measureLength = 16;
                    }

                    //only include songs with 4/4 time signature
                    if(measureLength != 16){
                        myFile.close();
                    }
                    continue;
                }
                //key
                if(line.compare(0,4,"(key") == 0){
                    int k = std::stoi(line.substr(4, line.length()-5));
                    // key is noted as number of sharps (#) (positive integer) or flats (b) (negative integer)
                    // adding or removing accidental (# or b) means transposing by fifth (7 halftones)
                    // C is the key with 0 accidentals
                    // OCTAVE*10 (=12*10) is added to prevent negative values for modulo
                    songKey = Chord::Tone((Chord::OCTAVE*10 + (Chord::Tone::C + k*7)) % Chord::OCTAVE);
                    continue;
                }
                //discard other meta information
                if(line.compare(0,1,"(") == 0){ continue; }
                if(line.compare(0,1,")") == 0){ continue; }
                //discard melody information
                if(line.compare(0,1," ") == 0){ continue; }

                //else it is a line of chords
                std::stringstream ss(line);
                std::string word;

                // we cannot directly insert into Measure, because we dont know the length of the chords
                // the measure length is divided equally among the contained chords, so we need to know how many chords there are
                std::vector<Chord> chordsBuffer;
                while(ss >> word){
                    //new measure
                    if(word.compare("|") == 0){
                       std::unique_ptr<Measure> m = std::unique_ptr<Measure>(new Measure(measureLength));

                       //compute chord length as a fraction of the length of a measure
                       for(Chord c : chordsBuffer){
                           m->addChord(c, measureLength / chordsBuffer.size());
                       }
                       Terminal t(std::move(m));

                       currentSentence.push_back(corpus->addTerminal(t));
                       chordsBuffer.clear();
                       continue;
                    }
                    // "/" denotes a continuation of previous chord
                    if(word.compare("/") == 0){
                        chordsBuffer.push_back(currentChord);
                        continue;
                    }
                    // "No Chord", consider it a continuation of previous chord
                    if(word.compare("NC") == 0){
                        chordsBuffer.push_back(currentChord);
                        continue;
                    }

                    currentChord = Chord::parseChord(word, songKey, paramObject);
                    chordsBuffer.push_back(currentChord);
                }
            }
            corpus->addSentence(currentSentence);
            currentSentence = Sentence();
            myFile.close();
        } else std::cout << "Unable to open file";
    }
}




