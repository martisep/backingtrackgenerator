#include <sstream>

#include "sentence.h"

// returns true if Sentence dcontains at least size words
bool Sentence::atLeast(size_t size){
    std::shared_ptr<Word> ptr = this->first;

    while(size > 0){
        if(ptr == nullptr){
            return false;
        }
        ptr = ptr->next;
        size--;
    }
    return true;
}


void Sentence::push_back(Terminal* terminal){
    //empty sentence
    if (first == nullptr){
        first = std::make_shared<Word>(terminal);
        first->next = nullptr;
        first->prev = first->next;

        last = first;
    }
    // append to the end and reconnect pointers
    else{
        if(last.expired()){
            //last could have expired when Sentence was reduced by adding a new AndNonterminal
            last = first;
            while(last.lock()->next != nullptr){
                last = first->next;
            }
        }
        std::shared_ptr<Word> tmp = last.lock();

        //create new Word and connect to the end of the Sentence
        std::shared_ptr<Word> tmp2 = std::make_shared<Word>(terminal);
        tmp2->prev = tmp;
        tmp2->next = nullptr;
        tmp->next = tmp2;

        //reset pointer to last element
        last = tmp2;
    }
}


std::string Sentence::toString() const {
    std::stringstream ss;
    auto tmp = first;

    while(tmp != nullptr){
        ss << tmp->getSymbol()->toString() << " ";
        tmp = tmp->next;
    }
    return ss.str();
}
