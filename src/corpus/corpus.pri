DEPENDPATH += $$PWD
INCLUDEPATH += $$PWD


    

HEADERS += \
    $$PWD/sentence.h \
    $$PWD/corpusloader.h \
    $$PWD/imaginarybookcorpusloader.h \
    $$PWD/irbcorpusloader.h \
    $$PWD/refsegcorpusloader.h \
    $$PWD/rockstudycorpusloader.h \
    $$PWD/corpus.h

SOURCES += \
    $$PWD/sentence.cpp \
    $$PWD/imaginarybookcorpusloader.cpp \
    $$PWD/irbcorpusloader.cpp \
    $$PWD/rockstudycorpusloader.cpp \
    $$PWD/refsegcorpusloader.cpp \
    $$PWD/corpus.cpp \
    $$PWD/corpusloader.cpp
