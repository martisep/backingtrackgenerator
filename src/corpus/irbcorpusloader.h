#ifndef IRBCORPUSLOADER_H
#define IRBCORPUSLOADER_H

#include "corpusloader.h"
#include "measure.h"

// implementation of CorpusLoader for parsing iRb Corpus
class iRbCorpusLoader : public CorpusLoader
{  

public:
    iRbCorpusLoader(){}
    virtual ~iRbCorpusLoader(){}
    virtual void loadCorpus(std::string dir, Corpus* corpus, ParamObject* paramObject);
private:
    //creates Chord (in Roman-Numeral representation relative to song key) from input string  and adds it to currentMeasure
    void parseChord(std::string s, Chord::Tone key, Measure& currentMeasure, ParamObject *paramObject);

    //parse string representing key of the song
    Chord::Tone parseKey(std::string s);
};

#endif // IRBCORPUSLOADER_H
