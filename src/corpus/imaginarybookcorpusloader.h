#ifndef IMAGINARYBOOKCORPUSLOADER_H
#define IMAGINARYBOOKCORPUSLOADER_H

#include "corpusloader.h"
#include "chord.h"
#include "measure.h"

// implementation of CorpusLoader for parsing Imaginary Book corpus
class ImaginaryBookCorpusLoader : public CorpusLoader
{
public:
    ImaginaryBookCorpusLoader(){}
    virtual ~ImaginaryBookCorpusLoader(){}
    virtual void loadCorpus(std::string dir, Corpus* corpus, ParamObject* paramObject);
};

#endif // IMAGINARYBOOKCORPUSLOADER_H
