#include "corpus.h"
#include <iostream>
#include <fstream>
#include "terminal.h"

std::unique_ptr<Terminal> Corpus::beginningOrEndOfSentence = std::unique_ptr<Terminal>(new Terminal());


Terminal* Corpus::addTerminal(Terminal& t) {
    //check if pointer to this term is already in set, return it
    // or create new pointer, insert and return
    for (const auto& term : terminals) {
        if(term->equal(t)){
            return term.get();
        }
    }
    std::shared_ptr<Terminal> newTerminal = std::make_shared<Terminal>(std::move(t));
    terminals.insert(newTerminal);
    return newTerminal.get();
}

void Corpus::writeToFile(std::string fileName){
    std::ofstream of;

    of.open(fileName);
    if(!of.is_open())
        return;

    for (const auto& sentence : this->sentences) {
        of << sentence.toString() << std::endl;
        }
    of.close();
}


