#include "refsegcorpusloader.h"

#include "fstream"
#include "iostream"
#include "songPart.h"


void RefSegCorpusLoader::loadCorpus(std::string dir, Corpus* corpus, ParamObject *paramObject)
{
    std::vector<std::string> files = std::vector<std::string>();
    getdir(dir,files);

    std::ifstream myFile;
    for (std::string file : files) {
        myFile.open(dir + "/" + file);
        if(!myFile.is_open()){
            std::cerr << "Unable to open file " << file;
            return;
        }

        Sentence sentence;
        std::string line;

        while ( getline (myFile,line) )
        {
            std::stringstream ssline(line);
            std::string word;

            //dump first two words (information about timing)
            ssline >> word >> word >> word;

            //use static function of SongPart to parse string to TSongPart
            SongPart::TSongPart type = SongPart::parseSongPart(word);

            //if the name of the song section is not recognized, ignore it
            if(type == SongPart::NOPART){
                continue;
            }

            Terminal t(std::unique_ptr<SongPart>(new SongPart(type)));
            Terminal* ptr = corpus->addTerminal(t);
            sentence.push_back(ptr);

        }
        myFile.close();

        //add sentence to corpus
        if(!sentence.empty() && sentence.atLeast(paramObject->getMinNumberOfSections()))
            corpus->addSentence(sentence);
    }
}
