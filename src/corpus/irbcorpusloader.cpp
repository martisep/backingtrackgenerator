#include "irbcorpusloader.h"

#include <fstream>
#include <iostream>


void iRbCorpusLoader::loadCorpus(std::string dir, Corpus* corpus, ParamObject *paramObject)
{
    std::vector<std::string> files = std::vector<std::string>();
    std::ifstream myFile;
    std::string line;

    getdir(dir,files);

    for (auto& file : files) {
        myFile.open(dir + "/" + file);

        unsigned short measureLength;
        Chord::Tone songKey;
        Measure currentMeasure;
        Sentence currentSentence;

        //parsing the file
        if(myFile.is_open()){
            while ( getline (myFile,line) )
            {
                if(line.empty())
                    continue;

                //comment
                if(line.compare(0,1,"!") == 0){
                    continue;
                }
                //sections list
                if(line.compare(0,3,"*>[") == 0){
                    continue;
                }
                //new section or end of song
                if((line.compare(0,2,"*>") == 0) || (line.compare(0,2,"*-") == 0)){
                    //flush current sentence
                    if(!currentSentence.empty() && currentSentence.atLeast(paramObject->getMinLengthOfChordProgression())){
                        corpus->addSentence(currentSentence);
                        currentSentence = Sentence();
                    }
                    continue;
                }
                //measure length
                if(line.compare(0,2,"*M") == 0){
                    std::string tmp = line.substr(2);
                    size_t slash = tmp.find("/");
                    try{
                        unsigned short numerator = std::stoi(tmp.substr(0,slash));
                        unsigned short denominator = std::stoi(tmp.substr(slash+1));
                        if(denominator == 0){
                            throw std::invalid_argument("");
                        }
                        measureLength = 16 * numerator / denominator;
                    }
                    catch(const std::invalid_argument&){
                        measureLength = 16;
                    }
                    //only include songs with 4/4 time signature
                    if(measureLength != 16){
                        myFile.close();
                        continue;
                    }

                    //discards currentMeasure (it is empty at this point anyway)
                    currentMeasure = Measure(measureLength);
                    continue;
                }

                // key
                if(line.compare(0,1,"*") == 0){
                    songKey = parseKey(line.erase(0,1));
                    continue;
                }

                //barline
                if(line.compare(0,1,"=") == 0){
                    //flush current measure and add it to currentSentence
                    Terminal t = Terminal(std::unique_ptr<Measure>(new Measure(currentMeasure)));

                    currentSentence.push_back(corpus->addTerminal(t));
                    currentMeasure = Measure(measureLength);

                    //double barline = new section
                    if(line.compare(0,2,"==") == 0){
                        //flush current sentence
                        if(!currentSentence.empty() && currentSentence.atLeast(paramObject->getMinLengthOfChordProgression())){
                            corpus->addSentence(currentSentence);
                            currentSentence = Sentence();
                        }
                    }
                    continue;
                }

                //else it is a chord
                parseChord(line, songKey, currentMeasure, paramObject);
            }
            myFile.close();
        } else std::cout << "Unable to open file";
    }
}



//parse string representing key of the song
Chord::Tone iRbCorpusLoader::parseKey(std::string s){
    int toneIndex = 0;
    char c = s.at(0);

    //small characters denote minor scale, but we convert them to their major counterparts (a = A minor = C major)
    switch(c){
    case 'A':
        toneIndex = 0;
        break;
    case 'g':
        toneIndex = 1;
        break;
    case 'B':
        toneIndex = 2;
        break;
    case 'a':
    case 'C':
        toneIndex = 3;
        break;
    case 'b':
    case 'D':
        toneIndex = 5;
        break;
    case 'c':
        toneIndex = 6;
        break;
    case 'E':
        toneIndex = 7;
        break;
    case 'd':
    case 'F':
        toneIndex = 8;
        break;
    case 'e':
    case 'G':
        toneIndex = 10;
        break;
    case 'f':
        toneIndex = 11;
        break;
    }
    s.erase(s.begin());

    //# denotes raising the key by semitone
    if(s.compare(0,1,"#") == 0){
        toneIndex = (++toneIndex) % Chord::OCTAVE;
    }
    //- denotes lowering the key by semitone
    else if(s.compare(0,1,"-") == 0){
        // adding one octave for correct modulo (--toneIndex can be -1)
        toneIndex = (--toneIndex + Chord::OCTAVE) % Chord::OCTAVE;
    }
    return Chord::Tone(toneIndex);
}

//creates Chord (in Roman-Numeral representation relative to song key) from input string  and adds it to currentMeasure
void iRbCorpusLoader::parseChord(std::string s, Chord::Tone key, Measure& currentMeasure, ParamObject* paramObject){

    //length is measured in sixteenth notes
    unsigned short length;

    try {
        int duration = std::stoi(s.substr(0,1));
        // 1 = whole note, 2 = half note, 4 = quarter note, 8 = eighth note
        // convert to number of sixteenth notes
        length = 16 / duration;
        s.erase(s.begin());

    } catch(std::invalid_argument){
        //default is whole note
        length = 16;
    }

    //dotted note (increases the length by half)
    if(s.compare(0,1,".") == 0){
        length += length/2;
        s.erase(s.begin());
    }

    // use static method of Chord to get Chord from the string
    Chord c = Chord::parseChord(s, key, paramObject);

    currentMeasure.addChord(c,length);
}
