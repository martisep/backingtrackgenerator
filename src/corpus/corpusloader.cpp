#include "corpusloader.h"

#include <dirent.h>
#include <iostream>

int CorpusLoader::getdir(std::string dir, std::vector<std::string> &files)
{
    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir(dir.c_str())) == NULL) {
        std::cout << "Error(" << errno << ") opening " << dir << std::endl;
        return errno;
    }

    while ((dirp = readdir(dp)) != NULL) {
        if(std::string(dirp->d_name).compare(0,1,".") != 0){
            files.push_back(std::string(dirp->d_name));
        }
    }
    closedir(dp);
    return 0;
}
