#ifndef REFSEGCORPUS_H
#define REFSEGCORPUS_H

#include "corpusloader.h"

// implementation of CorpusLoader for parsing Reference Segmentations Corpus
class RefSegCorpusLoader : public CorpusLoader
{

public:
    RefSegCorpusLoader(){}
    virtual ~RefSegCorpusLoader(){}
    virtual void loadCorpus(std::string dir, Corpus* corpus, ParamObject* paramObject);
};

#endif // REFSEGCORPUS_H
