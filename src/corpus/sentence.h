#ifndef SENTENCE_H
#define SENTENCE_H
#include <memory>

#include "symbol.h"
#include "terminal.h"


//custom linked list representing a sentence from a corpus
class Sentence
{
public:
    //node of the list
    struct Word{
        // one pointer is weak_ptr to prevent cyclic dependency and memory leaks
        std::shared_ptr<Word> next;
        std::weak_ptr<Word> prev;

        Word(Symbol* s) : symbol(s){}
        std::string toString(){
            return symbol->toString();
        }
        Symbol* getSymbol(){
            return symbol;
        }
        void setSymbol(Symbol* s){
            symbol = s;
        }
    private:
        //pointer to underlaying Symbol (Terminal or AndNonterminal)
        Symbol* symbol;
    };

    Sentence(){
        first = nullptr;
        last = first;
    }
    ~Sentence(){}

    bool empty() const {
        return (first == nullptr);
    }

    //returns true if the Sentence is at least "size" words long
    bool atLeast(size_t size);

    //adds new Word with given terminal at the end of the Sentence
    void push_back(Terminal *terminal);

    std::string toString() const;

    //pointer to last Word can be invalidated (during reducing by new AndNonterminal) and reset to valid position
    std::shared_ptr<Word> first;
    std::weak_ptr<Word> last;
};

#endif // SENTENCE_H
