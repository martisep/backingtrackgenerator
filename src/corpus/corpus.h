#ifndef CORPUS_H
#define CORPUS_H
#include <string>
#include <vector>
#include <unordered_set>
#include "sentence.h"
#include "grammar.h"


//data class representing the learning corpus
class Corpus
{
public:
    //special symbol denoting beginning or end of sentence (no need to differentiate)
    static std::unique_ptr<Terminal> beginningOrEndOfSentence;

    Corpus(){}
    ~Corpus(){}

    const std::vector<Sentence>& getSentences() const {return sentences;}
    const std::unordered_set<std::shared_ptr<Terminal>>& getTerminals() const {return terminals;}

    //returns pointer to the newly added Term (or to existing one if duplicite)
    Terminal* addTerminal(Terminal& t);

    void addSentence(Sentence s){ sentences.push_back(s); }
    bool isEmpty(){
        return sentences.empty();
    }

    void writeToFile(std::string fileName);
private:
    //representation of sentences in the corpus using custom constainer Sentence
    std::vector<Sentence> sentences;
    //terminals are later shared with Grammar
    std::unordered_set<std::shared_ptr<Terminal>> terminals;

};

#endif // CORPUS_H
