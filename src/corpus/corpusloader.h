#ifndef CORPUSLOADER_H
#define CORPUSLOADER_H

#include <memory>
#include "corpus.h"
#include "paramobject.h"

//abstract factory class for loading corpus form text files
class CorpusLoader
{
public:
    CorpusLoader(){}
    virtual ~CorpusLoader(){}
    //adds sentences created fro files in dir to given corpus
    virtual void loadCorpus(std::string dir, Corpus* corpus, ParamObject* paramObject) = 0;

protected:
    //gets vector of names of files in dir
    int getdir(std::string dir, std::vector<std::string> &files);
};

#endif // CORPUSLOADER_H
