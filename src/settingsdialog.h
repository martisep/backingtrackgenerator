#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include "accomplib.h"
#include "paramobject.h"
#include <map>
#include <memory>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    // constructs new SettingsDialog and sets default values using paramObject
    explicit SettingsDialog(ParamObject* paramObject, AccompLib::TGenre genre, QWidget *parent = 0);
    // returns paramObject created from user settings
    std::unique_ptr<ParamObject> getParamObject();
    ~SettingsDialog();

private slots:
    // button 'Default values'
    void on_pushButton_clicked();
    // sets default bass weights for given genre
    void setDefaultBassWeights(ParamObject* paramObject);

private:
    AccompLib::TGenre genre;
    Ui::SettingsDialog *ui;
};

#endif // SETTINGSDIALOG_H
