/*
 * QtPlaySMF
 *  Part of QtMidi (http://github.com/waddlesplash/qtmidi).
 *
 * Copyright (c) 2003-2012 by David G. Slomin
 * Copyright (c) 2012 WaddleSplash
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall
 * be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
*/

#include <stdio.h>
#include <QThread>
#include <QElapsedTimer>
#include <QCoreApplication>
#include <QMidiOut.h>
#include <QMidiFile.h>

class MidiPlayer : public QThread
{
    Q_OBJECT
public:
    MidiPlayer(QMidiFile* file, QMidiOut* out)
    { midi_file = file; midi_out = out; }

private:
    QMidiEvent* midi_file_event;
    QMidiFile* midi_file;
    QMidiOut* midi_out;

protected:
    void run()
    {
        QElapsedTimer t;
        t.start();

        QList<QMidiEvent*>* events = midi_file->events();
        for(int i = 0; i < events->count(); i++)
        {
            midi_file_event = events->at(i);
            if (midi_file_event->type() != QMidiEvent::Meta)
            {
                qint64 event_time = midi_file->timeFromTick(midi_file_event->tick()) * 1000;
                /*if(event_time < 20000)
                    continue;
                */
                qint32 waitTime = event_time - t.elapsed(); // - 20000;
                if(waitTime > 0) {
                    msleep(waitTime);
                }
                handleEvent();
            }
        }

        midi_out->disconnect();
    }
private slots:
    void handleEvent()
    {
        if (midi_file_event->type() == QMidiEvent::SysEx)
        { // TODO: sysex
        }
        else
        {
            qint32 message = midi_file_event->message();
            midi_out->sendMsg(message);
        }
    }
};

static void usage(char *program_name)
{
    fprintf(stderr, "Usage: %s -p<port> <MidiFile>\n\n", program_name);
    fprintf(stderr, "Ports:\nID	Name\n----------------\n");
    QMap<QString,QString> vals = QMidiOut::devices();
    foreach(QString key,vals.keys())
    {
        QString value = vals.value(key);
        fprintf(stderr,key.toUtf8().constData());
        fprintf(stderr,"	");
        fprintf(stderr,value.toUtf8().constData());
        fprintf(stderr,"\n");
    }
    exit(1);
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc,argv);

    QString filename = "the_entertainer.mid";
    QString midiOutName = "0";
    QMidiFile* midi_file = new QMidiFile();

    /*for(int i = 1; i < argc; i++)
    {
        QString curArg(argv[i]);
        if((curArg == "--help") || (curArg == "-h") || (curArg == "/?") || (curArg == "/help"))
        { usage(argv[0]); }
        else if(curArg.startsWith("-p"))
        {
            midiOutName = curArg.mid(2);
        }
        else if(filename == "")
        { filename = argv[i]; }
        else
        { usage(argv[0]); }
    }

    if((filename == "") || (midiOutName == ""))
    {
        usage(argv[0]);
    }*/
    //midi_file->load(filename);


    midi_file->setResolution(96);

    int track = midi_file->createTrack();
    midi_file->createNote(track, midi_file->tickFromBeat(0) , midi_file->tickFromBeat(8) ,0,40,100,100);
    midi_file->createNote(track, midi_file->tickFromBeat(1) , midi_file->tickFromBeat(8) ,0,44,100,100);
    midi_file->createNote(track, midi_file->tickFromBeat(2) , midi_file->tickFromBeat(8) ,0,47,100,100);
    midi_file->createNote(track, midi_file->tickFromBeat(3) , midi_file->tickFromBeat(8) ,0,52,100,100);
    midi_file->createTempoEvent(track,midi_file->tickFromBeat(8),200);
    midi_file->createNote(track, midi_file->tickFromBeat(8) , midi_file->tickFromBeat(16) ,0,42,100,100);
    midi_file->createNote(track, midi_file->tickFromBeat(9) , midi_file->tickFromBeat(16) ,0,46,100,100);
    midi_file->createNote(track, midi_file->tickFromBeat(10) , midi_file->tickFromBeat(16) ,0,49,100,100);
    midi_file->createNote(track, midi_file->tickFromBeat(11) , midi_file->tickFromBeat(16) ,0,54,100,100);

    int track2 = midi_file->createTrack();
    midi_file->createProgramChangeEvent(track2,0,1,25);
    midi_file->createNote(track2, midi_file->tickFromBeat(8) , midi_file->tickFromBeat(18) ,1,54,100,100);
    midi_file->createNote(track2, midi_file->tickFromBeat(9) , midi_file->tickFromBeat(16) ,1,58,100,100);
    midi_file->createNote(track2, midi_file->tickFromBeat(10) , midi_file->tickFromBeat(16) ,1,61,100,100);
    midi_file->createNote(track2, midi_file->tickFromBeat(11) , midi_file->tickFromBeat(16) ,1,66,100,100);

    int trackB = midi_file->createTrack();
    midi_file->createProgramChangeEvent(trackB,0,2,34);
    midi_file->createNote(trackB, midi_file->tickFromBeat(0) , midi_file->tickFromBeat(1) ,2,28,100,100);
    midi_file->createNote(trackB, midi_file->tickFromBeat(1) , midi_file->tickFromBeat(2) ,2,32,100,100);
    midi_file->createNote(trackB, midi_file->tickFromBeat(2) , midi_file->tickFromBeat(3) ,2,35,100,100);
    midi_file->createNote(trackB, midi_file->tickFromBeat(3) , midi_file->tickFromBeat(4) ,2,40,100,100);
    midi_file->createNote(trackB, midi_file->tickFromBeat(4) , midi_file->tickFromBeat(5) ,2,35,100,100);


    int track3 = midi_file->createTrack();
    midi_file->createNote(track3, midi_file->tickFromBeat(0) , midi_file->tickFromBeat(16) ,9,36,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(1) , midi_file->tickFromBeat(16) ,9,40,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(2) , midi_file->tickFromBeat(16) ,9,36,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(3) , midi_file->tickFromBeat(16) ,9,40,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(4) , midi_file->tickFromBeat(16) ,9,36,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(5) , midi_file->tickFromBeat(16) ,9,40,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(6) , midi_file->tickFromBeat(16) ,9,36,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(7) , midi_file->tickFromBeat(16) ,9,40,100,100);

    midi_file->createNote(track3, midi_file->tickFromBeat(0) , midi_file->tickFromBeat(1) ,9,46,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(0.5) , midi_file->tickFromBeat(16) ,9,42,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(1) , midi_file->tickFromBeat(16) ,9,42,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(1.5) , midi_file->tickFromBeat(16) ,9,42,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(2) , midi_file->tickFromBeat(16) ,9,42,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(2.5) , midi_file->tickFromBeat(16) ,9,42,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(3) , midi_file->tickFromBeat(16) ,9,42,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(3.5) , midi_file->tickFromBeat(16) ,9,42,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(4) , midi_file->tickFromBeat(16) ,9,42,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(5) , midi_file->tickFromBeat(16) ,9,42,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(6) , midi_file->tickFromBeat(16) ,9,42,100,100);
    midi_file->createNote(track3, midi_file->tickFromBeat(7) , midi_file->tickFromBeat(16) ,9,42,100,100);





    QMidiOut* midi_out = new QMidiOut();
    midi_out->connect(midiOutName);

    MidiPlayer* p = new MidiPlayer(midi_file,midi_out);
    QObject::connect(p,SIGNAL(finished()),&a,SLOT(quit()));
    p->start();

    return a.exec();
}
#include "playsmf.moc"
