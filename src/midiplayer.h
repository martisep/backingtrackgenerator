#ifndef MIDIPLAYER_H
#define MIDIPLAYER_H

#include <stdio.h>
#include <QThread>
#include <QMutex>
#include <QMutexLocker>
#include <QElapsedTimer>
#include <QCoreApplication>
#include <QMidiOut.h>
#include <QMidiFile.h>
#include <memory>

class MidiPlayer : public QThread
{
    Q_OBJECT
public:
    MidiPlayer(){}
    MidiPlayer(QMidiFile* file, QMidiOut* out, int from, int repeatFrom, int repeatTo, int repeatTimes)
        : midi_file(file),
          midi_out(out),
          from(from),
          repeatFrom(repeatFrom),
          repeatTo(repeatTo),
          repeatTimes(repeatTimes),
          currentBeat(-1),
          stopped(false)
    {}

    ~MidiPlayer(){}

public slots:
    void stop();
private slots:
    void handleEvent(QMidiEvent* midi_file_event);
signals:
    void newBeat(int beat);   
private:
    void run();

    // file to be played
    QMidiFile* midi_file;
    // midi output to be used
    QMidiOut* midi_out;

    //beat from which the song should be played
    int from;
    // beginning of repetition
    int repeatFrom;
    // end of repetition
    int repeatTo;
    // how many times should the section be repeated
    int repeatTimes;
    //currently played beat
    int currentBeat;

    // mutex lock for 'stopped'
    QMutex mutex;
    // indicator whether the user pressed 'Stop'
    bool stopped;

};

#endif // MIDIPLAYER_H
