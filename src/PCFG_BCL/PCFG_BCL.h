#ifndef PCFG_BCL_H
#define PCFG_BCL_H

#include <QCoreApplication>
#include <string>

#include "grammar.h"
#include "Corpus/corpus.h"
#include "ssmatrix.h"
#include "bicluster.h"
#include "memory"


//worker class implementing the algorithm for inferring of CFG from corpus
class PCFG_BCL : public QObject {
    Q_OBJECT

    // parameters of the algorithm
    static const unsigned short NUM_RANDOM_RESTARTS = 10;
    static const double WEIGHT_THRESHOLD;
    static const unsigned short INFER_CONTEXT_MAX_DEPTH = 10;
    static const double ALPHA;

public:
    PCFG_BCL(){}
    //initializes algorithm with given corpus
    PCFG_BCL(std::unique_ptr<Corpus> corpus_);

    //triggers the inferring process (returns inferred grammar and transfers ownership)
    std::unique_ptr<Grammar> inferGrammar();
    bool isEmpty(){
        return matrix.getSize() == 0;
    }
public slots:
    void cancel(){
       inferringCancelled = true;
    }
private:
    // evaluates given BiCluster and stores intermediary results (sums for rows and cols in both BiCluster and EC matrix)
    double evaluate(const BiCluster &bc, int &s,
                    std::unordered_map<Symbol*, int> &rowSums,
                    std::unordered_map<Symbol*, int> &colSums,
                    std::unordered_map<Symbol *, int> &leftContextColSums,
                    std::unordered_map<Symbol *, int> &rightContextColSums);

    // evaluates contribution to evaluation of adding/removing a row/col to Bicluster
    // BiCluster is given by sums of rows and cols in BiCLuster and its EC matrix
    double evaluateContribution(Symbol* newRowOrCol, bool isRow, bool adding, int s,
                                const std::unordered_map<Symbol*, int> &rowSums,
                                const std::unordered_map<Symbol*, int> &colSums,
                                const std::unordered_map<Symbol *, int> &leftContextColSums,
                                const std::unordered_map<Symbol *, int> &rightContextColSums);

    // find local maximum among BiClusters starting from initialBicluster
    BiCluster hillClimb(BiCluster initialBicluster);
    // find best (resp. sufficiently good) BiCluster in the matrix
    BiCluster findBestBC();

    // reduce all appearances of all the pairs from given BiCluster to newAndNonterminal
    void reduceCorpus(const BiCluster& newBicluster, AndNonterminal* newAndNonterminal);
    // try to attach newAndNonterminal to an existing OrNode in grammar
    // suceeds if the expanded BiCluster is better evaluated
    void attach(AndNonterminal *newAndNonterminal, Grammar *grammar);

    // infer left and right context for andNonterminal, which was discovered in some previous iteration
    // it could have been already reduced to another AndNonterminal, traverse all the parents
    void inferContext(AndNonterminal *andNonterminal, std::unordered_map<Symbol *, int>& contextSums, bool isLeftContext, double coef, int depth);

    double xlogx(double x);

    std::unique_ptr<Corpus> corpus_;
    SSMatrix matrix;
    bool inferringCancelled;
};


#endif // PCFG_BCL_H
