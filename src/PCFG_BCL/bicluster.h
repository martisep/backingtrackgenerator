#ifndef BICLUSTER_H
#define BICLUSTER_H

#include <unordered_set>
#include <string>
#include <memory>

#include "symbol.h"

//represents bicluster of SSMatrix (it is basically just a pair of sets of row and column indexes and evaluation)
class BiCluster
{
public:
    BiCluster() : evaluation(0){}
    BiCluster(const std::unordered_set<Symbol*>& rowKeys, const std::unordered_set<Symbol*>& colKeys) :
        rowKeys_(rowKeys), colKeys_(colKeys), evaluation(0){}
    ~BiCluster(){}

    const std::unordered_set<Symbol*>& getRowKeys() const{
        return rowKeys_;
    }
    const std::unordered_set<Symbol*>& getColKeys() const{
        return colKeys_;
    }
    double getEvaluation(){
        return evaluation;
    }

    void addRow(Symbol* row){ rowKeys_.insert(row);}
    void addCol(Symbol* col){ colKeys_.insert(col);}
    void removeRow(Symbol* row){ rowKeys_.erase(row);}
    void removeCol(Symbol* col){ colKeys_.erase(col);}

    void setEvaluation(double evaluation){ this->evaluation = evaluation;}
    bool operator<(BiCluster other) const
    {
        return evaluation < other.getEvaluation();
    }

private:
    std::unordered_set<Symbol*> rowKeys_, colKeys_;
    double evaluation;
};

#endif // BICLUSTER_H
