#include "ssmatrix.h"
#include <set>
#include <chrono>


SSMatrix::SSMatrix(std::unique_ptr<Corpus>& c)
{
    for (const Sentence& sentence : c->getSentences()) {
        // iterate the sentences word by word and add their location to the matrix
        std::shared_ptr<Sentence::Word> loc = sentence.first;

        while(loc != nullptr){
            addAppearance(loc);
            loc = loc->next;
        }
    }
}

void SSMatrix::addAppearance(std::shared_ptr<Sentence::Word> loc) {
    //loc should point to first of the symbol pair, if there is no second, it is not a pair
    if (loc->next == nullptr)
        return;

    // get the symbol pair
    Symbol* rowkey = loc->getSymbol();
    Symbol* colkey = loc->next->getSymbol();
    // add this location to matrix
    matrix[std::make_pair(rowkey,colkey)].addLocation(loc);

    //update sets of used row/col keys
    rowKeys.insert(rowkey);
    colKeys.insert(colkey);
}

void SSMatrix::removeAppearance(std::shared_ptr<Sentence::Word> loc){
        //loc should point to first of the symbol pair, if there is no second, it is not a pair
    if (loc->next == nullptr)
        return;

    // get the symbol pair
    Symbol* first = loc->getSymbol();
    Symbol* second = loc->next->getSymbol();

    // find corresponding cell (if there is one)
    auto it = matrix.find(std::make_pair(first, second));
    if(it != matrix.end()){
        // remove current location of the pair
        it->second.removeLocation(loc);
        //if it was the last appearance, erase the cell
        if(it->second.getNumApp() <= 0){
            matrix.erase(it);
            //check if a row wasnt emptied
            bool emptyRow = true;
            for(auto col : colKeys){
                if(matrix.find(std::make_pair(first, col)) != matrix.end())
                    emptyRow = false;
            }
            if(emptyRow)
                rowKeys.erase(first);

            //check if a col wasnt emptied
            bool emptyCol = true;
            for(auto row : rowKeys){
                if(matrix.find(std::make_pair(row, second)) != matrix.end())
                    emptyRow = false;
            }
            if (emptyCol)
                colKeys.erase(second);
        }
    }
}


const std::pair<Symbol*,Symbol*> &SSMatrix::getRandomKey(){
    // use system time as a seed
    // random generator can be static, we only change the parameters on each run
    static std::mt19937 mt(std::chrono::system_clock::now().time_since_epoch().count());
    static std::uniform_int_distribution<int> dist;

    std::uniform_int_distribution<int>::param_type param(0, matrix.size() - 1);
    size_t n = dist(mt, param);

    auto it = matrix.begin();
    std::advance(it, n);
    return it->first;
}

// add given location to the set of locations of current symbol pair
void SSMatrix::SSMatrixCell::addLocation(std::shared_ptr<Sentence::Word> loc) {
    //loc should point to first of the symbol pair, if there is no second, it is not a pair
    if(loc->next == nullptr){
        return;
    }
    //add location
    locationsInCorpus.insert(loc);
    // increase counter
    numApp_++;

    //increase counter of appearances of left/right context
    Symbol* leftContext;
    Symbol* rightContext;

    //beginning of the sentence, use special symbol
    if(loc->prev.lock() == nullptr){
        leftContext = Corpus::beginningOrEndOfSentence.get();
    } else {
        leftContext = loc->prev.lock()->getSymbol();
    }
    //end of the sentence, use special symbol
    if(loc->next->next == nullptr){
        rightContext = Corpus::beginningOrEndOfSentence.get();
    } else {
        rightContext = loc->next->next->getSymbol();
    }
    // increase appearances with this left/right context
    leftContextsCounter[leftContext]++;
    rightContextsCounter[rightContext]++;
}

// remove given location from the set of locations of current symbol pair
void SSMatrix::SSMatrixCell::removeLocation(std::shared_ptr<Sentence::Word> loc) {
    //remove location (if it exists in set)
    locationsInCorpus.erase(loc);
    // decrease counter
    numApp_--;

    //decrease counter of appearances of left/right context
    Symbol* left;
    Symbol* right;

    //beginning of the sentence, use special symbol
    if(loc->prev.lock() == nullptr){
        left = Corpus::beginningOrEndOfSentence.get();
    } else {
        left = loc->prev.lock()->getSymbol();
    }
    //end of the sentence, use special symbol
    if(loc->next->next == nullptr){
        right = Corpus::beginningOrEndOfSentence.get();

    } else {
        right = loc->next->next->getSymbol();
    }
    // decrease appearances with this left/right context
    leftContextsCounter[left]--;
    rightContextsCounter[right]--;
}
