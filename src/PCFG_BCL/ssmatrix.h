#ifndef SSMATRIX_H
#define SSMATRIX_H

#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <memory>

#include "corpus.h"
#include "sentence.h"
#include "symbol.h"

//class representing a Symbol-Symbol Matrix with numbers and locations of appearances of symbol pairs in corpus
class SSMatrix
{
public:
    //structure representing a cell of the matrix, stores references to locations of the symbol pair in corpus
    struct SSMatrixCell{
      public:
        SSMatrixCell() : numApp_(0){}
        void clear(){
            numApp_ = 0;
            locationsInCorpus.clear();
            leftContextsCounter.clear();
            rightContextsCounter.clear();
        }
        void addLeftContext(Symbol* s){ leftContextsCounter[s]++;}
        void addRightContext(Symbol* s){ rightContextsCounter[s]++;}

        void addLocation(std::shared_ptr<Sentence::Word> it);

        void removeLocation(std::shared_ptr<Sentence::Word> loc);

        int getNumApp() const {
            return numApp_;
        }
        std::unordered_map<Symbol*, int> const& getLeftContexts() const {
            return leftContextsCounter;
        }
        std::unordered_map<Symbol*, int> const& getRightContexts() const {
            return rightContextsCounter;
        }
        std::unordered_set<std::shared_ptr<Sentence::Word>> const& getLocations()const {
            return locationsInCorpus;
        }
        std::string toString() const{
            return std::to_string(numApp_);
        }
      private:
        // number of all the appearances of pair in corpus
        int numApp_;

        // counts numbers of appearances in different contexts, where context is given by left, respectively right neighbor
        // represents a row in corresponding Expression-Context matrix (EC-left resp. EC-right)
        std::unordered_map<Symbol*,int> leftContextsCounter;
        std::unordered_map<Symbol*,int> rightContextsCounter;

        //stores pointers to locations of the pair in corpus
        std::unordered_set<std::shared_ptr<Sentence::Word>> locationsInCorpus;
    };


    //constructs matrix from corpus
    SSMatrix(std::unique_ptr<Corpus> &c);
    SSMatrix(){}
    ~SSMatrix(){}

    // clears the cell at given indexes
    void clear(Symbol* rowkey, Symbol* colkey){
        matrix[std::make_pair(rowkey,colkey)].clear();
    }

    // takes symbol pair at given location and adds its location to matrix
    void addAppearance(std::shared_ptr<Sentence::Word> loc);
    // takes symbol pair at given location and removes its loacation from matrix
    void removeAppearance(std::shared_ptr<Sentence::Word> loc);

    bool contains(Symbol* rowkey, Symbol* colkey){
        return matrix.find(std::make_pair(rowkey, colkey)) != matrix.end();
    }
    size_t getSize(){
        return matrix.size();
    }

    //returns random element from the matrix (used for random restarts in hill-climbing)
    const std::pair<Symbol*,Symbol*> & getRandomKey();

    const std::unordered_set<Symbol*>& getRowKeys() const { return rowKeys; }
    const std::unordered_set<Symbol*>& getColKeys() const { return colKeys; }

    //return cell at indexes
    SSMatrixCell const& getCell(Symbol* row, Symbol* col){
        return this->matrix[std::make_pair(row,col)];
    }

private:
    std::unordered_map<std::pair<Symbol*,Symbol*>, SSMatrixCell> matrix;
    // set of row/col keys present in matrix
    std::unordered_set<Symbol*> rowKeys, colKeys;
};

#endif // SSMATRIX_H
