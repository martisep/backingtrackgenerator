//
// Created by Petr on 17.4.2015.
//

#include <iostream>
#include <math.h>
#include <random>
#include "PCFG_BCL.h"

const double PCFG_BCL::WEIGHT_THRESHOLD = 0.0;
const double PCFG_BCL::ALPHA = 0.3;

PCFG_BCL::PCFG_BCL(std::unique_ptr<Corpus> corpus) {
    // claims ownership of the corpus (it will be modified during the procedure)
    corpus_ = std::move(corpus);
    // create matrix of symbol pairs from corpus
    matrix = SSMatrix(corpus_);
}

//returns Probabilistic Context-Free Grammar in AND-OR form constructed from loaded Corpus
std::unique_ptr<Grammar> PCFG_BCL::inferGrammar() {
    // initialize grammar with the terminals from Corpus
    std::unique_ptr<Grammar> grammar = std::unique_ptr<Grammar>(new Grammar(corpus_->getTerminals()));

    if(matrix.getSize() == 0){
        return grammar;
    }
    inferringCancelled = false;

    // find BiCluster with best score (or sufficiently good) and reduce it to new symbol
    // repeat until no more reduction is possible
    BiCluster bestbc = findBestBC();

    while(bestbc.getEvaluation() > WEIGHT_THRESHOLD){
        qApp->processEvents();
        if(inferringCancelled){
            grammar->clear();
            return grammar;
        }

        std::unordered_map<Symbol*, int> rowkeys, colkeys;
        //get sums of appearences in rows/cols of the BiCluster
        for(const auto& rowkey : bestbc.getRowKeys()){
            for(const auto& colkey : bestbc.getColKeys()){
                int numApp = matrix.getCell(rowkey, colkey).getNumApp();
                rowkeys[rowkey] += numApp;
                colkeys[colkey] += numApp;
            }
        }

        //add new non-terminals to grammar
        OrNonterminal* left = grammar->addOrNonterminal(OrNonterminal(rowkeys));
        OrNonterminal* right = grammar->addOrNonterminal(OrNonterminal(colkeys));
        AndNonterminal* andNonterminal = grammar->addAndNonterminal(AndNonterminal(left,right));

        //add parents
        left->addParent(andNonterminal);
        right->addParent(andNonterminal);

        for(auto& child : left->getChildren()){
            AndNonterminal* a = dynamic_cast<AndNonterminal*>(child.first);
            if(a != nullptr){
                a->addParent(left);
            }
        }
        for(auto& child : right->getChildren()){
            AndNonterminal* a = dynamic_cast<AndNonterminal*>(child.first);
            if(a != nullptr){
                a->addParent(right);
            }
        }

        //reduce all appearances of pairs from BiCluster to new AND nonterminal
        reduceCorpus(bestbc, andNonterminal);
        //try to attach the new andNonterminal to existing OrNonterminals
        attach(andNonterminal, grammar.get());


        //find new candidate
        bestbc = findBestBC();
    }


    //postprocessing - add start rules (all the AndNonterminals)
    OrNonterminal startSymbol;
    for(const auto& a : grammar->getAndNonterminals()){
        startSymbol.addChild(a.get());
    }
    grammar->setStartSymbol(grammar->addOrNonterminal(startSymbol));

    return grammar;
}

//returns BiCluster (submatrix) with highest (or sufficiently high) score using hillclimbing with random restarts
BiCluster PCFG_BCL::findBestBC(){
    BiCluster bestBC;

    //repeat the procedure multiple times with different initial seeds for better result
    for(int i = 0; i < NUM_RANDOM_RESTARTS; i++){

        //starting point is randomly chosen cell from the matrix (it is a BiCluster of size 1x1)
        const std::pair<Symbol*,Symbol*>& keypair = matrix.getRandomKey();

        std::unordered_set<Symbol*> bcRows = {keypair.first};
        std::unordered_set<Symbol*> bcCols = {keypair.second};
        BiCluster initialBC(bcRows, bcCols);
        //hillclimb the initial BiCluster to find the best candidate
        BiCluster candidate = hillClimb(initialBC);
        if(bestBC < candidate){
            bestBC = candidate;
        }
    }
    return bestBC;
}

BiCluster PCFG_BCL::hillClimb(BiCluster initialBicluster){
    /* neighbor of a BC is another BC such that we can obtain it via single atomic operation,
     * which are adding/removing row/column. Therefore, each BC has (rows.size + cols.size) neighbors.
     *
     * for each neighbor we compute its contribution to objective function
     * then we choose new BC (end if there is no better BC among neighbors)
    */
    int s;
    std::unordered_map<Symbol*, int> rowSums, colSums;
    std::unordered_map<Symbol*, int> leftContextColSums;
    std::unordered_map<Symbol*, int> rightContextColSums;

    //evaluate initial BC and fill row/col sums for faster computation of contribution of new row/col
    initialBicluster.setEvaluation(evaluate(initialBicluster, s, rowSums, colSums, leftContextColSums, rightContextColSums));

    BiCluster bestBC = initialBicluster;

    bool improved = true;

    while(improved){
        // represents the index of row or col, whose adding will best improve the evaluation
        Symbol* bestCandidate = nullptr;
        // current best score
        double bestScore = 0;
        // row or col
        bool isRow = true;
        // adding or removing
        bool adding = true;


        improved = false;
        for(const auto& row : matrix.getRowKeys()){

            //adding row
            if(bestBC.getRowKeys().find(row) == bestBC.getRowKeys().end()){
                //evaluate contribution of adding current row to the bicluster
                double eval = evaluateContribution(row, true, true, s, rowSums, colSums, leftContextColSums, rightContextColSums);
                if(eval > bestScore){
                    bestCandidate = row;
                    bestScore = eval;
                    isRow = true;
                    adding = true;
                }

            }
            //removing row
            else {
                //dont remove if only row in bicluster
                if(bestBC.getRowKeys().size() <= 1){
                    continue;
                }
                //evaluate contribution of removing current row from the bicluster
                double eval = evaluateContribution(row, true, false, s, rowSums, colSums, leftContextColSums, rightContextColSums);
                if(eval > bestScore){
                    bestCandidate = row;
                    bestScore = eval;
                    isRow = true;
                    adding = false;
                }
            }
        }

        for(const auto& col : matrix.getColKeys()){

            //adding column
            if(bestBC.getColKeys().find(col) == bestBC.getColKeys().end()){
                //evaluate contribution of adding current column to the bicluster
                double eval = evaluateContribution(col, false, true, s, rowSums, colSums, leftContextColSums, rightContextColSums);
                if(eval > bestScore){
                    bestCandidate = col;
                    bestScore = eval;
                    isRow = false;
                    adding = true;
                }
            }
            //removing column
            else {
                //dont remove if only column in bicluster
                if(bestBC.getColKeys().size() <= 1){
                    continue;
                }
                //evaluate contribution of removing current column from the bicluster
                double eval = evaluateContribution(col, false, false, s, rowSums, colSums, leftContextColSums, rightContextColSums);
                if(eval > bestScore){
                    bestCandidate = col;
                    bestScore = eval;
                    isRow = false;
                    adding = false;
                }
            }
        }

        //if we found a suitable candidate, add it or remove it from bicluster
        if(bestCandidate != nullptr){
            if(isRow){
                if(adding){
                    bestBC.addRow(bestCandidate);
                } else {
                    bestBC.removeRow(bestCandidate);
                }
            } else {
                if(adding){
                    bestBC.addCol(bestCandidate);
                } else {
                    bestBC.removeCol(bestCandidate);
                }
            }
            //evaluate the expanded bicluster (and update row/col sums)
            bestBC.setEvaluation(evaluate(bestBC,s,rowSums,colSums,leftContextColSums, rightContextColSums));
            improved = true;
        }
    }
    return bestBC;

}



//evaluates given bicluster AND fills the containers with computed values for the following computation to be faster
double PCFG_BCL::evaluate(const BiCluster &bc, int &s,
                          std::unordered_map<Symbol *, int> &rowSums,
                          std::unordered_map<Symbol *, int> &colSums,
                          std::unordered_map<Symbol *, int> &leftContextColSums,
                          std::unordered_map<Symbol *, int> &rightContextColSums)
{
    /* Objective function of a bicluster is computed as sum of four numbers:
            1. bicluster coherence
            2. left context matrix coherence
            3. right context matrix coherence
            4. prior probability (penalizing too specialized grammars in favor of more general ones)
       Notation:
            BICLUSTER:
            * A - set of rows; B - set of columns
            * r_x - sum of values in row x
            * c_y - sum of values in column y
            * s - sum of all entries
            * a_xy - value of cell xy

            LEFT CONTEXT MATRIX
            * LCM - left context matrix; lcA - set of rows; lcB - set of columns
            * lcr_x - sum of values in row x
            * lcc_y - sum of values in column y
            * lcs - sum of all entries
            * lca_xy - value of cell xy

            RIGHT CONTEXT MATRIX
            * RCM - right context matrix; rcA - set of rows; rcB - set of columns
            * rcr_x - sum of values in row x
            * rcc_y - sum of values in column y
            * rcs - sum of all entries
            * rca_xy - value of cell xy

            PRIOR
            alpha - parametr of the prior that specifies how much the prior favors compact grammars

       Formulas:
            1. SUM_all-x-in-A(r_x * log(r_x))                         [1r]
                + SUM_all-y-in-B(c_y * log(c_y))                      [1c]
                - s*log(s)                                            [1s]
                - SUM_all-x-in-A-all-y-in-B(a_xy * log(a_xy))         [1a]

            2. SUM_all-x-in-lcA(lcr_x * log(lcr_x))                   [2r]
                + SUM_all-y-in-lcB(lcc_y * log(lcc_y))                [2c]
                - lcs*log(lcs)                                        [2s]
                - SUM_all-x-in-lcA-all-y-in-lcB(lca_xy * log(lca_xy)) [2a]

            2. SUM_all-x-in-rcA(rcr_x * log(rcr_x))                   [3r]
                + SUM_all-y-in-rcB(rcc_y * log(rcc_y))                [3c]
                - rcs*log(rcs)                                        [3s]
                - SUM_all-x-in-rcA-all-y-in-rcB(rca_xy * log(rca_xy)) [3a]

            4. alpha * (4 * s - 2*|A| - 2*|B| - 8)                    [4]

    */

    double bcCoherence = 0, leftContextMatrixCoherence = 0, rightContextMatrixCoherence = 0, prior = 0;

    int lcs = 0, rcs = 0, rowSum = 0;
    s = 0;

    // empty previously stored values
    rowSums.clear();
    colSums.clear();
    leftContextColSums.clear();
    rightContextColSums.clear();


    for(const auto& rowKey : bc.getRowKeys()){
        for(const auto& colKey : bc.getColKeys()){

            SSMatrix::SSMatrixCell cell = matrix.getCell(rowKey,colKey);
            int numApp = cell.getNumApp();

            rowSum += numApp;
            colSums[colKey] += numApp;
            s += numApp;

            bcCoherence -= xlogx(numApp);  // [1a]

            int rowLeftContextSum = 0;
            for(const auto& entry : cell.getLeftContexts()){
                leftContextMatrixCoherence -= xlogx(entry.second); // [2a]
                rowLeftContextSum += entry.second;
                leftContextColSums[entry.first] += entry.second; //
                lcs += entry.second;
            }
            leftContextMatrixCoherence += xlogx(rowLeftContextSum); // [2r]

            int rowRightContextSum = 0;
            for(const auto& entry : cell.getRightContexts()){
                rightContextMatrixCoherence -= xlogx(entry.second); // [3a]
                rowRightContextSum += entry.second;
                rightContextColSums[entry.first] += entry.second; //
                rcs += entry.second;
            }
            rightContextMatrixCoherence += xlogx(rowRightContextSum); // [3r]
        }

        bcCoherence += xlogx(rowSum);  // [1r]
        rowSums[rowKey] = rowSum;
        rowSum = 0;
    }

    bcCoherence -= xlogx(s); // [1s]
    for(const auto& colSum : colSums){
        bcCoherence += xlogx(colSum.second); // [1c]
    }

    leftContextMatrixCoherence -= xlogx(lcs); // [2s]
    rightContextMatrixCoherence -= xlogx(rcs); // [3s]

    for(const auto& entry : leftContextColSums){
        leftContextMatrixCoherence += xlogx(entry.second); //[2c]
    }
    for(const auto& entry : rightContextColSums){
        rightContextMatrixCoherence += xlogx(entry.second); //[3c]
    }

    int a = bc.getRowKeys().size();
    int b = bc.getColKeys().size();

    prior = ALPHA * (4 * s - 2 * a - 2 * b  - 8); // [4]

    //normalize left and right matrix coherence by 1/2
    return bcCoherence + leftContextMatrixCoherence/2 + rightContextMatrixCoherence/2 + prior;
}



//evaluates the contribution of adding a new row or column to the bicluster (based on function evaluate(), but more efficient
//uses values computed by evaluate() beforehand
double PCFG_BCL::evaluateContribution(Symbol *newRowOrCol, bool isRow, bool adding, int s,
                                      const std::unordered_map<Symbol *, int> &rowSums,
                                      const std::unordered_map<Symbol *, int> &colSums,
                                      const std::unordered_map<Symbol *, int> &leftContextColSums,
                                      const std::unordered_map<Symbol *, int> &rightContextColSums
                                      ){
    /* Evaluation function of adding a row to bicluster is computed as sum of four numbers:
            1. bicluster coherence contribution
            2. left context matrix coherence contribution
            3. right context matrix coherence contribution
            4. prior probability contribution

       Notation:
            xlogx(value) = value * log(value)

            BICLUSTER:
            * A - set of rows; B - set of columns
            * r_x - sum of values in row x
            * c_y - sum of values in column y
            * s - sum of all entries
            * a_xy - value of cell xy

            NEW ROW
            * N - symbol indexing the new row
            * a_Ny - value of cell Ny
            * r_N - sum of values in the new row (sum_all_y_in_B (a_Ny))

            LEFT CONTEXT MATRIX
            * LCM - left context matrix; lcA - set of rows; lcB - set of columns
            * lcr_x - sum of values in row x
            * lcc_y - sum of values in column y
            * lcs - sum of all entries
            * lca_xy - value of cell xy

            RIGHT CONTEXT MATRIX
            * RCM - right context matrix; rcA - set of rows; rcB - set of columns
            * rcr_x - sum of values in row x
            * rcc_y - sum of values in column y
            * rcs - sum of all entries
            * rca_xy - value of cell xy

            PRIOR
            alpha - parametr of the prior that specifies how much the prior favors compact grammars

       Formulas:
            1. xlogx(r_N)                                                     [1r]
                + SUM_all-y-in-B(xlogx(c_y + a_Ny))                           [1cn]
                - SUM_all-y-in-B(xlogx(c_y))                                  [1c]
                - 2 * xlogx(s + r_N)                                          [1sn]
                + 2 * xlogx(s)                                                [1s]

            2. SUM_all-q-in-lcB(xlogx(lcc_q + sum_all_y_in_B(lca_{Ny,q})))    [2cn]
                - SUM_all-q-in-lcB(xlogx(lcc_q))                              [2c]
                - SUM_all-q-in-lcB-all-y-in-B(xlogx(lca_{Ny,q}))              [2a]

            2. SUM_all-q-in-rcB(xlogx(rcc_q + sum_all_y_in_B(rca_{Ny,q})))    [3cn]
                - SUM_all-q-in-rcB(xlogx(rcc_q))                              [3c]
                - SUM_all-q-in-rcB-all-y-in-B(xlogx(rca_{Ny,q}))              [3a]

            4. alpha * (4 * r_N - 2)                                          [4]

       Removing of row has the same formulas, with the only difference that we substract new values.
       It is implemented by using coefficient, which takes either 1 or -1 depending on adding/removing.

       Adding or removing column is similar. Differences are managed by branching the computation using
       boolean isRow.

    */

    int newRowOrColSum = 0;
    double contribution = 0;
    // adding or removing
    int coef = adding ? 1 : -1;

    if(isRow){
        for(const auto& y : colSums){
            int numApps =  matrix.getCell(newRowOrCol, y.first).getNumApp();
            contribution += xlogx(y.second + coef*numApps);      //[1cn]
            contribution -= xlogx(y.second);       //[1c]

            newRowOrColSum += numApps;
        }
    } else {
        for(const auto& x : rowSums){
            int numApps =  matrix.getCell(x.first, newRowOrCol).getNumApp();
            contribution += xlogx(x.second + coef*numApps);
            contribution -= xlogx(x.second);

            newRowOrColSum += numApps;
        }
    }

    //high level filter
    if(adding && newRowOrColSum == 0){
        return 0;
    }

    contribution += coef * xlogx(newRowOrColSum);           //[1r]
    contribution -= 2 * xlogx(s + coef*newRowOrColSum);     //[1sn]
    contribution += 2 * xlogx(s);                           //[1s]

    for(const auto& q : leftContextColSums){
        int newValuesInColumn = 0;

        if(isRow){
            for(const auto& y : colSums){
                int val;
                try{
                    val = matrix.getCell(newRowOrCol,y.first).getLeftContexts().at(q.first);
                }
                catch(const std::out_of_range&){
                    val = 0;
                }

                newValuesInColumn += val;
                contribution-= coef*xlogx(val)/2;        //[2a]
            }
        } else{
            for(const auto& x : rowSums){
                int val;
                try{
                    val = matrix.getCell(x.first,newRowOrCol).getLeftContexts().at(q.first);
                }
                catch(const std::out_of_range&){
                    val = 0;
                }
                newValuesInColumn += val;
                contribution-= coef*xlogx(val)/2;
            }
        }

        contribution += xlogx(q.second + coef*newValuesInColumn)/2; //[2cn]
        contribution -= xlogx(q.second)/2;                          //[2c]
    }

    for(const auto& q : rightContextColSums){
        int newValuesInColumn = 0;

        if(isRow){
            for(const auto& y : colSums){
                int val;
                try{
                    val = matrix.getCell(newRowOrCol,y.first).getRightContexts().at(q.first);
                }
                catch(const std::out_of_range&){
                    val = 0;
                }

                newValuesInColumn += val;
                contribution-= coef*xlogx(val)/2;  //[3a]
            }
        } else{
            for(const auto& x : rowSums){
                int val;
                try{
                    val = matrix.getCell(x.first,newRowOrCol).getRightContexts().at(q.first);
                }
                catch(const std::out_of_range&){
                    val = 0;
                }
                newValuesInColumn += val;
                contribution-= coef*xlogx(val)/2;
            }
        }

        contribution += xlogx(q.second + coef*newValuesInColumn)/2;  //[3cn]
        contribution -= xlogx(q.second)/2;                          //[3c]
    }

    contribution += ALPHA * coef * (4 * newRowOrColSum - 2);  //[4]

    return contribution;
}


//reduce corpus with new rules after extracting a new bicluster
void PCFG_BCL::reduceCorpus(const BiCluster& bestbc, AndNonterminal* andNonterminal) {
    for(const auto & row :bestbc.getRowKeys()){
        for(const auto & col : bestbc.getColKeys()){
            //first lets replace each appearance of the pair in Corpus with new AND Symbol,
            //reconnect surrounding words accordingly and add new entries into the matrix
            for(std::shared_ptr<Sentence::Word> location : matrix.getCell(row, col).getLocations()){

                std::shared_ptr<Sentence::Word> leftContext = location->prev.lock();
                std::shared_ptr<Sentence::Word> second = location->next;
                std::shared_ptr<Sentence::Word> rightContext = second != nullptr ? second->next : nullptr;


                //if not beginning of a Sentence
                if(leftContext != nullptr){
                    matrix.removeAppearance(leftContext);
                }

                //change symbol under first word of the pair to new AND node
                location->setSymbol(andNonterminal);

                if(leftContext != nullptr){
                    //add new entry to matrix for left neighbor and new AND symbol
                    //and a pointer to its location in the corpus
                    matrix.addAppearance(leftContext);
                }

                /* and reconnect to right neighbor (at this point, second word of the pair is
                 disconnected from the sentence - the only shared_ptr pointing to it is the local
                 variable 'second'. All other pointers are of type weak_ptr.)
                */
                location->next = rightContext;

                /*if there is a right neighbor (it is not an end of a sentence), then
                 * there is entry in the matrix for the pair (second, rightNeighbor), which
                 * is invalidated
                */
                if(rightContext != nullptr){
                    //reconnect the sentence
                    rightContext->prev = std::weak_ptr<Sentence::Word>(location);
                    //and remove invalidated appearance of pair (second, rightNeighbor) from matrix
                    matrix.removeAppearance(second);

                    //add new entry to the matrix for the new AND symbol and the rightNeighbor
                    matrix.addAppearance(location);
                }
            }
            //now the Corpus is updated (regarding this pair), invalidated neighbor entries in matrix
            // are resolved and new entries for AND symbols have been added, therefore is now safe
            // to clear the cell
            matrix.clear(row,col);
        }
    }
}

// try to attach newAndNonterminal to an existing OrNode in grammar
// suceeds if the expanded BiCluster is better evaluated
void PCFG_BCL::attach(AndNonterminal* newAndNonterminal, Grammar* grammar) {

    for (const auto& orNonterminalParent : grammar->getOrNonterminals()) {
        // get all bc in which ornode figured

        for (AndNonterminal* andNonterminalGrandParent : orNonterminalParent->getParents()) {
            //ssmatrix already has different values, we can use weights saved in grammar rules for biCluster,
            //but we need to compute the values for EC matrix by traversing all parents

            //determine whether the current orNonterminal represents rows or columns in the bicluster
            bool isRow = orNonterminalParent.get() == andNonterminalGrandParent->getLeft();


            //doesn't matter whether we take sum of all rows or all columns
            int s = andNonterminalGrandParent->getLeft()->getSumWeights();

            //construct derived EC matrix
            std::unordered_map<Symbol*, int> leftContextColSums;
            std::unordered_map<Symbol*, int> rightContextColSums;

            inferContext(andNonterminalGrandParent,leftContextColSums,true,1,0);
            inferContext(andNonterminalGrandParent,rightContextColSums,false,1,0);

            //evaluate contribution of the new andNonterminal to the derived bicluster
            double contribution = evaluateContribution(newAndNonterminal, isRow, true, s,
                                                       andNonterminalGrandParent->getLeft()->getChildren(),
                                                       andNonterminalGrandParent->getRight()->getChildren(),
                                                       leftContextColSums,
                                                       rightContextColSums);


            if (contribution > WEIGHT_THRESHOLD) {
                //attach new AND symbol to this OrNonterminal, which effectively creates new OR node (with different children)
                int newRowOrColSum = 0;

                if(isRow){
                    for(const auto& y : andNonterminalGrandParent->getRight()->getChildren()){
                        int numApps =  matrix.getCell(newAndNonterminal, y.first).getNumApp();
                        newRowOrColSum += numApps;
                    }
                } else {
                    for(const auto& x : andNonterminalGrandParent->getLeft()->getChildren()){
                        int numApps =  matrix.getCell(x.first, newAndNonterminal).getNumApp();
                        newRowOrColSum += numApps;
                    }
                }

                //if this OR node has multiple parents, we must create new OR node and reconnect
                if(orNonterminalParent->getParents().size() > 1){
                    OrNonterminal newOrNonterminal(orNonterminalParent->getChildren());
                    newOrNonterminal.addChild(newAndNonterminal,newRowOrColSum);
                    OrNonterminal* o = grammar->addOrNonterminal(newOrNonterminal);

                    //reconnect parentage to new OR node
                    orNonterminalParent->removeParent(andNonterminalGrandParent);
                    o->addParent(andNonterminalGrandParent);
                    newAndNonterminal->addParent(o);
                } else {
                    orNonterminalParent->addChild(newAndNonterminal,newRowOrColSum);
                    newAndNonterminal->addParent(orNonterminalParent.get());
                }

            }
        }
    }
}



void PCFG_BCL::inferContext(AndNonterminal* andNonterminal, std::unordered_map<Symbol*, int>& contextSums, bool isLeftContext, double coef, int depth){
    if(isLeftContext){
        for(Symbol* row : matrix.getRowKeys()){
            int numapps = matrix.getCell(row,andNonterminal).getNumApp();
            if(numapps > 0){
                contextSums[row] += coef * numapps;
            }
        }
    } else {
        for(Symbol* col : matrix.getColKeys()){
            int numapps = matrix.getCell(andNonterminal, col).getNumApp();
            contextSums[col] += coef * numapps;
        }
    }
    for(OrNonterminal* orParent : andNonterminal->getParents()){
        for(AndNonterminal* andParent : orParent->getParents()){
            bool isLeftChild = (orParent == andParent->getLeft());
            int ratio = (double)orParent->getChildren().at(andNonterminal) / orParent->getSumWeights();

            //recursively get contexts of AND parents
            if(isLeftChild == isLeftContext){
                if(depth < INFER_CONTEXT_MAX_DEPTH){
                    inferContext(andParent, contextSums,isLeftContext,coef * ratio, depth+1);
                }
            } else {
                //contexts are AND's siblings
                OrNonterminal* uncle = isLeftChild ? andParent->getRight() : andParent->getLeft();
                for(const auto& child : uncle->getChildren()){
                    contextSums[child.first] += child.second * ratio * coef;
                }
            }
        }
    }


}



double PCFG_BCL::xlogx(double x){
    return x == 0 ? 0 : x * log(x);
}
