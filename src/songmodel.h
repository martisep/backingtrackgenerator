#ifndef SONGMODEL_H
#define SONGMODEL_H
#include <QAbstractListModel>
#include <QBrush>
#include "song.h"

// custom implementation of QAbstractModel, used to provide data for chordsView
class SongModel : public QAbstractListModel
{
public:
    SongModel(QObject* parent = 0) : QAbstractListModel(parent) {
        song = nullptr;
    }
    ~SongModel(){}
    //number of measures
    int rowCount(const QModelIndex &parent) const;

    QVariant data(const QModelIndex &index, int role) const;
    QModelIndex getIndex(int beat);
    void setSong(Song* s);

private:
    //data is the current song
    Song* song;
};

#endif // SONGMODEL_H
