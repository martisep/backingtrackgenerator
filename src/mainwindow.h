#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringListModel>
#include <QStringList>
#include <QAbstractItemView>
#include <QStandardItemModel>
#include <QKeyEvent>
#include <map>
#include "grammar.h"
#include "song.h"
#include "PCFG_BCL.h"
#include "songmodel.h"
#include "QMidiOut.h"
#include "midiplayer.h"
#include "settingsdialog.h"
#include "paramobject.h"

static const std::string GRAMMAR_CHORD_FILE = "chordGrammar.txt";
static const std::string GRAMMAR_SONGPART_FILE = "songPartGrammar.txt";

static const std::string CORPUS_RS200HARMONY_DIR = "corpora/rs200_harmony";
static const std::string CORPUS_REFERENCE_SEGMENTATIONS_DIR = "corpora/reference_segmentations";
static const std::string CORPUS_IRB_DIR = "corpora/iRb_v1-1";
static const std::string CORPUS_IMAGINARY_BOOK = "corpora/imaginary-book";


namespace Ui {

class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    // updates the chordsView to the correct Measure
    void updateBeat(int beat);
    // enables control elements in gui
    void stoppedPlayback();
    // disables controls elements in gui
    void startedPlayback();
protected:
    // ensure that keyPressed events are first handled by this class (for detecting spacebar)
    bool eventFilter(QObject* obj, QEvent* event);

private slots:
    // launches or stops the playback
    void on_playBtn_clicked();
    // launches the song generation
    void on_generateBtn_clicked();
    // gets beat of the selected field and launches player from that beat
    void on_chordsView_clicked(const QModelIndex &index);
    // launches SettingsDialog, passes current paramObject and gets an updated one afterwards
    void on_actionSettings_triggered();
    // launches 'Save to MIDI' dialog
    void on_actionSave_to_MIDI_triggered();
    // launches the inferring process
    void on_actionInfer_grammars_from_corpora_triggered();
    // updates chordsView to show the song in the new key
    void on_keyComboBox_currentIndexChanged(int index);
    // updates bass weights in paramObject according to new genre, sets tempoSpinBox to default tempo of the genre
    void on_genreComboBox_currentIndexChanged(int index);
signals:
    void cancelInferring();

private:
    // loads the corpora and initiates the inferring procedure
    void inferGrammars();
    // loads grammars from files, if available
    void loadGrammars();
    // creates new Song from grammars (if available) and displays it in chordsView
    void generateSong();
    // updates the model connected to chordsView
    void displaySong();
    // launches or stops the playback
    void playOrStop();
    //play the song from given beat
    void play(int from = 0);
    // launches 'Save to MIDI' dialog
    void saveToMidi();


    Ui::MainWindow *ui;
    std::unique_ptr<SongModel> model;
    std::unique_ptr<Grammar> grammarChords, grammarSongparts;
    std::unique_ptr<Song> song;
    std::unique_ptr<MidiPlayer> player;
    std::unique_ptr<QMidiOut> midi_out;

    std::unique_ptr<ParamObject> paramObject;

};

#endif // MAINWINDOW_H
