#include "songmodel.h"


//number of measures
int SongModel::rowCount(const QModelIndex &parent) const{
    if(song == nullptr){
        return 0;
    }

    size_t count = 0;
    for(const SongPart& sp : song->getSongparts()){
        count += sp.getMeasures().size();
    }
    return count;
}

// method which can return different representation of current Measure based on given 'role'
QVariant SongModel::data(const QModelIndex &index, int role) const{
    if(!index.isValid()){
        return QVariant();
    }
    if(song == nullptr){
        return QVariant();
    }

    // returns style for displayed chords (different colors for different song parts)
    if(role == Qt::ForegroundRole){
        int tmp = 0;
        for(const SongPart& sp : song->getSongparts()){
            tmp += sp.getMeasures().size();
            if(tmp > index.row()){
                switch (sp.getType()) {
                case SongPart::VERSE:
                    return QBrush(Qt::blue);
                case SongPart::CHORUS:
                    return QBrush(Qt::red);
                case SongPart::BRIDGE:
                    return QBrush(Qt::darkGreen);
                default:
                    return QBrush(Qt::black);
                }
            }
        }
        return QVariant();
    }

    // returns human eadable string representation of chords in current measure (in the key of the song)
    if(role == Qt::DisplayRole){
        int tmp = 0;
        for(const SongPart& sp : song->getSongparts()){
            for(const Measure& m : sp.getMeasures()){
                if(tmp == index.row()){
                    return QString::fromStdString(m.toString(song->getKey()));
                }
                tmp++;
            }
        }
        return "";
    }
    // returns current beat of the song
    if(role == Qt::UserRole){
        int tmp = 0;
        int beats = 0;

        for(const SongPart& sp : song->getSongparts()){
            for(const Measure& m : sp.getMeasures()){
                if(tmp == index.row()){
                    return beats;
                }
                tmp++;
                beats += m.getLength()/4;
            }
        }
    }
    return QVariant();
}

// returns index in the model corresponding to given beat of the song
QModelIndex SongModel::getIndex(int beat){
    int index = 0;
    int tmp = 0;

    for(const SongPart& sp : song->getSongparts()){
        for(const Measure& m : sp.getMeasures()){
            tmp += m.getLength()/4;
            if(beat < tmp){
                return QAbstractListModel::createIndex(index,0);
            }
            index++;

        }
    }
    return QAbstractListModel::createIndex(0,0);
}


void SongModel::setSong(Song *s){
    // the view is repainted
    beginResetModel();
    this->song = s;
    endResetModel();
}
