#include "grammarloader.h"

#include <iostream>
#include <fstream>
#include "measure.h"
#include "songPart.h"


std::unique_ptr<Grammar> GrammarLoader::loadGrammar(std::string fileName, TerminalObject *sample)
{
    //grammar being constructed
    std::unique_ptr<Grammar> grammar = std::unique_ptr<Grammar>(new Grammar());

    std::ifstream myFile;
    std::string line;

    myFile.open(fileName);

    if(myFile.is_open()){
        try{
            //first three lines hold number of terminals, AND nonterminals and OR nonterminals respectively
            getline (myFile,line);
            int termSize = std::stoi(line);
            getline (myFile,line);
            int andSize = std::stoi(line);
            getline (myFile,line);
            int orSize = std::stoi(line);

            std::vector<Terminal*> parsedTerms;
            std::vector<AndNonterminal*> parsedAnds;
            // we need to insert valid OrNonterminal pointers to constructed AndNonterminals
            // but OrNonterminals are parses AFTER the AndNonterminals
            // thus we cannot allocate them during parsing, we have to do it beforehand
            // and then move them to the grammar "manually"
            std::vector<std::unique_ptr<OrNonterminal>> parsedOrs;

            parsedTerms.reserve(termSize);
            parsedAnds.reserve(andSize);
            parsedOrs.reserve(orSize);

            for (int i = 0; i < termSize; ++i) {
                parsedTerms.emplace_back();
            }
            for (int i = 0; i < andSize; ++i) {
                parsedAnds.emplace_back();
            }
            for (int i = 0; i < orSize; ++i) {
                parsedOrs.emplace_back(std::unique_ptr<OrNonterminal>(new OrNonterminal()));
            }

            while ( getline (myFile,line) )
            {
                if(line.empty())
                    continue;

                std::stringstream ss(line);
                std::string word;
                int index;

                //first word on the line is either T, AND or OR
                ss >> word;

                if(word.compare("T") == 0){

                    //terminals are separated by "_" instead of " " for easier parsing
                    std::getline(ss,word,'_');
                    try{
                        index = std::stoi(word);
                    }
                    catch(const std::invalid_argument&){
                        continue;
                    }


                    std::getline(ss,word,'_');
                    //word now holds the string representation of a terminalObject stored in the grammar

                    // parse the string depending on what terminalObject is actually stored in grammar
                    // for that purpose we use pointer to sample terminalObject
                    // and use it for parsing (making use of polymorphism)
                    Terminal t(sample->parseString(word));

                    parsedTerms[index] = grammar->addTerminal(std::move(t));

                }else if(word.compare("AND") == 0){
                    int left, right;
                    ss >> index >> left >> right;
                    parsedAnds[index-termSize] = grammar->addAndNonterminal(AndNonterminal(parsedOrs[left].get(), parsedOrs[right].get()));

                }else if(word.compare("OR") == 0){
                    ss >> index;
                    int child,weight;
                    while(ss >> child >> weight){
                        // index is shared for terminals and and nonterminals
                        // decide what is actually indexed by checking with termSize
                        if(child < termSize){
                            parsedOrs[index]->addChild(parsedTerms[child], weight);
                        } else{
                            parsedOrs[index]->addChild(parsedAnds[child-termSize], weight);
                        }
                    }

                }else if(word.compare("SOR") == 0){
                    ss >> index;
                    try{
                        grammar->setStartSymbol(parsedOrs.at(index).get());
                    }
                    catch(const std::out_of_range&){}
                }
            }
            //manually move OrNonterminal pointers to grammar
            for(auto& o : parsedOrs){
                grammar->insertOrNonterminal(o);
            }
        }
        catch(const std::invalid_argument&){
        }

        myFile.close();
    } else std::cerr << "Unable to open file";
    return grammar;

}

