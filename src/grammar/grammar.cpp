#include "grammar.h"
#include <iostream>
#include <fstream>
#include "measure.h"
#include "songPart.h"
#include <map>

// writes grammar to text file so it can be later loaded with GrammmarLoader
// symbols are indexed (one index for terminals and AND nonterminals, second for OR nonterminals)
// children of nonterminals are represented by corresponding indexes
void Grammar::writeToFile(std::string fileName) {
    if(isEmpty()){
        return;
    }
    std::ofstream of;

    of.open(fileName);
    if(!of.is_open()){return;}

    of << terminals.size() << std::endl;
    of << andNonterminals.size() << std::endl;
    of << orNonterminals.size() << std::endl;

    std::map<OrNonterminal*, int> orMap;
    // orNontermial's children can be either Terminal or AndNonterminal
    // we wont differentiate, just use a shared index for both
    std::map<Symbol*, int> termAndMap;

    int index = 0;
    for (const auto& orNonterminal : orNonterminals) {
        // we cant write them to file yet since we dont know indices of children symbols
        orMap[orNonterminal.get()] = index;
        index++;
    }

    index = 0;
    for (const auto& term : terminals) {
        termAndMap[term.get()] = index;
        of << "T " << index << "_" << term->toString() << std::endl;
        index++;
    }

    for (const auto& andNonterminal : andNonterminals) {
        termAndMap[andNonterminal.get()] = index;
        of << "AND " << index << " "
           << orMap[andNonterminal->getLeft()]
           << " "
           << orMap[andNonterminal->getRight()]
           << std::endl;
        index++;
    }

    //now we can output OrNonterminals
    for (const auto& orNonterminal : orNonterminals) {
        of << "OR " << orMap[orNonterminal.get()] << " ";
        for(auto& child : orNonterminal->getChildren()){
           of << termAndMap[child.first] << " " << child.second << " ";
        }
        of << std::endl;
    }

    of << "SOR " << orMap[startSymbol_] << std::endl;
    of.close();
}

AndNonterminal* Grammar::addAndNonterminal(AndNonterminal newNonterminal) {
    //check if pointer to this term is already in set, return it
    for (const auto& andNonterminal : andNonterminals) {
        if(andNonterminal->equal(newNonterminal)){
            return andNonterminal.get();
        }
    }
    // or create new pointer, insert and return
    std::unique_ptr<AndNonterminal> n = std::unique_ptr<AndNonterminal>(new AndNonterminal(newNonterminal));
    auto it = andNonterminals.emplace(std::move(n));
    return it.first->get();
}

OrNonterminal* Grammar::addOrNonterminal(OrNonterminal newNonterminal) {
    //check if pointer to this term is already in set, return it
    for (const auto& orNonterminal : orNonterminals) {
        if(orNonterminal->equal(newNonterminal)){
            return orNonterminal.get();
        }
    }
    // or create new pointer, insert and return
    std::unique_ptr<OrNonterminal> n = std::unique_ptr<OrNonterminal>(new OrNonterminal(newNonterminal));
    auto it = orNonterminals.emplace(std::move(n));
    return it.first->get();
}

Terminal* Grammar::addTerminal(Terminal newTerminal) {
    //check if pointer to this term is already in set, return it
    for (const auto& term : terminals) {
        if(term->equal(newTerminal)){
            return term.get();
        }
    }
    // or create new pointer, insert and return
    std::shared_ptr<Terminal> t = std::make_shared<Terminal>(std::move(newTerminal));
    terminals.insert(t);
    return t.get();
}
