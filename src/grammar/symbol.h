#ifndef SYMBOL_H
#define SYMBOL_H
#include <string>
#include <functional>
#include <vector>

//base abstract class for terminals and nonterminals
class Symbol
{
public:
    Symbol(){}
    ~Symbol(){}

    virtual std::string toString() const {return "";}

    // expands the current symbol and adds the result to constructedSequence
    virtual void expand(std::vector<Symbol*>& constructedSequence) = 0;

};


//custom hash function (needed for SSMatrix)
namespace std
{
     template <>
     struct hash<std::pair<Symbol*,Symbol*>>
     {
         size_t operator()(const std::pair<Symbol*,Symbol*>& k) const
         {
             std::hash<Symbol*> fn;
             std::size_t h = fn(k.first)*3 ^ fn(k.second);
             return h;
         }
     };
}

#endif // SYMBOL_H
