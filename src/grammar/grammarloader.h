#ifndef GRAMMARLOADER_H
#define GRAMMARLOADER_H

#include "grammar.h"

//provides method for loading grammar from text file
class GrammarLoader
{
public:
    GrammarLoader(){}
    ~GrammarLoader(){}

    // takes string name of a file with a grammar and a sample of TerminalObject saved in the grammar
    // returns loaded grammar and transfers ownership
    std::unique_ptr<Grammar> loadGrammar(std::string fileName, TerminalObject* sample);
};

#endif // GRAMMARLOADER_H
