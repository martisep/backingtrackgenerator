#include "andnonterminal.h"
#include "ornonterminal.h"


std::string AndNonterminal::toString() const{
    return "([" + left_->toString() + "] + [" + right_->toString() + "])";
}

void AndNonterminal::expand(std::vector<Symbol *> &constructedSequence){
    left_->expand(constructedSequence);
    right_->expand(constructedSequence);
}



