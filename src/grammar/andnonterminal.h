#ifndef ANDNODE_H
#define ANDNODE_H

#include "symbol.h"
#include <memory>
#include <unordered_set>

// forward declaration to resolve cyclic dependency
class OrNonterminal;


//represents an AND nonterminal
class AndNonterminal : public Symbol
{
public:
    AndNonterminal(){}
    AndNonterminal(OrNonterminal* left, OrNonterminal* right) : left_(left), right_(right) {}
    ~AndNonterminal(){}

    OrNonterminal* getLeft() const{ return left_; }
    OrNonterminal* getRight() const { return right_; }

    std::unordered_set<OrNonterminal*>& getParents(){
        return parents_;
    }

    bool equal(const AndNonterminal& other) const{
        return ((left_ == other.getLeft()) && (right_ == other.getRight()));
    }

    void addParent(OrNonterminal* o){
        parents_.insert(o);
    }

    virtual std::string toString() const;

    //expands this node to terminals
    virtual void expand(std::vector<Symbol*>& constructedSequence);

private:
    //children
    OrNonterminal* left_;
    OrNonterminal* right_;
    //parents
    std::unordered_set<OrNonterminal*> parents_;
};

#endif // ANDNODE_H
