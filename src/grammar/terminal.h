#ifndef TERM_H
#define TERM_H

#include "symbol.h"
#include "terminalObject.h"
#include <string>

// represents terminal in grammar
class Terminal : public Symbol
{
public:
    Terminal(){}
    Terminal(std::unique_ptr<TerminalObject> o) : object(std::move(o)){}
    ~Terminal(){}

    //copy assignment and constructor are forbidden
    Terminal(Terminal const &) = delete;
    Terminal &operator=(Terminal const &) = delete;

    //move assignment and constructor (because we use unique_ptr as member)
    Terminal(Terminal &&t) : object(std::move(t.object)) {}
    Terminal &operator=(Terminal &&t)
    {
        if (this != &t)
        {
            object = std::move(t.object);
        }
        return *this;
    }


    virtual std::string toString() const{
        return object->toString();
    }

    //returns pointer to itself
    virtual void expand(std::vector<Symbol*>& constructedSequence){
        constructedSequence.push_back(this);
    }

    bool equal(Terminal& t) {
        return this->getTerminalObject()->equal(t.getTerminalObject());
    }

    // get observer pointer to contained TerminalObject
    TerminalObject* getTerminalObject(){
        return object.get();
    }
private:
    // contained terminal object (can be anything implementing TerminalObject)
    std::unique_ptr<TerminalObject> object;
};

#endif // TERM_H
