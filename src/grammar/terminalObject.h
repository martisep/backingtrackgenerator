#ifndef TERMOBJECT_H
#define TERMOBJECT_H

#include <string>
#include <memory>

//abstract class for object stored in terminals
class TerminalObject {
public:
    TerminalObject(){}
    virtual ~TerminalObject(){}

    virtual std::string toString() const = 0;
    virtual bool equal(TerminalObject*) const = 0;
    virtual std::unique_ptr<TerminalObject> parseString(std::string s) = 0;
 };


#endif //TERMOBJECT_H
