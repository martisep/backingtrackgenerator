#ifndef ORNODE_H
#define ORNODE_H

#include <vector>
#include <sstream>
#include <string>
#include <unordered_map>
#include <memory>
#include <random>
#include <unordered_set>

#include "symbol.h"
#include "andnonterminal.h"


//represents OR nonterminal
class OrNonterminal : public Symbol
{
public:
    OrNonterminal() : sumWeights(0) {}

    //constructs new instance with given children
    OrNonterminal(std::unordered_map<Symbol*, int> children);
    ~OrNonterminal(){}

    //adds child with given weight
    void addChild(Symbol* s, int weight = 1);
    void addParent(AndNonterminal* a){
        parents_.insert(a);
    }
    void removeParent(AndNonterminal* a){
        parents_.erase(a);
    }

    //getters
    const std::unordered_set<AndNonterminal*>& getParents() const { return parents_; }
    const std::unordered_map<Symbol*, int>& getChildren() const { return children_; }
    int getSumWeights() const{ return sumWeights; }

    virtual std::string toString() const;

    bool equal(const OrNonterminal& o) {
        return (sumWeights == o.sumWeights) && (children_ == o.getChildren());
    }

    //expands to terminals, chooses child to expand randomly according to weights
    virtual void expand(std::vector<Symbol*>& constructedSequence);

private:
    std::unordered_map<Symbol*, int> children_;
    std::unordered_set<AndNonterminal*> parents_;
    int sumWeights;
};

#endif // ORNODE_H
