#ifndef GRAMMAR_H
#define GRAMMAR_H

#include <unordered_set>
#include "andnonterminal.h"
#include "ornonterminal.h"
#include "terminal.h"
#include <memory>

// class representing a context-free grammar in AND-OR form
// rules are represented directly as AND or OR nonterminals
// there is always exactly one AND -> OR1 OR2 rule for any AND nonterminal
// rules OR1 -> a, OR1 -> AND, ... are joind to one multirule OR1 -> a | AND1 | ...
class Grammar
{
public:
    Grammar(){
        startSymbol_ = nullptr;
    }

    //constructs grammar with given set of terminals and no nonterminals
    Grammar(const std::unordered_set<std::shared_ptr<Terminal>>& t){
        startSymbol_ = nullptr;
        terminals = t;
    }
    ~Grammar(){}

    std::vector<Symbol*> generateSentence() const{
        std::vector<Symbol*> constructedSequence;
        if(startSymbol_ != nullptr){
            startSymbol_->expand(constructedSequence);
        }
        return constructedSequence;
    }

    //adding functions return pointer to the newly added member or to existing duplicite one
    AndNonterminal* addAndNonterminal(AndNonterminal newNonterminal);
    OrNonterminal* addOrNonterminal(OrNonterminal node);
    Terminal* addTerminal(Terminal newTerminal);

    //directly inserts pointer (used in GrammarLoader)
    void insertOrNonterminal(std::unique_ptr<OrNonterminal>& o){
        orNonterminals.insert(std::move(o));
    }

    void setStartSymbol(OrNonterminal* startSymbol){
        startSymbol_ = startSymbol;
    }

    OrNonterminal* getStartSymbol() const{
        return startSymbol_;
    }

    bool isEmpty() const {
        return (startSymbol_ == nullptr);
    }

    const std::unordered_set<std::unique_ptr<OrNonterminal>>& getOrNonterminals() const {
        return orNonterminals;
    }
    const std::unordered_set<std::unique_ptr<AndNonterminal>>& getAndNonterminals() const {
        return andNonterminals;
    }
    void clear(){
        andNonterminals.clear();
        orNonterminals.clear();
        terminals.clear();
        startSymbol_ = nullptr;
    }

    //writes the grammar to file in format readable by GrammarLoader
    void writeToFile(std::string fileName);
private:
    //nonterminals
    std::unordered_set<std::unique_ptr<AndNonterminal>> andNonterminals;
    std::unordered_set<std::unique_ptr<OrNonterminal>> orNonterminals;

    //terminals (shared ownership with Corpus if inferred by PCFG-BCL)
    std::unordered_set<std::shared_ptr<Terminal>> terminals;

    //startSymbol (contained in orNonterminals)
    OrNonterminal* startSymbol_;
};

#endif // GRAMMAR_H
