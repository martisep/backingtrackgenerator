#include "ornonterminal.h"
#include <chrono>


OrNonterminal::OrNonterminal(std::unordered_map<Symbol *, int> children){
    children_ = children;
    sumWeights = 0;
    for(const auto& child : children){
        sumWeights += child.second;
    }
}

void OrNonterminal::addChild(Symbol* s, int weight)
{
    children_[s] += weight;
    sumWeights+= weight;
}

std::string OrNonterminal::toString() const{
    std::stringstream ss;
    for(const auto& child : children_){
        ss << child.first->toString() << " | ";
    }
    //remove ending " | "
    return ss.str().substr(0,ss.str().size()-3);
}

void OrNonterminal::expand(std::vector<Symbol *> &constructedSequence){
    if(children_.empty()){
        return;
    }
    //engine initialized only once
    //using system time as seed (previous implementation with random_device() as seed generated the same sequence each time)
    static std::mt19937 mt(std::chrono::system_clock::now().time_since_epoch().count());
    static std::uniform_int_distribution<int> dist;

    //weights change with each run
    std::uniform_int_distribution<int>::param_type p(0, sumWeights);

    int seed = dist(mt,p);

    auto it = children_.begin();
    int tmpSum = it->second;

    //randomly choose child
    while(tmpSum < seed){
        it++;
        tmpSum+= it->second;
    }
    it->first->expand(constructedSequence);
}


