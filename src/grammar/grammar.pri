DEPENDPATH += $$PWD
INCLUDEPATH += $$PWD

SOURCES += $$PWD/grammarloader.cpp \
    $$PWD/grammar.cpp \
    $$PWD/ornonterminal.cpp \
    $$PWD/andnonterminal.cpp

HEADERS += $$PWD/terminalObject.h \
    $$PWD/symbol.h \
    $$PWD/grammar.h \
    $$PWD/ornonterminal.h \
    $$PWD/andnonterminal.h \
    $$PWD/terminal.h \
    $$PWD/grammarloader.h
