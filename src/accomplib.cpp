#include "accomplib.h"

#include <iostream>
#include <bitset>
#include <random>
#include <vector>
#include <string>
#include <chrono>
#include <iostream>
#include <fstream>


#include <QMidiOut.h>
#include "grammarloader.h"


//anonymous namespace for private helper functions
namespace {

//note is a specific MIDI note number (not to be confused with Chord::Tone or MidiNote)
using note = unsigned short;

const note BOTTOM_OF_BASS_RANGE = 28; // e1
const note TOP_OF_BASS_RANGE = 55; // g3
const double ALPHA = 3; // coefficient of how much we prefer notes closer to target note (for generating basslines)

//standard value of midi resolution
const unsigned short MIDI_RESOLUTION = 96;

//channels for instruments
const unsigned short PIANO_CHANNEL = 0;
const unsigned short BASS_CHANNEL = 3;
const unsigned short DRUMS_CHANNEL = 9; //channel 9 is dedicated to drums by General MIDI standard


//directory with grammars for accompaniment generation
const std::string ACCOMP_DIR = "accomp_grammars";



//T is any type convertible to double (used double and int)
template<typename T>
int rndChooseIndex(const std::vector<T>& weights){
    //engine initialized only once
    static std::mt19937 mt(std::chrono::system_clock::now().time_since_epoch().count());
    static std::discrete_distribution<int> disc;

    //weights change with each run
    std::discrete_distribution<int>::param_type p(weights.begin(), weights.end());

    return disc(mt,p);
}

// randomly choose note according to given distances
note rndChooseNoteByDistance(const std::vector<std::pair<note,unsigned short>> distances){
    std::vector<double> weights;
    weights.reserve(distances.size());
    std::transform(distances.begin(),
                   distances.end(),
                   std::back_inserter(weights),
                   [](const std::pair<note,unsigned short>& p){
        if(p.second == 0)
            return 0.0;
        return 1 / (double)(p.second * p.second);
    });
    return distances.at(rndChooseIndex(weights)).first;
}


unsigned short distance(note a, note b){
    return ((int)(a - b)) > 0
            ? a - b
            : b - a;
}


// scale approach strategy for generating the bassline
note scaleApproach(note firstNote, note targetNote, std::bitset<Chord::OCTAVE> scale, Chord::Tone key){
    std::vector<std::pair<note,unsigned short>> distances;

    //only consider notes within an octave from targetNote AND within instrument range
    note lower_bound = targetNote - 12 >= BOTTOM_OF_BASS_RANGE
            ? targetNote - 12
            :BOTTOM_OF_BASS_RANGE;
    note upper_bound = targetNote + 12 <= TOP_OF_BASS_RANGE
            ? targetNote + 12
            :TOP_OF_BASS_RANGE;
    for (note n = lower_bound; n <= upper_bound; ++n) {
        //check if note is in given scale
        if(scale[(n - key - AccompLib::LOW_A) % 12]){
            //compute its distance to both first and target note (combination)
            unsigned short compDistance = distance(firstNote,n) + ALPHA * distance(n,targetNote);
            distances.push_back(std::make_pair(n,compDistance));
        }
    }
    //choose approach note by computed distances
    return rndChooseNoteByDistance(distances);

}

// chord approach strategy for generating the bassline
note chordApproach(note firstNote, note nextNote, Chord currentChord, Chord::Tone key){
    // use scale approach with scale created from current chord
    return scaleApproach(firstNote, nextNote, currentChord.toBitset(), currentChord.getToneOfHarmonicFunction(Chord::ROOT, key));
}

// chromatic approach strategy for generating the bassline
note chromaticApproach(note firstNote, note nextNote){
    // choose neighboring note so that the sequence is monotonic
    return firstNote < nextNote ? nextNote - 1 : nextNote + 1;
}

//recirsively fills the sequence with notes based on current chord and scale
void fillApproachSequence(note firstNote, note nextNote, Chord currentChord, std::bitset<12> scale, Chord::Tone key,
                          std::vector<note>& approachSequence, unsigned short startPos, unsigned short endPos, ParamObject* paramObject){
    int sequenceLength = endPos - startPos + 1;
    if (sequenceLength <= 0)
        return;
    if (sequenceLength == 1){ // one sixteenth note, we go no deeper
        approachSequence.at(startPos) = firstNote;
        return;
    }

    //decide how to divide given sequence
    int subsectionSize = 2;
    while (subsectionSize < sequenceLength){
        subsectionSize *= 2;
    }

    int numApproachNotes = 0;
    while ((sequenceLength % subsectionSize != 0) || (numApproachNotes <= 0)){ //for example, half notes will not fit to 3/4 bar, but quarter notes will
        subsectionSize /= 2;
        numApproachNotes = (sequenceLength / subsectionSize) - 1;
    }

    //we wont actually generate multiple approach notes simultaneously (for example for 3/4 bar), we rather create unevenly sized subsections

    // choose approach according to depth
    // five strategies: chord approach, scale approach, chromatic approach, same approach, no approach

    int approachStrategyIndex = 0;
    int approachNote;

    //half notes
    if (subsectionSize >= 8){
        approachStrategyIndex = rndChooseIndex(paramObject->getBassWeights().at(0));
    }
    //quarter notes
    else if (subsectionSize >= 4){
        approachStrategyIndex = rndChooseIndex(paramObject->getBassWeights().at(1));
    }
    //eighth notes
    else if (subsectionSize >= 2){
        approachStrategyIndex = rndChooseIndex(paramObject->getBassWeights().at(2));
    }
    //sixteenth notes
    else {
        approachStrategyIndex = rndChooseIndex(paramObject->getBassWeights().at(3));
    }

    //apply chosen strategy
    switch (approachStrategyIndex)
    {
    case 0:
        approachNote = chordApproach(firstNote, nextNote, currentChord,key);
        break;
    case 1:
        approachNote = scaleApproach(firstNote, nextNote, scale, key);
        break;
    case 2:
        approachNote = chromaticApproach(firstNote, nextNote);
        break;
    case 3:
        //"same approach"
        approachNote = firstNote;
        break;
    case 4:
    default:
        //no approach notes, we stop the recursion and return
        approachSequence.at(startPos) = firstNote;
        return;
    }

    // divide sequence and recurse
    // first subsequence can be longer, for example 3/4 bar will be divided to 2/4 (length 8) and 1/4 (length 4)
    fillApproachSequence(firstNote, approachNote, currentChord, scale, key, approachSequence, startPos, startPos + sequenceLength - subsectionSize - 1, paramObject);
    fillApproachSequence(approachNote, nextNote, currentChord, scale, key, approachSequence, startPos + sequenceLength - subsectionSize, startPos + sequenceLength - 1, paramObject);
}

// choose which note will be played first in a sequence
Chord::Tone chooseBassTone(const Chord &chord, Chord::Tone key, ParamObject* paramObject){
    //if the bass note is explicitely specified, use it
    if(chord.hasBass()){
        return chord.getBass(key);
    }
    // else choose one of the harmonic functions of the chord according to weights specified in paramObjects
    int j = rndChooseIndex(paramObject->getBassToneOfChordWeights());
    //get tone given by the harmonic function in given key
    return chord.getToneOfHarmonicFunction(Chord::HarmonicFunction(j), key);
}


//generate bassline for given chord
unsigned int generateBassline(Chord::Tone key, Chord currentChord, unsigned short targetNote, int sequenceLengthInTicks, int currentTick, QMidiFile* midi_file, int track, ParamObject* paramObject){

    //basic major scale (read from right to left)
    std::bitset<12> scale("101010110101");

    //convert length of sequence from ticks to sixteenth notes
    int sequenceLength = sequenceLengthInTicks / MIDI_RESOLUTION * 4;

    if (sequenceLength == 0){
        return targetNote;
    }

    //choose bass tone...
    Chord::Tone firstTone = chooseBassTone(currentChord, key, paramObject);

    //... and in decide in which octave to play it (based on distance to target note)
    note lower_bound = AccompLib::LOW_A + firstTone;
    while(lower_bound < BOTTOM_OF_BASS_RANGE){
        lower_bound += 12;
    }
    std::vector<std::pair<note,unsigned short>> distances;
    for (note n = lower_bound; n <= TOP_OF_BASS_RANGE; n += 12) {
        distances.push_back(std::make_pair(n,distance(targetNote,n)));
    }
    note firstNote = rndChooseNoteByDistance(distances);



    // bassline is represented as a vector of integers of length sequenceLength, where number means new MIDI note
    // and 0 means continuation of previous note
    // for example [40,0,0,0,51,0,30,0,40,0,22,21]
    std::vector<note> approachSequence(sequenceLength, 0);

    //recursively fill the sequence with notes
    fillApproachSequence(firstNote, targetNote, currentChord, scale, key, approachSequence, 0, sequenceLength - 1, paramObject);


    //insert notes into MIDI file
    int currentNote = -1;
    for(int note : approachSequence){
        if(note == 0){
            //sixteenth rest
            currentTick+= MIDI_RESOLUTION / 4;
            continue;
        } else{
            //it is new note, first switch off the current one
            //unless it is the beginning of the sequence
            if(currentNote != -1){
                // noteOff is always one tick sooner
                // so it doesnt get switched with noteOn on same beat
                midi_file->createNoteOffEvent(track,currentTick-1,3,currentNote);
            }
            currentNote = note;
            midi_file->createNoteOnEvent(track,currentTick,3,currentNote, 100);
            currentTick+= MIDI_RESOLUTION /4;
        }
    }
    //switch of final note after parsing the whole sequence
    midi_file->createNoteOffEvent(track,currentTick-1,3,currentNote);

    return firstNote;
}

//generate final chord of a song (major chord from key)
unsigned int generateFinalChord(int currentTick, Chord::Tone key, QMidiFile* midi_file, int drumTrack, int bassTrack, int pianoTrack){

    float endTick = currentTick + 8 * MIDI_RESOLUTION; //final chord will be 2 bars long (2 bars = 8 quarter notes)

    // crash (49) and bassdrum (36)
    midi_file->createNote(drumTrack, currentTick , endTick ,9,49,100,100);
    midi_file->createNote(drumTrack, currentTick , endTick ,9,36,100,100);

    // create base note from key
    unsigned int bassNote = AccompLib::LOW_A + Chord::OCTAVE + (int)key;
    midi_file->createNote(bassTrack, currentTick, endTick, 3,
                          bassNote,
                          100,100);

    // base for piano is two octaves above low A
    unsigned int baseA = AccompLib::LOW_A + 2*Chord::OCTAVE;

    // create major chord from key
    midi_file->createNote(pianoTrack, currentTick, endTick, 0,
                          baseA + (int)key,
                          100,100);
    midi_file->createNote(pianoTrack, currentTick, endTick, 0,
                          baseA + (int)key + Chord::MAJOR3,
                          100,100);
    midi_file->createNote(pianoTrack, currentTick, endTick, 0,
                          baseA + (int)key + Chord::PERFECT5,
                          100,100);
    midi_file->createNote(pianoTrack, currentTick, endTick, 0,
                          baseA + (int)key + Chord::OCTAVE,
                          100,100);
    return bassNote;

}

//create string from genre and instrument (for opening grammar files)
std::string getGenreInstrumentString(AccompLib::TGenre genre, AccompLib::TInstrument instrument){
    std::string genreString = "";
    switch (genre) {
    case AccompLib::ROCK:
        genreString = "rock";
        break;
    case AccompLib::SWING:
        genreString = "swing";
        break;
    case AccompLib::BLUES:
        genreString = "blues";
        break;
    default:
        break;
    }

    std::string instrumentString;
    switch (instrument) {
    case AccompLib::DRUMS:
        instrumentString = "drums";
        break;
    case AccompLib::PIANO:
        instrumentString = "piano";
        break;
    default:
        break;
    }
    return genreString + "_" + instrumentString;
}


// load post-generative rewriting rules from file corresponding to genre and intrument (if such exists)
void loadPostMappings(AccompLib::TInstrument instrument, AccompLib::TGenre genre, std::unordered_map<int,int>& postMappings){
    std::string filename = getGenreInstrumentString(genre, instrument);
    std::ifstream myFile;
    myFile.open(ACCOMP_DIR + "/" + filename + "_post");
    if(myFile.is_open()){
        std::string line;
        int left, right, weight;
        while(std::getline(myFile, line)){
            std::stringstream ss(line);
            // format:
            // note_to_be_rewritten possible_new_note_1 weight1 possible_new_note_2 weight2 ...
            ss >> left;
            std::vector<int> rights, weights;

            while(ss >> right >> weight) {
                rights.push_back(right);
                weights.push_back(weight);
            }
            postMappings[left] = rights.at(rndChooseIndex(weights));
        }
        myFile.close();
    }
}

//generate drums or piano midi sequence from grammar and insert it to given midi file
void generateMidiSequenceFromGrammar(Grammar* g, AccompLib::TInstrument instrument, AccompLib::TGenre genre, Chord::Tone key, Chord currentChord, int sequenceLength,
                              int currentTick, QMidiFile* midi_file, int track, int channel)
{
    //generate sentence from grammar
    if(g == nullptr)
        return;
    std::vector<Symbol*> sentence = g->generateSentence();
    if(sentence.empty())
        return;


    // load postprocessing rules - rules for certain symbols that are applied ON ALL APPEARANCES SIMULTANEOUSLY
    // (similarly to L-systems)
    // for example all appearances of open hi-hat can be changed to ride cymbal

    std::unordered_map<int,int> postMappings;
    loadPostMappings(instrument, genre, postMappings);

    //current length of constructed sequence in sixteenth notes
    int currSequenceLength = 0;

    //repeat until the whole section is filled with notes
    while(true){
        for(auto item : sentence){
            Terminal* t = dynamic_cast<Terminal*>(item);
            AccompLib::MidiNote* d = dynamic_cast<AccompLib::MidiNote*>(t->getTerminalObject());

            int note = d->getNote();

            //apply post-generative rules
            auto it = postMappings.find(note);
            if(it != postMappings.end()){
                note = it->second;
            }

            //if the terminal denotes a rest, add corresponding length to currSequenceLength
            switch (note) {
            case AccompLib::MidiNote::SIXTEENTH_REST:
                currSequenceLength+= MIDI_RESOLUTION / 4;
                continue;
            case AccompLib::MidiNote::EIGHT_SWING_SHORT_REST:
                currSequenceLength+= MIDI_RESOLUTION / 3;
                continue;
            case AccompLib::MidiNote::EIGHT_SWING_LONG_REST:
                currSequenceLength+= MIDI_RESOLUTION * 2 / 3;
                continue;
            default:
                break;
            }

            // translate piano harmonic symbols to specific MIDI note
            if(instrument == AccompLib::PIANO){
                int rootNote = AccompLib::LOW_A + currentChord.getToneOfHarmonicFunction(Chord::ROOT,key) + 2*Chord::OCTAVE;

                switch (note) {
                case AccompLib::MidiNote::ROOT:
                    note = rootNote;
                    break;
                case AccompLib::MidiNote::THIRD:
                    note = rootNote + currentChord.getThird();
                    break;
                case AccompLib::MidiNote::FITFH:
                    note = rootNote + currentChord.getFifth();
                    break;
                case AccompLib::MidiNote::SEVENTH:
                    note = rootNote + currentChord.getSeventh();
                    if(note == rootNote) //NULL7
                        continue;
                    break;
                case AccompLib::MidiNote::NINTH:
                    note = rootNote + currentChord.getNinth();
                    if(note == rootNote) //NULL9
                        continue;
                    break;
                case AccompLib::MidiNote::ELEVENTH:
                    note = rootNote + currentChord.getEleventh();
                    if(note == rootNote) //NULL11
                        continue;
                    break;
                case AccompLib::MidiNote::THIRTEENTH:
                    note = rootNote + currentChord.getThirteenth();
                    if(note == rootNote) //NULL13
                        continue;
                    break;
                default:
                    break;
                }
            }

            if(currSequenceLength >= sequenceLength){
               return;
            }

            int startTick = currentTick+currSequenceLength;
            int endTick = currentTick + sequenceLength; //let the notes ring to the end of the sequence

            midi_file->createNote(track, startTick, endTick, channel, note, 100, 100);
        }
    }
}

}






// load bass strategy weight from file according to genre and save them in paramObject
void AccompLib::loadBassWeights(ParamObject* paramObject,AccompLib::TGenre genre)
{
    std::string filename = "";
    switch (genre) {
    case ROCK:
        filename = "rock_bass_weights";
        break;
    case SWING:
        filename = "swing_bass_weights";
        break;
    case BLUES:
        filename = "blues_bass_weights";
        break;
    default:
        return;
    }

    std::ifstream file;
    file.open(ACCOMP_DIR + "/" + filename);
    if(!file.is_open()){
        return;
    }

    std::string line;
    int weight;
    int depth = 0;
    int strategy = 0;
    while(std::getline(file,line)){
        std::stringstream ss(line);
        while(ss >> weight){
            paramObject->setBassWeight(depth, strategy++, weight);
        }
        strategy = 0;
        depth++;
    }
}


unsigned short AccompLib::getTempo(AccompLib::TGenre genre){
    switch (genre) {
    case BLUES:
        return BLUES_TEMPO;
    case ROCK:
        return ROCK_TEMPO;
    case SWING:
        return SWING_TEMPO;
    default:
        return ROCK_TEMPO;
    }
}



void AccompLib::generateAccompaniment(TGenre genre, Chord::Tone key, int tempo, const std::vector<SongPart>& songparts, QMidiFile* midi_file, ParamObject* paramObject)
{
    // default resolution is 96 ppq (pulses per quarter-note (pulse (=tick) is the atomic time unit))
    midi_file->setResolution(MIDI_RESOLUTION);

    //create tracks for instruments
    int pianoTrack = midi_file->createTrack();
    int drumTrack = midi_file->createTrack();
    int bassTrack = midi_file->createTrack();

    //set tempo
    midi_file->createTempoEvent(0,0,tempo);

    // set MIDI instrument numbers (33 = "fingered bass))
    // default is 0 = "acoustic piano"
    // no need to set drums - channel 9 (DRUM_CHANNEL) is dedicated to percussion by General MIDI standard
    midi_file->createProgramChangeEvent(bassTrack,0,BASS_CHANNEL,33);

    // get length of song in ticks
    // there are MIDI_RESOLUTION ticks in a quarter note
    int currentTick = 0;
    for(const SongPart& sp : songparts){
        for(const Measure& m : sp.getMeasures()){
            //getLength returns number of sixteenth notes
            currentTick += MIDI_RESOLUTION * m.getLength() / 4;
        }
    }

    //add final chord to the very end of the song (returns target note for the bass sequence)
    unsigned int targetNote = generateFinalChord(currentTick, key, midi_file, drumTrack,bassTrack,pianoTrack);

    //load grammar from file

    GrammarLoader gl;
    std::string genreInstrumentString = getGenreInstrumentString(genre,PIANO);
    std::unique_ptr<AccompLib::MidiNote> sample = std::unique_ptr<AccompLib::MidiNote>(new AccompLib::MidiNote());

    std::unique_ptr<Grammar> pianoGrammar = gl.loadGrammar(ACCOMP_DIR + "/" + genreInstrumentString, sample.get());

    //load grammar from file

    genreInstrumentString = getGenreInstrumentString(genre,DRUMS);

    std::unique_ptr<Grammar> drumGrammar = gl.loadGrammar(ACCOMP_DIR + "/" + genreInstrumentString, sample.get());

    // iterate the song backwards
    /* because generation of bassline works on "approaching" principle, the bassline for a measure gravitates
     * towards the first note of the next measure*/
    for(std::vector<SongPart>::const_reverse_iterator it = songparts.rbegin(); it != songparts.rend(); it++){
        int songpartlength = 0;

        for(std::vector<Measure>::const_reverse_iterator itt = it->getMeasures().rbegin(); itt != it->getMeasures().rend(); itt++){
            for(std::vector<std::pair<Chord, unsigned short>>::const_reverse_iterator ittt = itt->getChords().rbegin(); ittt != itt->getChords().rend(); ittt++){
                Chord c = ittt->first;
                int length = MIDI_RESOLUTION * ittt->second / 4;
                songpartlength+= length;
                //get on the beginning of current chord
                currentTick -= length;

                //generate bass (notes are directly inserted into midi_file)
                targetNote = generateBassline(key,c,targetNote,length,currentTick, midi_file,bassTrack,paramObject);

                // generate piano (notes are directly inserted into midi_file)
                generateMidiSequenceFromGrammar(pianoGrammar.get(), PIANO, genre, key, c, length,
                                                           currentTick, midi_file, pianoTrack, PIANO_CHANNEL);

            }
        }
        //generate drums
        generateMidiSequenceFromGrammar(drumGrammar.get(), DRUMS, genre, key, Chord(), songpartlength,
                                                   currentTick, midi_file, drumTrack, DRUMS_CHANNEL);
    }
}
