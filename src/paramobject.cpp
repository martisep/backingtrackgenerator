#include "paramobject.h"
#include "accomplib.h"

ParamObject::ParamObject()
{
    bassWeights = {
        {0,0,0,0,0},
        {0,0,0,0,0},
        {0,0,0,0,0},
        {0,0,0,0,0}
    };
    //default bass weights
    AccompLib::loadBassWeights(this);

    bassToneOfChordWeigths = { 100, 8, 15, 2, 0, 0, 0 };

    //default corpora
    usedCorpora[ParamObject::RS] = true;
    usedCorpora[ParamObject::REFSEG] = true;
    usedCorpora[ParamObject::IRB] = true;
    usedCorpora[ParamObject::IMAG] = false;

    //default values
    minLengthOfChordProgression = 4;
    minLengthOfSection = 8;
    minNumberOfSections = 3;

    ignoreSeventh = false;
    ignoreHigherHarmonies = true;
}


bool ParamObject::isCorpusUsed(ParamObject::TCorpora type) const{
    try{
        return usedCorpora.at(type);
    }
    catch(std::out_of_range){
        return false;
    }
}

void ParamObject::setCorpusUsed(ParamObject::TCorpora type, bool used){
    try{
        usedCorpora.at(type) = used;
    }
    catch(std::out_of_range){
        return;
    }
}

int ParamObject::getBassWeight(int depth, int strategy) const{
    try{
        return bassWeights.at(depth).at(strategy);
    }
    catch(std::out_of_range){
        return 0;
    }
}

void ParamObject::setBassWeight(int depth, int strategy, int weight){
    try{
        bassWeights.at(depth).at(strategy) = weight;
    }
    catch(std::out_of_range){
        return;
    }
}

