#ifndef BASSLIB_H
#define BASSLIB_H

#include "chord.h"
#include "paramobject.h"
#include "terminalObject.h"
#include "QMidiFile.h"
#include "songPart.h"


namespace AccompLib
{
    // midi number for note a0
    const unsigned short LOW_A = 21;

    //predefined default tempo for each style
    const unsigned short BLUES_TEMPO = 80;
    const unsigned short ROCK_TEMPO = 120;
    const unsigned short SWING_TEMPO = 170;

    enum TGenre {ROCK, SWING, BLUES};
    enum TInstrument {DRUMS, PIANO, BASS};

    //loads bass strategy weights from file according to genre and saves it to paramObject
    void loadBassWeights(ParamObject *paramObject, AccompLib::TGenre genre = BLUES);

    //returns default tempo for genre
    unsigned short getTempo(TGenre genre);

    //generates complete accompaniment for song (drums, bass, piano) and saves it to given MIDI file
    void generateAccompaniment(TGenre genre, Chord::Tone key, int tempo, const std::vector<SongPart>& songparts, QMidiFile* midi_file, ParamObject* paramObject);




    //terminal object of drum and piano grammars (just a simple integer)
    class MidiNote : public TerminalObject{
    public:
        // defined special symbols for rests (or more precise 'move in measure')
        const static int SIXTEENTH_REST = 0;
        const static int EIGHT_SWING_SHORT_REST = -1;
        const static int EIGHT_SWING_LONG_REST = -2;

        // defined values for diferent harmonic functions in the chord (used by piano grammars)
        enum {ROOT = 1, THIRD = 3, FITFH = 5, SEVENTH = 7, NINTH = 9, ELEVENTH = 11, THIRTEENTH = 13};

        MidiNote(){}
        MidiNote(std::string s)
        {
            try{
                note = std::stoi(s);
            }
            catch(const std::invalid_argument&){
                note = 0;
            }
        }
        ~MidiNote(){}

        virtual bool equal(TerminalObject* t) const{
            return (this->note == (dynamic_cast<MidiNote*>(t))->getNote());
        }
        virtual std::string toString() const{
            return std::to_string(note);
        }
        virtual std::unique_ptr<TerminalObject> parseString(std::string s){
                return std::unique_ptr<MidiNote>(new MidiNote(s));
        }
        int getNote(){
            return note;
        }
    private:
        int note;
    };
}


#endif // BASSLIB_H
