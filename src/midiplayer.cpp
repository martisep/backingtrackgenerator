#include "midiplayer.h"


void MidiPlayer::run()
{
    //convert beats to ticks
    qint64 startTime = midi_file->timeFromTick(midi_file->tickFromBeat((float)from)) * 1000;
    qint64 repeatFromTime = midi_file->timeFromTick(midi_file->tickFromBeat((float)repeatFrom)) * 1000;
    qint64 repeatToTime = midi_file->timeFromTick(midi_file->tickFromBeat((float)repeatTo)) * 1000;

    //start the timer
    QElapsedTimer t;
    t.start();

    int repeated = 0;

    QList<QMidiEvent*>* events = midi_file->events();
    for(int i = 0; i < events->count(); i++)
    {
        {
            //check if playback was stopped by user
            QMutexLocker locker(&mutex);
            if(stopped){
                break;
            }
        }
        QMidiEvent* midi_file_event = events->at(i);

        //ignore meta events in playback (lyrics, texts,...)
        if (midi_file_event->type() != QMidiEvent::Meta)
        {
            qint64 event_time = midi_file->timeFromTick(midi_file_event->tick()) * 1000;

            //on first repetion only play notes from specified starting point
            if((event_time < startTime) && repeated == 0){
                if(!midi_file_event->isNoteEvent()){
                    //process events such as program change, only skip note events
                    handleEvent(midi_file_event);
                }
                continue;
            }

            // if end of repeated segment is reached
            // and the number of repeats is not yet equal to repeatTimes
            if((event_time >= repeatToTime) && (repeated < repeatTimes)){
                // process noteOff events
                // (they are always before NoteOn events at the same time)
                if(midi_file_event->type() != QMidiEvent::NoteOff){
                    //restart the playback from repeatFromTime and increase the counter
                    t.restart();
                    startTime = repeatFromTime;
                    i=0;
                    repeated++;
                    continue;
                }
            }

            //wait until the time for the event has come
            qint32 waitTime = event_time - t.elapsed() - startTime;
            if(waitTime > 0) {
                thread()->msleep(waitTime);
            }

            handleEvent(midi_file_event);
        }
    }

    midi_out->disconnect();
}

void MidiPlayer::stop(){
    // user pressed the 'Stop' button - stop the playback right after finishing playing current note
    QMutexLocker locker(&mutex);
    stopped = true;
}


void MidiPlayer::handleEvent(QMidiEvent *midi_file_event)
{
    // ignore 'System Exclusive' messages
    if (midi_file_event->type() != QMidiEvent::SysEx){
        qint32 message = midi_file_event->message();
        midi_out->sendMsg(message);

        // inform the MainWindow that new beat is played
        float beat = midi_file->beatFromTick(midi_file_event->tick());

        //only beats (not shorter notes)
        if((beat - (int)beat) == 0){
            if(beat != currentBeat){
                currentBeat = beat;
                emit newBeat(beat);
            }
        }
    }
}
