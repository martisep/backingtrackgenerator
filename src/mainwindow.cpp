#include <stdio.h>
#include <QThread>
#include <QElapsedTimer>
#include <QCoreApplication>
#include <QMidiOut.h>
#include <QMidiFile.h>
#include <QProgressDialog>
#include <QFileDialog>
#include <QStandardPaths>
#include <QMessageBox>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "midiplayer.h"
#include "rockstudycorpusloader.h"
#include "irbcorpusloader.h"
#include "imaginarybookcorpusloader.h"
#include "refsegcorpusloader.h"
#include "grammarloader.h"
#include "settingsdialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    this->setWindowTitle("Backing-track generator");
    ui->setupUi(this);

    //ensure that keyPressed events are first handled by this class (for detecting spacebar)
    qApp->installEventFilter(this);

    //create custom model (we will populate it later, when we have generated the song)
    model = std::unique_ptr<SongModel>(new SongModel(this));
    //glue model and view together
    ui->chordsView->setModel(model.get());

    paramObject = std::unique_ptr<ParamObject>(new ParamObject());

    //load grammars from files, if available (save the time consuming process of inferring grammars on each run)
    loadGrammars();
}

MainWindow::~MainWindow()
{
    delete ui;
}


//updates chordsView to show currently played Measure as selected
void MainWindow::updateBeat(int beat){
    QModelIndex ind = model->getIndex(beat);
    if(ind.row() != ui->chordsView->currentIndex().row()){
        ui->chordsView->setCurrentIndex(model->getIndex(beat));
    }
}

// enables control elements in gui
void MainWindow::stoppedPlayback()
{
    ui->keyComboBox->setEnabled(true);
    ui->tempoSpinBox->setEnabled(true);
    ui->genreComboBox->setEnabled(true);
    ui->repeatSpinBox->setEnabled(true);

    // and changes 'Stop' button to 'Play' button
    ui->playBtn->setIcon(QIcon("play.png"));
    ui->playBtn->setText("Play");

}

// disables control elements in gui
void MainWindow::startedPlayback()
{
    ui->keyComboBox->setEnabled(false);
    ui->tempoSpinBox->setEnabled(false);
    ui->genreComboBox->setEnabled(false);
    ui->repeatSpinBox->setEnabled(false);

    // and changes 'Play' button to 'Stop' button
    ui->playBtn->setIcon(QIcon("stop.png"));
    ui->playBtn->setText("Stop");

}

//ensure that keyPressed events are first handled by this class (for detecting spacebar)
bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    //only keypress events
    if (event->type()==QEvent::KeyPress) {
        QKeyEvent* key = static_cast<QKeyEvent*>(event);
        // only spacebar
        if (key->key()==Qt::Key_Space) {
            playOrStop();
        } else {
            return QObject::eventFilter(obj, event);
        }
        return true;
    } else {
        return QObject::eventFilter(obj, event);
    }
    return false;
}


//play the song from given beat
void MainWindow::play(int from){
    if(song == nullptr){
        return;
    }

    if(midi_out == nullptr){
        midi_out = std::unique_ptr<QMidiOut>(new QMidiOut());
    }

    if(player != nullptr){
        //stop player if already playing and wait for the thread to finish
        player->stop();
        player->wait();
    }

    // set song parameters specified in gui control components
    song->setGenre((AccompLib::TGenre)ui->genreComboBox->currentIndex());
    song->setTempo((unsigned short)ui->tempoSpinBox->value());

    if(!song->hasValidMidi()){
        song->generateMIDI();
    }

    //connect to default MIDI device
    midi_out->connect("0");


    //repeat the whole song (possible future extension to user defined bounds for repetition)
    int repeatFrom = 0;
    int repeatTo = model->rowCount(QModelIndex()) * 4;

    //"repeat 2 times" means play once through and then repeat 1 more time
    int repeatTimes = ui->repeatSpinBox->value() - 1;

    //create new player, connect signals to slots and start
    player = std::unique_ptr<MidiPlayer>(
                new MidiPlayer(song->getMidiFile(), midi_out.get(), from, repeatFrom, repeatTo, repeatTimes)
                );

    connect(player.get(), SIGNAL(newBeat(int)), this, SLOT(updateBeat(int)));
    connect(player.get(), SIGNAL(finished()), this, SLOT(stoppedPlayback()));
    connect(player.get(), SIGNAL(started()), this, SLOT(startedPlayback()));

    player->start();
}

// updates the model connected to chordsView
void MainWindow::displaySong(){
    if(song == nullptr){
        return;
    }
    model->setSong(song.get());
}

// launches or stops the playback
void MainWindow::playOrStop()
{
    if(song == nullptr){
        return;
    }
    // if already playing, stop
    if(player != nullptr && player->isRunning()){
        player->stop();
    }
    // else play from selected measure
    else {
        int beat = model->data(ui->chordsView->currentIndex(), Qt::UserRole).toInt();
        play(beat);
    }
}

//loads grammars from files, if available
void MainWindow::loadGrammars()
{
    GrammarLoader gl;
    // GrammarLoader needs sample of terminal object stored in grammar to know how to parse the file
    std::unique_ptr<SongPart> sampleSongPart = std::unique_ptr<SongPart>(new SongPart());
    std::unique_ptr<Measure> sampleMeasure = std::unique_ptr<Measure>(new Measure());

    grammarSongparts = gl.loadGrammar(GRAMMAR_SONGPART_FILE, sampleSongPart.get());
    grammarChords = gl.loadGrammar(GRAMMAR_CHORD_FILE, sampleMeasure.get());
}


// creates new Song from grammars (if available) and displays it in chordsView
void MainWindow::generateSong()
{
    if(player != nullptr){
        //stop player if already playing and wait for the thread to finish
        player->stop();
        player->wait();
    }
    // if there are no grammars or they are empty, they need to be inferred again
    if((grammarSongparts->getStartSymbol() == nullptr)
            || grammarSongparts->getStartSymbol()->getSumWeights() == 0
            || (grammarChords->getStartSymbol() == nullptr)
            || grammarChords->getStartSymbol()->getSumWeights() == 0){

        //prompt user
        QMessageBox msgBox;
        msgBox.setText("Failed to load grammars from file. Please infer the grammars from corpora.");
        msgBox.setInformativeText("Do you want to initiate the inferring procedure?");
        msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        msgBox.setWindowTitle("Missing grammars.");
        msgBox.setIcon(QMessageBox::Warning);

        switch (msgBox.exec()) {
          case QMessageBox::Ok:
              inferGrammars();
              return;
          case QMessageBox::Cancel:
              return;
          default:
              return;
        }
    }

    // create song
    song = std::unique_ptr<Song>(
                new Song(
                    grammarSongparts.get(),
                    grammarChords.get(),
                    (Chord::Tone)ui->keyComboBox->currentIndex(),
                    (unsigned short)ui->tempoSpinBox->value(),
                    (AccompLib::TGenre)ui->genreComboBox->currentIndex(),
                    paramObject.get()));

    displaySong();

    //reset the selected chord on a view
    ui->chordsView->setCurrentIndex(QModelIndex());
}

// launches 'Save to MIDI' dialog
void MainWindow::saveToMidi(){
    if(song == nullptr){
        // no song, prompt user
        QMessageBox msgBox;
        msgBox.setText("There is no song to be saved.");
        msgBox.setInformativeText("Do you want to generate a song?");
        msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        msgBox.setWindowTitle("No song generated");
        msgBox.setIcon(QMessageBox::Warning);

        switch (msgBox.exec()) {
          case QMessageBox::Ok:
              generateSong();
              return;
          case QMessageBox::Cancel:
              return;
          default:
              return;
        }
    }

    QString filename = QFileDialog::getSaveFileName(this, "Save to MIDI file",
                                                    QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + QDir::separator() + "myBackingTrack",
                                                    "MIDI file (*.mid)");
    if(song->getMidiFile() == nullptr){
        song->generateMIDI();
    }
    song->getMidiFile()->save(filename);
}

// loads the corpora and initiates the inferring procedure
void MainWindow::inferGrammars()
{
    // request confirmation from user
    QMessageBox msgBox;
    msgBox.setText("The process of inferring grammar from learning corpora can take several minutes.");
    msgBox.setInformativeText("Do you wish to begin the procedure?");
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setWindowTitle("");
    msgBox.setIcon(QMessageBox::Information);

    switch (msgBox.exec()) {
      case QMessageBox::Cancel:
          return;
      case QMessageBox::Ok:
          break;
      default:
          break;
    }

    // show the current process
    QProgressDialog dialog;
    dialog.setWindowTitle("Inferring...");
    dialog.setRange(0,100);
    dialog.setMinimumDuration(0);
    dialog.setValue(0);
    dialog.setWindowModality(Qt::NonModal);
    qApp->processEvents();


    //load selected corpora
    std::unique_ptr<Corpus> chordCorpus = std::unique_ptr<Corpus>(new Corpus());
    std::unique_ptr<Corpus> songpartsCorpus = std::unique_ptr<Corpus>(new Corpus());

    std::unique_ptr<CorpusLoader> cl;

    if(this->paramObject->isCorpusUsed(ParamObject::RS)){
        cl = std::unique_ptr<CorpusLoader>(new RockStudyCorpusLoader());
        cl->loadCorpus(CORPUS_RS200HARMONY_DIR, songpartsCorpus.get(), paramObject.get());

    }
    if(this->paramObject->isCorpusUsed(ParamObject::REFSEG)){
        cl = std::unique_ptr<CorpusLoader>(new RefSegCorpusLoader());
        cl->loadCorpus(CORPUS_REFERENCE_SEGMENTATIONS_DIR, songpartsCorpus.get(), paramObject.get());
    }

    if(this->paramObject->isCorpusUsed(ParamObject::IRB)){
        cl = std::unique_ptr<CorpusLoader>(new iRbCorpusLoader());
        cl->loadCorpus(CORPUS_IRB_DIR, chordCorpus.get(), paramObject.get());
    }

    if(this->paramObject->isCorpusUsed(ParamObject::IMAG)){
        cl = std::unique_ptr<CorpusLoader>(new ImaginaryBookCorpusLoader());
        cl->loadCorpus(CORPUS_IMAGINARY_BOOK, chordCorpus.get(), paramObject.get());
    }
    if(chordCorpus->isEmpty() && songpartsCorpus->isEmpty()){
        QMessageBox msgBox;
        msgBox.setText("No corpus loaded.");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setWindowTitle("No corpus");
        msgBox.setIcon(QMessageBox::Information);

        msgBox.exec();
        return;
    }

    dialog.setValue(30);
    qApp->processEvents();

    //create new instance move PCFG_BCL, move corpora
    std::unique_ptr<PCFG_BCL> grammarLearningSongparts = std::unique_ptr<PCFG_BCL>(new PCFG_BCL(std::move(songpartsCorpus)));
    std::unique_ptr<PCFG_BCL> grammarLearningChords = std::unique_ptr<PCFG_BCL>(new PCFG_BCL(std::move(chordCorpus)));

    dialog.setValue(40);
    qApp->processEvents();

    // launch the inferring process
    if(grammarLearningSongparts != nullptr && !grammarLearningSongparts->isEmpty()){
        QObject::connect(&dialog, SIGNAL(canceled()), grammarLearningSongparts.get(), SLOT(cancel()));
        grammarSongparts= grammarLearningSongparts->inferGrammar();
        // save the learned grammar to file for future use
        grammarSongparts->writeToFile(GRAMMAR_SONGPART_FILE);
    }
    dialog.setValue(60);
    qApp->processEvents();
    if(grammarLearningChords != nullptr && !grammarLearningChords->isEmpty()){
        QObject::connect(&dialog, SIGNAL(canceled()), grammarLearningChords.get(), SLOT(cancel()));
        grammarChords = grammarLearningChords->inferGrammar();
        // save the learned grammar to file for future use
        grammarChords->writeToFile(GRAMMAR_CHORD_FILE);
    }
    dialog.setValue(100);
    qApp->processEvents();
}





// sets off or stops the playback
void MainWindow::on_playBtn_clicked()
{
    playOrStop();
}

// launches the song generation
void MainWindow::on_generateBtn_clicked()
{
    generateSong();
}
// gets beat of the selected field and launches player from that beat
void MainWindow::on_chordsView_clicked(const QModelIndex &index)
{
    int beat = model->data(index, Qt::UserRole).toInt();
    play(beat);
}
// launches 'Save to MIDI' dialog
void MainWindow::on_actionSave_to_MIDI_triggered()
{
    saveToMidi();
}
// launches the inferring process
void MainWindow::on_actionInfer_grammars_from_corpora_triggered()
{
    inferGrammars();
}

// launches SettingsDialog, passes current paramObject and gets an updated one afterwards
void MainWindow::on_actionSettings_triggered()
{
    SettingsDialog d(paramObject.get(), (AccompLib::TGenre)ui->genreComboBox->currentIndex());
    if(d.exec()){
        this->paramObject = d.getParamObject();

        if(this->song != nullptr){
           this->song->setParamObject(paramObject.get());
        }
    }
}
// updates chordsView to show the song in the new key
void MainWindow::on_keyComboBox_currentIndexChanged(int index)
{
    if(song == nullptr)
        return;
    song->setKey((Chord::Tone)index);
    displaySong();
}
// updates bass weights in paramObject according to new genre, sets tempoSpinBox to default tempo of the genre
void MainWindow::on_genreComboBox_currentIndexChanged(int index)
{
    AccompLib::TGenre newGenre = (AccompLib::TGenre)index;
    AccompLib::loadBassWeights(paramObject.get(),newGenre);
    ui->tempoSpinBox->setValue(AccompLib::getTempo(newGenre));
}
