#ifndef PARAMOBJECT_H
#define PARAMOBJECT_H

#include <map>
#include <vector>

class ParamObject
{

public:
    // enumeration of used corpora
    enum TCorpora {RS, REFSEG, IRB, IMAG};

    // public bool values specifying whether the higher harmonies of chords should be ignored during inference
    bool ignoreSeventh;
    bool ignoreHigherHarmonies;


    ParamObject();
    ~ParamObject(){}

    // returns whether the particular corpus should be used for inference
    bool isCorpusUsed(TCorpora type) const;
    // sets whether the particular corpus should be used for inference
    void setCorpusUsed(TCorpora type, bool used);
    //gets weight for given bass approach strategy on given beat depth
    int getBassWeight(int depth, int strategy) const;
    //sets weight for given bass approach strategy on given beat depth
    void setBassWeight(int depth, int strategy, int weight);

    unsigned short getMinNumberOfSections() const{
        return minNumberOfSections;
    }
    unsigned short getMinLengthOfChordProgression() const{
        return minLengthOfChordProgression;
    }
    unsigned short getMinLengthOfSection() const{
        return minLengthOfSection;
    }
    const std::vector<std::vector<int>>& getBassWeights() const{
        return bassWeights;
    }
    // returns weights for choosing which tone of a chord will serve as bass
    // (in order: root, third, fifth, seventh, ninth, eleventh, thirteenth)
    const std::vector<int>& getBassToneOfChordWeights() const{
        return bassToneOfChordWeigths;
    }

private:
    // weights of different bass approach strategies on different beat depth
    // (specific for each genre)
    std::vector<std::vector<int>> bassWeights;
    //weights for choosing which tone of a chord will serve as bass (in order: root, third, fifth, seventh, ninth, eleventh, thirteenth)
    std::vector<int> bassToneOfChordWeigths;
    // stores whether the particular corpus given by value of TCorpora should be used for inference
    std::map<TCorpora, bool> usedCorpora;

    unsigned short minNumberOfSections;
    unsigned short minLengthOfChordProgression;
    unsigned short minLengthOfSection;
};

#endif // PARAMOBJECT_H
