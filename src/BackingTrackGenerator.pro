#-------------------------------------------------
#
# Project created by QtCreator 2015-06-01T11:05:45
#
#-------------------------------------------------

QT       += core gui
QMAKE_CXXFLAGS += -std=c++0x

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BackingTrackGenerator
TEMPLATE = app



SOURCES += main.cpp\
        mainwindow.cpp \
    songmodel.cpp \
    settingsdialog.cpp \
    paramobject.cpp \
    accomplib.cpp \
    midiplayer.cpp


HEADERS  += mainwindow.h \
    midiplayer.h \
    songmodel.h \
    settingsdialog.h \
    paramobject.h \
    accomplib.h

DEPENDPATH += .

FORMS    += mainwindow.ui \
        settingsdialog.ui

DISTFILES += \
    BackingTrackGenerator.pro.user \
    CMakeLists.txt \

include(qtmidi-master/src/QMidi.pri)
include(PCFG_BCL/PCFG_BCL.pri)
include(grammar/grammar.pri)
include(corpus/corpus.pri)
include(song/song.pri)

QMAKE_CXXFLAGS_WARN_ON = -Wall -Wno-unused-parameter
