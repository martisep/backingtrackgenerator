DEPENDPATH += $$PWD
INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/song.h \
    $$PWD/measure.h \
    $$PWD/songPart.h \
    $$PWD/chord.h

SOURCES += \
    $$PWD/song.cpp \
    $$PWD/measure.cpp \
    $$PWD/songPart.cpp \
    $$PWD/chord.cpp
