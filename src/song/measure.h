#ifndef MEASURE_H
#define MEASURE_H

#include <vector>
#include <sstream>
#include <iostream>
#include "terminalObject.h"
#include "chord.h"


//data class representing a measure (a bar) of music, which can contain multiple chords of different durations
class Measure : public TerminalObject{
public:
    //default length is 4/4, 16 sixteenth notes
    Measure(unsigned short length = 16) : length_(length){}

    //constructs new object from string created by toString (and saved in file)
    Measure(std::string string);

    virtual std::unique_ptr<TerminalObject> parseString(std::string s){
        return std::unique_ptr<Measure>(new Measure(s));
    }

    ~Measure(){}

    // returns key-independent string representation of contained chords in machine readable format
    virtual std::string toString() const;

    // returns human readable chord name in given key
    std::string toString(Chord::Tone key) const;

    virtual bool equal(TerminalObject* t) const{
        Measure * p2 = dynamic_cast<Measure*>(t);
        return ((this->getLength() == p2->getLength()) &&
               (this->getChords() == p2->getChords()));
    }
    bool operator==(const Measure& other) const{
        return (this->getLength() == other.getLength())
                && (this->getChords() == other.getChords());
    }

    const unsigned short getLength() const {
        return length_;
    }
    const std::vector<std::pair<Chord, unsigned short>>& getChords() const{
        return chords;
    }

    //adds new chord to measure
    void addChord(Chord chord, unsigned short length);

private:
    unsigned short length_;
    std::vector<std::pair<Chord, unsigned short>> chords;
};


#endif //MEASURE_H
