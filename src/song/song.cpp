#include "song.h"

Song::Song(const Grammar* grammarSongParts,
           const Grammar* grammarChordProgression,
           Chord::Tone key,
           unsigned short tempo, AccompLib::TGenre genre,
           ParamObject *paramObject)
    : key(key),
      tempo(tempo),
      genre(genre),
      isMidiValid(false),
      paramObject(paramObject)

{
    //load bass strategies weights for given genre to paramObject
    AccompLib::loadBassWeights(this->paramObject, genre);

    //create generators from given grammars

    //check if the generator have valid grammars
    if(grammarSongParts == nullptr || grammarChordProgression == nullptr || grammarSongParts->isEmpty() || grammarChordProgression->isEmpty()){
        return;
    }

    // we will use only three distinct chord progressions through out the song,
    // assigning certain sections with the same progression for more compact sound
    std::vector<Measure> progression[3];

    std::vector<Symbol*> generatedMeasures;

    for (int i = 0; i < 3; ++i) {
        progression[i] = std::vector<Measure>();

        //safeguard to prevent endless loop
        int guard = 0;
        //generate a chord progression sufficiently long
        do {
            generatedMeasures = grammarChordProgression->generateSentence();
            guard++;
        } while((generatedMeasures.size() < paramObject->getMinLengthOfChordProgression()) && (guard < 100));

        // repeat the generated progression as many times as needed to reach the limit
        // for minimal section length
        do{
            for(const auto& item : generatedMeasures){
                Terminal* t = dynamic_cast<Terminal*>(item);
                Measure* m = dynamic_cast<Measure*>(t->getTerminalObject());
                progression[i].push_back(*m);
            }
        } while(progression[i].size() < paramObject->getMinLengthOfSection());

        //progressions should be unique
        bool unique = true;
        for (int j = 0; j < i; ++j) {
            if(progression[j] == progression[i]){
                unique = false;
                break;
            }
        }
        //if the current progression is already used, try again
        if(!unique){
            i--;
        }
    }

    std::vector<Symbol*> generatedSongparts;

    //safeguard to prevent endless loop
    int guard = 0;
    //generate sequence of sections of the song sufficiently long
    do{
        generatedSongparts = grammarSongParts->generateSentence();
        guard++;
    } while((generatedSongparts.size() < paramObject->getMinNumberOfSections()) && (guard < 100));

    for(const auto& item : generatedSongparts){
        Terminal* t = dynamic_cast<Terminal*>(item);
        SongPart* sp = dynamic_cast<SongPart*>(t->getTerminalObject());

        //assign each songpart one of the three chord progressions
        switch (sp->getType()) {
        case SongPart::INTRO:
        case SongPart::VERSE:
            sp->setMeasures(progression[0]);
            break;
        case SongPart::CHORUS:
        case SongPart::OUTRO:
            sp->setMeasures(progression[1]);
            break;
        case SongPart::BRIDGE:
        case SongPart::PRECHORUS:
            sp->setMeasures(progression[2]);
            break;
        default:
            break;
        }
        songparts.push_back(*sp);
    }
}

std::string Song::toString() const{
    std::stringstream ss;
    for(const SongPart& sp : songparts){
        ss << sp.toString()<< std::endl; //type of section such as "VERSE"
        for(const Measure& m : sp.getMeasures()){
            ss << m.toString();         //measure with chords
        }
        ss<< std::endl;
    }
    return ss.str();
}


void Song::generateMIDI(){
    //create new midifile
    midi_file = std::unique_ptr<QMidiFile>(new QMidiFile());

    //create accompaniment using library AccompLib
    AccompLib::generateAccompaniment(genre, key, tempo, songparts, midi_file.get(), paramObject);

    //re-validate midi
    isMidiValid = true;
}

//invalidates midi if key is changed
void Song::setKey(Chord::Tone tone){
    if(this->key == tone){
        return;
    }
    this->key = tone;
    isMidiValid = false;
}

void Song::setTempo(unsigned short tempo){
    if(this->tempo == tempo){
        return;
    }
    this->tempo = tempo;

    //if midi exists and tempo midi event can be inserted, it is
    if((midi_file != nullptr) && (midi_file->tracks().size() > 0)){
        midi_file->createTempoEvent(0,0,tempo);
    } else {
        //else invalidate midi and leave it to generateMIDI()
        isMidiValid = false;
    }
}

void Song::setGenre(AccompLib::TGenre genre)
{
    if(this->genre == genre){
        return;
    }
    this->genre = genre;
    //load weights for bass strategies for new genre into paramObject
    AccompLib::loadBassWeights(paramObject, genre);
    isMidiValid = false;
}

void Song::setParamObject(ParamObject* paramObject){
    this->paramObject = paramObject;
    isMidiValid = false;
}




