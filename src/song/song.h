#ifndef SONG_H
#define SONG_H

#include <vector>
#include <memory>
#include "songPart.h"
#include "QMidiFile.h"
#include "accomplib.h"
#include "paramobject.h"
#include "grammar.h"


//data class representing a complete song
class Song
{
public:
    Song(){}
    // creates song in given key, tempo ad genre and generates its structure
    // and chord progression for each section using given grammars
    Song(const Grammar* grammarSongparts,
         const Grammar* grammarChordProgressions,
         Chord::Tone key,
         unsigned short tempo,
         AccompLib::TGenre genre,
         ParamObject *paramObject);
    ~Song(){}

    std::string toString() const;

    //generates MIDI interpretation of the song
    void generateMIDI();

    // midi file is considered valid if it exists and is coherent with the current parameters
    // (such as genre or key)
    bool hasValidMidi(){
        return isMidiValid;
    }

    //setters
    void setKey(Chord::Tone tone);
    void setTempo(unsigned short tempo);
    void setGenre(AccompLib::TGenre genre);
    void setParamObject(ParamObject* paramObject);

    //getters
    AccompLib::TGenre getGenre(){  return genre; }
    Chord::Tone getKey() const{ return key; }
    ParamObject* getParamObject(){ return paramObject; }
    //get observer pointer
    QMidiFile* getMidiFile(){
        if(midi_file == nullptr){
            return nullptr;
        }
        return midi_file.get();
    }
    const std::vector<SongPart>& getSongparts() const{
        return songparts;
    }
private:
    //song parameters
    Chord::Tone key;
    unsigned short tempo;
    AccompLib::TGenre genre;

    //top layer of song hierarchy (contains SongParts with Measures with Chords)
    std::vector<SongPart> songparts;

    //container of midi messages representing the song
    std::unique_ptr<QMidiFile> midi_file;
    // midi file is considered valid if it exists and is coherent with the current parameters
    // (such as genre or key)
    bool isMidiValid;
    //pointer to object with global parameters and settings
    ParamObject* paramObject;
};

#endif // SONG_H
