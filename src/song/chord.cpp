#include "chord.h"


std::string Chord::toFileString() const{
    std::stringstream ss;
    ss << root_ << " "
       << bass_ << " "
       << third_ << " "
       << fifth_ << " "
       << seventh_<< " "
       << ninth_<< " "
       << eleventh_<< " "
       << thirteenth_ << " ";
    return ss.str();
}

//default constructor returns a I major chord
Chord::Chord(){
    root_ = I;
    bass_ = I;
    third_ = MAJOR3;
    fifth_ = PERFECT5;
    seventh_ = NULL7;
    ninth_ = NULL9;
    eleventh_ = NULL11;
    thirteenth_ = NULL13;
}

//parses representation created by toFileString()
Chord::Chord(std::string string){
    std::stringstream ss(string);
    int i;

    ss >> i;
    setRoot(static_cast<RomanNumeral>(i));
    ss >> i;
    setBass(static_cast<RomanNumeral>(i));
    ss >> i;
    setThird(static_cast<ThirdQuality>(i));
    ss >> i;
    setFifth(static_cast<FifthQuality>(i));
    ss >> i;
    setSeventh(static_cast<SeventhQuality>(i));
    ss >> i;
    setNinth(static_cast<NinthQuality>(i));
    ss >> i;
    setEleventh(static_cast<EleventhQuality>(i));
    ss >> i;
    setThirteenth(static_cast<ThirteenthQuality>(i));
}


Chord::Tone parseTone(std::string& s){
    int toneIndex = 0;
    char c = s.at(0);

    switch(c){
    case 'A':
        toneIndex = 0;
        break;
    case 'B':
        toneIndex = 2;
        break;
    case 'C':
        toneIndex = 3;
        break;
    case 'D':
        toneIndex = 5;
        break;
    case 'E':
        toneIndex = 7;
        break;
    case 'F':
        toneIndex = 8;
        break;
    case 'G':
        toneIndex = 10;
        break;
    default:
        break;
    }
    s.erase(s.begin());
    if(s.compare(0,1,"#") == 0){
        toneIndex = (++toneIndex) % Chord::OCTAVE;
        s.erase(s.begin());
    } else if((s.compare(0,1,"-") == 0) || (s.compare(0,1,"b") == 0)){
        //add OCTAVE to prevent negative argument for modulo
        toneIndex = (--toneIndex + Chord::OCTAVE) % Chord::OCTAVE;
        s.erase(s.begin());
    }
    return Chord::Tone(toneIndex);
}

Chord Chord::parseChord(std::string s, Chord::Tone key, ParamObject* paramObject)
{
    Chord c;

    Chord::Tone absoluteRoot = parseTone(s);
    int diff = absoluteRoot - key;

    Chord::RomanNumeral root = Chord::RomanNumeral((OCTAVE+diff)%OCTAVE);

    c.setRoot(root);
    c.setBass(root);

    size_t pos;

    //ignore chords in brackets (usually mean alternative voicing)
    pos = s.find("(");
    if(pos != std::string::npos){
        s.erase(pos);
    }
    //ignore chords like "C13no9"
    pos = s.find("no");
    if(pos != std::string::npos){
        s.erase(pos, 2);
    }


    //look for marks in chord symbol and deduce harmonic qualities

    // !!!!!!!! ORDER IS IMPORTANT !!!!!!!!!!!!

    //first consider complex chord names like dim or alt

    //diminished and diminished seventh
    pos = s.find("dim7");
    if(pos != std::string::npos){
        c.setThird(Chord::MINOR3);
        c.setFifth(Chord::DIM5);
        c.setSeventh( Chord::DIM7);
        s.erase(pos, 4);
    }
    pos = s.find("o7");
    if(pos != std::string::npos){
        c.setThird(Chord::MINOR3);
        c.setFifth(Chord::DIM5);
        c.setSeventh(Chord::DIM7);
        s.erase(pos, 2);
    }
    pos = s.find("o");
    if(pos != std::string::npos){
        c.setThird(Chord::MINOR3);
        c.setFifth(Chord::DIM5);
        s.erase(pos, 1);
    }
    pos = s.find("dim");
    if(pos != std::string::npos){
        c.setThird(Chord::MINOR3);
        c.setFifth(Chord::DIM5);
        s.erase(pos, 3);
    }

    //half-diminished
    pos = s.find("h7");
    if(pos != std::string::npos){
        c.setThird(Chord::MINOR3);
        c.setFifth(Chord::DIM5);
        c.setSeventh(Chord::MINOR7);
        s.erase(pos, 2);
    }
    pos = s.find("h");
    if(pos != std::string::npos){
        c.setThird(Chord::MINOR3);
        c.setFifth(Chord::DIM5);
        s.erase(pos, 1);
    }

    //altered
    pos = s.find("alt");
    if(pos != std::string::npos){
        c.setThird(Chord::MAJOR3);
        c.setFifth(Chord::AUG5);
        c.setSeventh(Chord::MINOR7);
        s.erase(pos, 3);
    }

    //fifth (perfect fifth is default, untagged)
    pos= s.find("#5");
    if(pos != std::string::npos){
        c.setFifth(Chord::AUG5);
        s.erase(pos, 2);
    }
    pos= s.find("+");
    if(pos != std::string::npos){
        c.setFifth(Chord::AUG5);
        s.erase(pos, 1);
    }
    pos= s.find("b5");
    if(pos != std::string::npos){
        c.setFifth(Chord::DIM5);
        s.erase(pos, 2);
    }

    //thirteenth
    pos= s.find("b13");
    if(pos != std::string::npos){
        c.setSeventh(Chord::MINOR7);
        c.setNinth(Chord::MAJOR9);
        c.setEleventh(Chord::PERFECT11);
        c.setThirteenth(Chord::MINOR13);
        s.erase(pos, 3);
    }
    pos= s.find("#13");
    if(pos != std::string::npos){
        c.setSeventh(Chord::MINOR7);
        c.setNinth(Chord::MAJOR9);
        c.setEleventh(Chord::PERFECT11);
        c.setThirteenth(Chord::AUG13);
        s.erase(pos, 3);
    }
    pos= s.find("13");
    if(pos != std::string::npos){
        c.setSeventh(Chord::MINOR7);
        c.setNinth(Chord::MAJOR9);
        c.setEleventh(Chord::PERFECT11);
        c.setThirteenth(Chord::MAJOR13);
        s.erase(pos, 2);
    }

    //eleventh
    pos= s.find("#11");
    if(pos != std::string::npos){
        c.setSeventh(Chord::MINOR7);
        c.setNinth(Chord::MAJOR9);
        c.setEleventh(Chord::AUG11);
        s.erase(pos, 3);
    }
    pos= s.find("b11");
    if(pos != std::string::npos){
        c.setSeventh(Chord::MINOR7);
        c.setNinth(Chord::MAJOR9);
        c.setEleventh(Chord::DIM11);
        s.erase(pos, 3);
    }
    pos= s.find("11");
    if(pos != std::string::npos){
        c.setSeventh(Chord::MINOR7);
        c.setNinth(Chord::MAJOR9);
        c.setEleventh(Chord::PERFECT11);
        s.erase(pos, 2);
    }

    //ninth
    pos= s.find("add9");
    if(pos != std::string::npos){
        c.setNinth(Chord::MAJOR9);
        s.erase(pos, 4);
    }
    pos= s.find("maj9");
    if(pos != std::string::npos){
        c.setSeventh(Chord::MAJOR7);
        c.setNinth(Chord::MAJOR9);
        s.erase(pos, 4);
    }
    pos= s.find("M9");
    if(pos != std::string::npos){
        c.setSeventh(Chord::MAJOR7);
        c.setNinth(Chord::MAJOR9);
        s.erase(pos, 2);
    }

    pos= s.find("#9");
    if(pos != std::string::npos){
        c.setSeventh(Chord::MINOR7);
        c.setNinth(Chord::AUG9);
        s.erase(pos, 2);
    }
    pos= s.find("b9");
    if(pos != std::string::npos){
        c.setSeventh(Chord::MINOR7);
        c.setNinth(Chord::MINOR9);
        s.erase(pos, 2);
    }
    pos= s.find("9");
    if(pos != std::string::npos){
        c.setSeventh(Chord::MINOR7);
        c.setNinth(Chord::MAJOR9);
        s.erase(pos, 1);
    }

    //seventh
    pos= s.find("maj7");
    if(pos != std::string::npos){
        c.setSeventh(Chord::MAJOR7);
        s.erase(pos, 4);
    }
    pos= s.find("Maj7");
    if(pos != std::string::npos){
        c.setSeventh(Chord::MAJOR7);
        s.erase(pos, 4);
    }
    pos= s.find("M7");
    if(pos != std::string::npos){
        c.setSeventh(Chord::MAJOR7);
        s.erase(pos, 2);
    }
    pos= s.find("maj");
    if(pos != std::string::npos){
        c.setSeventh(Chord::MAJOR7);
        s.erase(pos, 3);
    }
    pos= s.find("^");
    if(pos != std::string::npos){
        c.setSeventh(Chord::MAJOR7);
        s.erase(pos, 1);
    }
    pos= s.find("7");
    if(pos != std::string::npos){
        c.setSeventh(Chord::MINOR7);
        s.erase(pos, 1);
    }
    pos= s.find("b6");
    if(pos != std::string::npos){
        c.setSeventh(Chord::DIM6);
        s.erase(pos, 2);
    }
    pos= s.find("M6");
    if(pos != std::string::npos){
        c.setSeventh(Chord::DIM7);
        s.erase(pos, 2);
    }
    pos= s.find("6");
    if(pos != std::string::npos){
        c.setSeventh(Chord::DIM7);
        s.erase(pos, 1);
    }


    //third (major is default, untagged)
    pos= s.find("min");
    if(pos != std::string::npos){
        c.setThird(Chord::MINOR3);
        s.erase(pos, 3);
    }
    pos= s.find("mi");
    if(pos != std::string::npos){
        c.setThird(Chord::MINOR3);
        s.erase(pos, 2);
    }
    pos= s.find("m");
    if(pos != std::string::npos){
        c.setThird(Chord::MINOR3);
        s.erase(pos, 1);
    }
    pos= s.find("sus4");
    if(pos != std::string::npos){
        c.setThird(Chord::AUG3);
        s.erase(pos, 4);
    }
    pos= s.find("sus2");
    if(pos != std::string::npos){
        c.setThird(Chord::DIM3);
        s.erase(pos, 4);
    }
    pos= s.find("sus");
    if(pos != std::string::npos){
        c.setThird(Chord::AUG3);
        s.erase(pos, 3);
    }


    //inversion (different bass note)
    pos = s.find("/");
    if(pos != std::string::npos){
        std::string inversion = s.substr(pos+1);

        int diff = parseTone(inversion) - key;

        c.setBass(Chord::RomanNumeral((OCTAVE+diff)%OCTAVE));
        s.erase(pos);
    }

    // ignore higher harmonies
    if(paramObject->ignoreSeventh)
        c.setSeventh(NULL7);
    if(paramObject->ignoreHigherHarmonies){
        c.setNinth(NULL9);
        c.setEleventh(NULL11);
        c.setThirteenth(NULL13);
    }

    return c;
}

std::bitset<Chord::OCTAVE> Chord::toBitset(){
    std::stringstream ss;
    // bitset can be constructed from string, but values must be in reverse order
    // creates "scale" from harmonic functions contained in chord
    // 11 semitones above root
    if(seventh_== MAJOR7) ss << "1"; else ss << "0";
    // 10 semitones above root
    if((seventh_ == MINOR7) || (thirteenth_ == AUG13)) ss << "1"; else ss << "0";
    // 9 semitones above root
    if((seventh_ == DIM7) || (thirteenth_ == MAJOR13)) ss << "1"; else ss << "0";
    // 8 semitones above root
    if((seventh_ == DIM6) || (thirteenth_ == MINOR13) || (fifth_ == AUG5)) ss << "1"; else ss << "0";
    // 7 semitones above root
    if(fifth_ == PERFECT5) ss << "1"; else ss << "0";
    // 6 semitones above root
    if((fifth_ == DIM5) || (eleventh_ == AUG11)) ss << "1"; else ss << "0";
    // 5 semitones above root
    if((third_ == AUG3) || (eleventh_ == PERFECT11)) ss << "1"; else ss << "0";
    // 4 semitones above root
    if((third_ == MAJOR3) || (eleventh_ == DIM11)) ss << "1"; else ss << "0";
    // 3 semitones above root
    if((third_ == MINOR3) || (ninth_ == AUG9)) ss << "1"; else ss << "0";
    // 2 semitones above root
    if((third_ == DIM3) || (ninth_ == MAJOR9)) ss << "1"; else ss << "0";
    // 1 semitone above root
    if(ninth_ == MINOR9) ss << "1"; else ss << "0";
    // root
    ss << "1";
    std::string s = ss.str();
    return std::bitset<Chord::OCTAVE>(s);
}

Chord::Tone Chord::getToneOfHarmonicFunction(Chord::HarmonicFunction harmonic, Chord::Tone key) const{
    //return specific music tone for given harmonic in given key
    switch (harmonic)
    {
    case ROOT:
        return Tone((key + root_) % OCTAVE);
    case THIRD:
        return Tone((key + root_ + third_) % OCTAVE);
    case FIFTH:
        return Tone((key + root_ + fifth_) % OCTAVE);
    case SEVENTH:
        return Tone((key + root_ + seventh_) % OCTAVE);
    case NINTH:
        return Tone((key + root_ + ninth_) % OCTAVE);
    case ELEVENTH:
        return Tone((key + root_ + eleventh_) % OCTAVE);
    case THIRTEENTH:
        return Tone((key + root_ + thirteenth_) % OCTAVE);
    default:
        return Tone((key + root_) % OCTAVE);
    }
}

//setters with validity check
void Chord::setRoot(Chord::RomanNumeral root){
    if(root >= I && root <= VII){
        this->root_ = root;
    }
}
void Chord::setBass(Chord::RomanNumeral bass){
    if(bass >= I && bass <= VII){
        this->bass_ = bass;
    }
}
void Chord::setThird(Chord::ThirdQuality third){
    if(third == DIM3 || third == MINOR3 || third == MAJOR3 || third == AUG3){
        this->third_ = third;
    }
}
void Chord::setFifth(Chord::FifthQuality fifth){
    if(fifth == DIM5 || fifth == PERFECT5 || fifth == AUG5){
        this->fifth_ = fifth;
    }
}
void Chord::setSeventh(Chord::SeventhQuality seventh){
    if(seventh == DIM6 || seventh == DIM7 || seventh == MINOR7 || seventh == MAJOR7 || seventh == NULL7){
        this->seventh_= seventh;
    }
}
void Chord::setNinth(Chord::NinthQuality ninth){
    if(ninth == MINOR9 || ninth == MAJOR9 || ninth == AUG9 || ninth == NULL9){
        this->ninth_= ninth;
    }
}
void Chord::setEleventh(Chord::EleventhQuality eleventh){
    if(eleventh == DIM11 || eleventh == PERFECT11 || eleventh == AUG11 || eleventh == NULL11){
        this->eleventh_= eleventh;
    }
}
void Chord::setThirteenth(Chord::ThirteenthQuality thirteenth){
    if(thirteenth == MINOR13 || thirteenth == MAJOR13 || thirteenth == AUG13 || thirteenth == NULL13){
        this->thirteenth_= thirteenth;
    }
}

// return human readable string representation of the chord in roman-numeral notation
std::string Chord::toString() const{
    std::stringstream ss;
    ss << romanStrings[root_];
    ss << getSuffix();
    return ss.str();
}

// return human readable string representation of the chord in given key
std::string Chord::toString(Chord::Tone key) const
{
    //decide whether to use sharps (#) or flats (b) notation according to key
    std::stringstream ss;
    switch (key) {
    case Chord::C:
    case Chord::G:
    case Chord::D:
    case Chord::A:
    case Chord::E:
    case Chord::B:
    case Chord::Fs:
        ss << toneStringsSharp[(root_ + key) % OCTAVE];
        break;
    case Chord::F:
    case Chord::As:
    case Chord::Ds:
    case Chord::Gs:
    case Chord::Cs:
        ss << toneStringsFlat[(root_ + key) % OCTAVE];
        break;
    default:
        ss << toneStringsSharp[(root_ + key) % OCTAVE];
        break;
    }

    ss << getSuffix();
    return ss.str();
}

bool Chord::operator==(const Chord &other) const{
    return (
                this->root_ == other.root_
                && this->bass_ == other.bass_
                && this->third_ == other.third_
                && this->fifth_ == other.fifth_
                && this->seventh_ == other.seventh_
                && this->ninth_ == other.ninth_
                && this->eleventh_ == other.eleventh_
                && this->thirteenth_ == other.thirteenth_
                );
}

// get suffix for the chord expressing qualities of contained harmonic functions according to standards of music notation
std::string Chord::getSuffix() const
{
    // !!!!!!!! ORDER IS IMPORTANT !!!!!!!!!!!!

    std::stringstream ss;
    // major third is default, not written
    switch (third_) {
    case DIM3:
        ss << "sus2";
        break;
    case MINOR3:
        ss << "mi";
        break;
    case AUG3:
        ss << "sus4";
        break;
    default:
        break;
    }
    switch (seventh_) {
    case DIM6:
        ss << "b6";
        break;
    case DIM7:
        ss << "6";
        break;
    case MINOR7:
        // minor seventh is default, only written if there are no higher harmonics present
        if (thirteenth_ == NULL13 && eleventh_ == NULL11 && ninth_ == NULL9)
            ss << "7";
        break;
    case MAJOR7:
        ss << "maj7";
        break;

    default:
        break;
    }

    switch (thirteenth_) {
    case MINOR13:
        ss << "b13";
        break;
    case MAJOR13:
        ss << "13";
        break;
    case AUG13:
        ss << "#13";
        break;
    default:
        break;
    }

    switch (eleventh_) {
    case DIM11:
        ss << "b11";
        break;
    case PERFECT11:
        // perfect eleventh is default, only written if there are no higher harmonics present
        if(thirteenth_ == NULL13)
            ss << "11";
        break;
    case AUG11:
        ss << "#11";
        break;
    default:
        break;
    }

    switch (ninth_) {
    case MINOR9:
        ss << "b9";
        break;
    case MAJOR9:
        // major ninth is default, only written if there are no higher harmonics present
        if (thirteenth_ == NULL13 && eleventh_ == NULL11)
            ss << "9";
        break;
    case AUG9:
        ss << "#9";
        break;
    default:
        break;
    }

    // perfect fifth is default, not written
    switch (fifth_) {
    case DIM5:
        ss << "b5";
        break;
    case AUG5:
        ss << "#5";
        break;
    default:
        break;
    }
    return ss.str();

}

const std::string Chord::romanStrings[] = { "I", "bII", "II", "bIII", "III", "IV", "bV", "V", "bVI","VI","bVII","VII"};
const std::string Chord::toneStringsSharp[] = { "A", "A#", "B", "C", "C#", "D", "D#", "E", "F","F#","G","G#"};
const std::string Chord::toneStringsFlat[] = { "A", "Bb", "B", "C", "Db", "D", "Eb", "E", "F","Gb","G","Ab"};
