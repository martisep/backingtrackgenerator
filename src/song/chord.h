#ifndef CHORD_H
#define CHORD_H

#include <string>
#include <sstream>
#include <bitset>

#include "paramobject.h"

class Chord
{	
public:
    static const std::string romanStrings[];
    static const std::string toneStringsSharp[];
    static const std::string toneStringsFlat[];

    //interval of one octave is 12 semitones
    static const unsigned short OCTAVE = 12;

    // all twelve music tones
    enum Tone {A, As, B, C, Cs, D, Ds, E, F, Fs, G, Gs};
    // roman numeral notation for intervals
    enum RomanNumeral {I, IIb, II, IIIb, III, IV, Vb, V, VIb, VI, VIIb, VII};
    // all possible harmonic functions that can be contained in a chord
    enum HarmonicFunction {ROOT, THIRD, FIFTH, SEVENTH, NINTH, ELEVENTH, THIRTEENTH};
    // possible qualities of the harmonic functions (value is distance from root in semitones)
	enum ThirdQuality { DIM3 = 2, MINOR3 = 3, MAJOR3 = 4, AUG3 = 5 };
	enum FifthQuality { DIM5 = 6, PERFECT5 = 7, AUG5 = 8 };
	enum SeventhQuality { NULL7 = 0, DIM6 = 8, DIM7 = 9, MINOR7 = 10, MAJOR7 = 11 };
	enum NinthQuality { NULL9 = 0, MINOR9 = 13, MAJOR9 = 14, AUG9 = 15 };
	enum EleventhQuality { NULL11 = 0, DIM11 = 16, PERFECT11 = 17, AUG11 = 18 };
	enum ThirteenthQuality { NULL13 = 0, MINOR13 = 20, MAJOR13 = 21, AUG13 = 22 };

    //construct Chord with default values (major chord)
    Chord();
    //constructs Chord from string representation created by toFileString()
    Chord(std::string string);
    ~Chord(){}

    // static function for parsing a chord from name
    static Chord parseChord(std::string s, Chord::Tone key, ParamObject* paramObject);

    //returns bitset mask of the chord
    std::bitset<Chord::OCTAVE> toBitset();

    Tone getBass(Tone key = Tone::A) const{
        return Tone((key + bass_) % OCTAVE);
    }

    Tone getToneOfHarmonicFunction(HarmonicFunction harmonic, Tone key=Tone::A) const;

    ThirdQuality getThird(){ return third_;}
    FifthQuality getFifth(){ return fifth_;}
    SeventhQuality getSeventh(){ return seventh_;}
    NinthQuality getNinth(){ return ninth_;}
    EleventhQuality getEleventh(){ return eleventh_;}
    ThirteenthQuality getThirteenth(){ return thirteenth_;}

    //setters with validity check
    void setRoot(RomanNumeral root);
    void setBass(RomanNumeral bass);
    void setThird(ThirdQuality third);
    void setFifth(FifthQuality fifth);
    void setSeventh(SeventhQuality seventh);
    void setNinth(NinthQuality ninth);
    void setEleventh(EleventhQuality eleventh);
    void setThirteenth(ThirteenthQuality thirteenth);

    //returns true if bass was explicitly defined different from root tone
    bool hasBass() const{
        return !(root_ == bass_);
    }
    //returns text representation in Roman-Numeral notation (not related to any specific key)
    std::string toString() const;

    //returns text representation in specified key
    std::string toString(Chord::Tone key) const;

    //returns text representation suitable for storing in text file
    std::string toFileString() const;

    bool operator==(const Chord& other) const;

private:
    //returns suffix of a chord according to conventional music notation
    std::string getSuffix() const;

	RomanNumeral root_;
	RomanNumeral bass_;
	ThirdQuality third_;
	FifthQuality fifth_;
	SeventhQuality seventh_;
	NinthQuality ninth_;
	EleventhQuality eleventh_;
	ThirteenthQuality thirteenth_;
};

#endif //CHORD_H
