#include "measure.h"

// parses string representation of Measure created by toString()
Measure::Measure(std::string string){
    length_ = 0;

    std::size_t found = 0;
    std::size_t found2 = 0;


    while((found = string.find("(")) != std::string::npos){
        std::string s = string.substr(0,found);

        // create chord from file string representation
        Chord c(s);

        found2 = string.find(")");

        if(found2 == std::string::npos){
            std::cerr << string << " " << found2;
            return;
        }
        std::string sub = string.substr(found+1,found2-found-1);

        int chordLength=0;

        try{
            chordLength = std::stoi(sub);
        }
        catch(const std::invalid_argument&){}

        if(chordLength > 0){
            length_ += chordLength;
            this->addChord(c, chordLength);
        }

        string.erase(0,found2+1);
    }
}

std::string Measure::toString(Chord::Tone key) const{
    std::stringstream ss;
    for (const auto& chord : chords) {
        ss << chord.first.toString(key) << " ";
    }
    return ss.str();
}

std::string Measure::toString() const{
    std::stringstream ss;
    for (const auto& chord : chords) {
        ss << chord.first.toFileString() <<" (" << std::to_string(chord.second) << ") ";
    }
    ss << " || ";
    return ss.str();
}

void Measure::addChord(Chord chord, unsigned short length){
    //if new chord is the same as the last one added, just increase the duration of the chord
    if (!chords.empty() && (chord == chords.back().first)){
        chords.back().second += length;
        return;
    }
    //else add new chord
    chords.push_back(std::make_pair(chord,length));
}
