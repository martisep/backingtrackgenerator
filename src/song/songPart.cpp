#include "songPart.h"


// string representations of different songparts
const std::string SongPart::EnumStrings[] = { "INTRO", "VERSE", "CHORUS", "BRIDGE", "PRECHORUS", "OUTRO"};

SongPart::TSongPart SongPart::parseSongPart(std::string word){
    if((word.compare(0,3,"$In") == 0)
            || (word.compare(0,5,"intro") == 0)
            || (word.compare(0,5,"Intro") == 0)
            || (word.compare(0,5,"INTRO") == 0)){
        return INTRO;
    } else if ((word.compare(0,3,"$Vr") == 0)
               || (word.compare(0,5,"verse") == 0)
               || (word.compare(0,5,"Verse") == 0)
               || (word.compare(0,5,"VERSE") == 0)){
        return VERSE;
    } else if ((word.compare(0,3,"$Ch") == 0)
               || (word.compare(0,6,"chorus") == 0)
               || (word.compare(0,6,"Chorus") == 0)
               || (word.compare(0,6,"CHORUS") == 0)
               || (word.compare(0,7,"refrain") == 0)
               || (word.compare(0,7,"Refrain") == 0)){
        return CHORUS;
    } else if ((word.compare(0,3,"$Ou") == 0)
               || (word.compare(0,5,"outro") == 0)
               || (word.compare(0,5,"Outro") == 0)
               || (word.compare(0,5,"OUTRO") == 0)
               || (word.compare(0,8,"$Fadeout") == 0)){
        return OUTRO;
    } else if ((word.compare(0,3,"$Br") == 0)
               || (word.compare(0,6,"bridge") == 0)
               || (word.compare(0,6,"Bridge") == 0)
               || (word.compare(0,6,"BRIDGE") == 0)){
        return BRIDGE;
    } else if ((word.compare(0,3,"$Pc") == 0)
               || (word.compare(0,5,"$Prec") == 0)
               || (word.compare(0,9,"prechorus") == 0)
               || (word.compare(0,9,"Prechorus") == 0)
               || (word.compare(0,9,"PRECHORUS") == 0)
               || (word.compare(0,10,"pre_chorus") == 0)){
        return PRECHORUS;
    } else{
        return NOPART;
    }
}
