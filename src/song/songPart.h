#ifndef SONGPART_H
#define SONGPART_H

#include <string>
#include <vector>
#include <memory>

#include "terminalObject.h"
#include "measure.h"


//data class representing a section of a song (is a terminal object of a grammar)
class SongPart : public TerminalObject {
public:
    //recognized types of sections
    enum TSongPart {
        NOPART = -1, INTRO, VERSE, CHORUS, BRIDGE, PRECHORUS, OUTRO
    };

    SongPart(){}
    SongPart(TSongPart type) : type_(type){}

    // implementation of virtual function of TerminalObject
    // returns new songpart from string
    // is basically static, but cannot be declared as such because then we couldn't use polymorphism
    virtual std::unique_ptr<TerminalObject> parseString(std::string s){
        return std::unique_ptr<SongPart>(new SongPart(parseSongPart(s)));
    }

    ~SongPart(){}

    //if string corresponds to a songpart, return its type (otherwise return NOPART)
    static TSongPart parseSongPart(std::string word);

    void setMeasures(const std::vector<Measure>& list){
        this->measures = list;
    }
    const std::vector<Measure>& getMeasures() const{
        return this->measures;
    }
    virtual std::string toString() const {
        if(type_ == NOPART){
            return "";
        }
        return EnumStrings[type_];
    }

    virtual bool equal(TerminalObject* t) const{
        return (this->type_ == (dynamic_cast<SongPart*>(t))->getType());
    }

    TSongPart getType() const {
        return type_;
    }
private:
    //type of section (verse, chorus,...)
    TSongPart type_;
    // contained measures with chords
    std::vector<Measure> measures;

    // string representations of different songparts
    static const std::string EnumStrings[8];
};



#endif //SONGPART_H

