#include "settingsdialog.h"
#include "ui_settingsdialog.h"

SettingsDialog::SettingsDialog(ParamObject *paramObject, AccompLib::TGenre genre, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog),
    genre(genre)
{
    ui->setupUi(this);

    if(paramObject == nullptr)
        return;

    //set default values using paramObject
    setDefaultBassWeights(paramObject);

    ui->checkBox->setChecked(paramObject->isCorpusUsed(ParamObject::RS));
    ui->checkBox_2->setChecked(paramObject->isCorpusUsed(ParamObject::REFSEG));
    ui->checkBox_3->setChecked(paramObject->isCorpusUsed(ParamObject::IRB));
    ui->checkBox_4->setChecked(paramObject->isCorpusUsed(ParamObject::IMAG));

    ui->checkIgnoreSeventh->setChecked(paramObject->ignoreSeventh);
    ui->checkIgnoreHigher->setChecked(paramObject->ignoreHigherHarmonies);
}

std::unique_ptr<ParamObject> SettingsDialog::getParamObject()
{
    std::unique_ptr<ParamObject> p = std::unique_ptr<ParamObject>(new ParamObject());

    //set values in paramObject according to values of gui components
    p->setBassWeight(0,0,this->ui->horizontalSlider->value());
    p->setBassWeight(0,1,this->ui->horizontalSlider_2->value());
    p->setBassWeight(0,2,this->ui->horizontalSlider_3->value());
    p->setBassWeight(0,3,this->ui->horizontalSlider_4->value());
    p->setBassWeight(0,4,this->ui->horizontalSlider_17->value());
    p->setBassWeight(1,0,this->ui->horizontalSlider_5->value());
    p->setBassWeight(1,1,this->ui->horizontalSlider_6->value());
    p->setBassWeight(1,2,this->ui->horizontalSlider_7->value());
    p->setBassWeight(1,3,this->ui->horizontalSlider_8->value());
    p->setBassWeight(1,4,this->ui->horizontalSlider_18->value());
    p->setBassWeight(2,0,this->ui->horizontalSlider_9->value());
    p->setBassWeight(2,1,this->ui->horizontalSlider_10->value());
    p->setBassWeight(2,2,this->ui->horizontalSlider_11->value());
    p->setBassWeight(2,3,this->ui->horizontalSlider_12->value());
    p->setBassWeight(2,4,this->ui->horizontalSlider_19->value());
    p->setBassWeight(3,0,this->ui->horizontalSlider_13->value());
    p->setBassWeight(3,1,this->ui->horizontalSlider_14->value());
    p->setBassWeight(3,2,this->ui->horizontalSlider_15->value());
    p->setBassWeight(3,3,this->ui->horizontalSlider_16->value());
    p->setBassWeight(3,4,this->ui->horizontalSlider_20->value());

    p->setCorpusUsed(ParamObject::RS, ui->checkBox->isChecked());
    p->setCorpusUsed(ParamObject::REFSEG, ui->checkBox_2->isChecked());
    p->setCorpusUsed(ParamObject::IRB, ui->checkBox_3->isChecked());
    p->setCorpusUsed(ParamObject::IMAG, ui->checkBox_4->isChecked());

    p->ignoreSeventh = ui->checkIgnoreSeventh->isChecked();
    p->ignoreHigherHarmonies = ui->checkIgnoreSeventh->isChecked() || ui->checkIgnoreHigher->isChecked();

    return p;
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::setDefaultBassWeights(ParamObject *paramObject){
    ui->horizontalSlider->setValue(paramObject->getBassWeight(0,0));
    ui->horizontalSlider_2->setValue(paramObject->getBassWeight(0,1));
    ui->horizontalSlider_3->setValue(paramObject->getBassWeight(0,2));
    ui->horizontalSlider_4->setValue(paramObject->getBassWeight(0,3));
    ui->horizontalSlider_17->setValue(paramObject->getBassWeight(0,4));

    ui->horizontalSlider_5->setValue(paramObject->getBassWeight(1,0));
    ui->horizontalSlider_6->setValue(paramObject->getBassWeight(1,1));
    ui->horizontalSlider_7->setValue(paramObject->getBassWeight(1,2));
    ui->horizontalSlider_8->setValue(paramObject->getBassWeight(1,3));
    ui->horizontalSlider_18->setValue(paramObject->getBassWeight(1,4));

    ui->horizontalSlider_9->setValue(paramObject->getBassWeight(2,0));
    ui->horizontalSlider_10->setValue(paramObject->getBassWeight(2,1));
    ui->horizontalSlider_11->setValue(paramObject->getBassWeight(2,2));
    ui->horizontalSlider_12->setValue(paramObject->getBassWeight(2,3));
    ui->horizontalSlider_19->setValue(paramObject->getBassWeight(2,4));

    ui->horizontalSlider_13->setValue(paramObject->getBassWeight(3,0));
    ui->horizontalSlider_14->setValue(paramObject->getBassWeight(3,1));
    ui->horizontalSlider_15->setValue(paramObject->getBassWeight(3,2));
    ui->horizontalSlider_16->setValue(paramObject->getBassWeight(3,3));
    ui->horizontalSlider_20->setValue(paramObject->getBassWeight(3,4));
}

void SettingsDialog::on_pushButton_clicked()
{
    std::unique_ptr<ParamObject> p = std::unique_ptr<ParamObject>(new ParamObject());
    AccompLib::loadBassWeights(p.get(),genre);
    setDefaultBassWeights(p.get());
}
