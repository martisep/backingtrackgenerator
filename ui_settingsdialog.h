/********************************************************************************
** Form generated from reading UI file 'settingsdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTINGSDIALOG_H
#define UI_SETTINGSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>

QT_BEGIN_NAMESPACE

class Ui_SettingsDialog
{
public:
    QDialogButtonBox *buttonBox;
    QSlider *horizontalSlider;
    QSlider *horizontalSlider_2;
    QSlider *horizontalSlider_3;
    QSlider *horizontalSlider_4;
    QSlider *horizontalSlider_5;
    QSlider *horizontalSlider_6;
    QSlider *horizontalSlider_7;
    QSlider *horizontalSlider_8;
    QSlider *horizontalSlider_9;
    QSlider *horizontalSlider_10;
    QSlider *horizontalSlider_11;
    QSlider *horizontalSlider_12;
    QSlider *horizontalSlider_13;
    QSlider *horizontalSlider_14;
    QSlider *horizontalSlider_15;
    QSlider *horizontalSlider_16;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QPushButton *pushButton;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_13;
    QCheckBox *checkBox;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_4;
    QSlider *horizontalSlider_17;
    QSlider *horizontalSlider_18;
    QSlider *horizontalSlider_19;
    QSlider *horizontalSlider_20;
    QLabel *label_14;
    QLabel *label_15;
    QCheckBox *checkIgnoreHigher;
    QCheckBox *checkIgnoreSeventh;

    void setupUi(QDialog *SettingsDialog)
    {
        if (SettingsDialog->objectName().isEmpty())
            SettingsDialog->setObjectName(QStringLiteral("SettingsDialog"));
        SettingsDialog->resize(555, 519);
        QIcon icon;
        icon.addFile(QStringLiteral("generator.png"), QSize(), QIcon::Normal, QIcon::Off);
        SettingsDialog->setWindowIcon(icon);
        buttonBox = new QDialogButtonBox(SettingsDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(200, 470, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        horizontalSlider = new QSlider(SettingsDialog);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setGeometry(QRect(140, 210, 91, 16));
        horizontalSlider->setOrientation(Qt::Horizontal);
        horizontalSlider_2 = new QSlider(SettingsDialog);
        horizontalSlider_2->setObjectName(QStringLiteral("horizontalSlider_2"));
        horizontalSlider_2->setGeometry(QRect(140, 235, 91, 16));
        horizontalSlider_2->setOrientation(Qt::Horizontal);
        horizontalSlider_3 = new QSlider(SettingsDialog);
        horizontalSlider_3->setObjectName(QStringLiteral("horizontalSlider_3"));
        horizontalSlider_3->setGeometry(QRect(140, 260, 91, 16));
        horizontalSlider_3->setOrientation(Qt::Horizontal);
        horizontalSlider_4 = new QSlider(SettingsDialog);
        horizontalSlider_4->setObjectName(QStringLiteral("horizontalSlider_4"));
        horizontalSlider_4->setGeometry(QRect(140, 285, 91, 16));
        horizontalSlider_4->setOrientation(Qt::Horizontal);
        horizontalSlider_5 = new QSlider(SettingsDialog);
        horizontalSlider_5->setObjectName(QStringLiteral("horizontalSlider_5"));
        horizontalSlider_5->setGeometry(QRect(240, 210, 91, 16));
        horizontalSlider_5->setOrientation(Qt::Horizontal);
        horizontalSlider_6 = new QSlider(SettingsDialog);
        horizontalSlider_6->setObjectName(QStringLiteral("horizontalSlider_6"));
        horizontalSlider_6->setGeometry(QRect(240, 235, 91, 16));
        horizontalSlider_6->setOrientation(Qt::Horizontal);
        horizontalSlider_7 = new QSlider(SettingsDialog);
        horizontalSlider_7->setObjectName(QStringLiteral("horizontalSlider_7"));
        horizontalSlider_7->setGeometry(QRect(240, 260, 91, 16));
        horizontalSlider_7->setOrientation(Qt::Horizontal);
        horizontalSlider_8 = new QSlider(SettingsDialog);
        horizontalSlider_8->setObjectName(QStringLiteral("horizontalSlider_8"));
        horizontalSlider_8->setGeometry(QRect(240, 285, 91, 16));
        horizontalSlider_8->setOrientation(Qt::Horizontal);
        horizontalSlider_9 = new QSlider(SettingsDialog);
        horizontalSlider_9->setObjectName(QStringLiteral("horizontalSlider_9"));
        horizontalSlider_9->setGeometry(QRect(340, 210, 91, 16));
        horizontalSlider_9->setOrientation(Qt::Horizontal);
        horizontalSlider_10 = new QSlider(SettingsDialog);
        horizontalSlider_10->setObjectName(QStringLiteral("horizontalSlider_10"));
        horizontalSlider_10->setGeometry(QRect(340, 235, 91, 16));
        horizontalSlider_10->setOrientation(Qt::Horizontal);
        horizontalSlider_11 = new QSlider(SettingsDialog);
        horizontalSlider_11->setObjectName(QStringLiteral("horizontalSlider_11"));
        horizontalSlider_11->setGeometry(QRect(340, 260, 91, 16));
        horizontalSlider_11->setOrientation(Qt::Horizontal);
        horizontalSlider_12 = new QSlider(SettingsDialog);
        horizontalSlider_12->setObjectName(QStringLiteral("horizontalSlider_12"));
        horizontalSlider_12->setGeometry(QRect(340, 285, 91, 16));
        horizontalSlider_12->setOrientation(Qt::Horizontal);
        horizontalSlider_13 = new QSlider(SettingsDialog);
        horizontalSlider_13->setObjectName(QStringLiteral("horizontalSlider_13"));
        horizontalSlider_13->setGeometry(QRect(440, 210, 91, 16));
        horizontalSlider_13->setOrientation(Qt::Horizontal);
        horizontalSlider_14 = new QSlider(SettingsDialog);
        horizontalSlider_14->setObjectName(QStringLiteral("horizontalSlider_14"));
        horizontalSlider_14->setGeometry(QRect(440, 235, 91, 16));
        horizontalSlider_14->setOrientation(Qt::Horizontal);
        horizontalSlider_15 = new QSlider(SettingsDialog);
        horizontalSlider_15->setObjectName(QStringLiteral("horizontalSlider_15"));
        horizontalSlider_15->setGeometry(QRect(440, 260, 91, 16));
        horizontalSlider_15->setOrientation(Qt::Horizontal);
        horizontalSlider_16 = new QSlider(SettingsDialog);
        horizontalSlider_16->setObjectName(QStringLiteral("horizontalSlider_16"));
        horizontalSlider_16->setGeometry(QRect(440, 285, 91, 16));
        horizontalSlider_16->setOrientation(Qt::Horizontal);
        label = new QLabel(SettingsDialog);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 150, 251, 16));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label_2 = new QLabel(SettingsDialog);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 210, 91, 16));
        label_3 = new QLabel(SettingsDialog);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(20, 235, 91, 16));
        label_4 = new QLabel(SettingsDialog);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(20, 260, 111, 16));
        label_5 = new QLabel(SettingsDialog);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(20, 278, 131, 31));
        label_5->setWordWrap(true);
        label_6 = new QLabel(SettingsDialog);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(140, 190, 61, 16));
        label_7 = new QLabel(SettingsDialog);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(240, 190, 81, 16));
        label_8 = new QLabel(SettingsDialog);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(340, 190, 81, 16));
        label_9 = new QLabel(SettingsDialog);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(440, 190, 91, 16));
        label_10 = new QLabel(SettingsDialog);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(290, 170, 51, 16));
        pushButton = new QPushButton(SettingsDialog);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(430, 340, 101, 23));
        label_11 = new QLabel(SettingsDialog);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(20, 10, 251, 16));
        label_11->setFont(font);
        label_12 = new QLabel(SettingsDialog);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(20, 40, 91, 16));
        label_13 = new QLabel(SettingsDialog);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(20, 90, 101, 16));
        checkBox = new QCheckBox(SettingsDialog);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setGeometry(QRect(150, 40, 301, 17));
        checkBox->setChecked(true);
        checkBox_2 = new QCheckBox(SettingsDialog);
        checkBox_2->setObjectName(QStringLiteral("checkBox_2"));
        checkBox_2->setGeometry(QRect(150, 60, 321, 17));
        checkBox_2->setChecked(true);
        checkBox_3 = new QCheckBox(SettingsDialog);
        checkBox_3->setObjectName(QStringLiteral("checkBox_3"));
        checkBox_3->setGeometry(QRect(150, 90, 321, 17));
        checkBox_3->setChecked(true);
        checkBox_4 = new QCheckBox(SettingsDialog);
        checkBox_4->setObjectName(QStringLiteral("checkBox_4"));
        checkBox_4->setGeometry(QRect(150, 110, 321, 17));
        horizontalSlider_17 = new QSlider(SettingsDialog);
        horizontalSlider_17->setObjectName(QStringLiteral("horizontalSlider_17"));
        horizontalSlider_17->setGeometry(QRect(140, 310, 91, 16));
        horizontalSlider_17->setOrientation(Qt::Horizontal);
        horizontalSlider_18 = new QSlider(SettingsDialog);
        horizontalSlider_18->setObjectName(QStringLiteral("horizontalSlider_18"));
        horizontalSlider_18->setGeometry(QRect(240, 310, 91, 16));
        horizontalSlider_18->setOrientation(Qt::Horizontal);
        horizontalSlider_19 = new QSlider(SettingsDialog);
        horizontalSlider_19->setObjectName(QStringLiteral("horizontalSlider_19"));
        horizontalSlider_19->setGeometry(QRect(340, 310, 91, 16));
        horizontalSlider_19->setOrientation(Qt::Horizontal);
        horizontalSlider_20 = new QSlider(SettingsDialog);
        horizontalSlider_20->setObjectName(QStringLiteral("horizontalSlider_20"));
        horizontalSlider_20->setGeometry(QRect(440, 310, 91, 16));
        horizontalSlider_20->setOrientation(Qt::Horizontal);
        label_14 = new QLabel(SettingsDialog);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(20, 310, 111, 16));
        label_15 = new QLabel(SettingsDialog);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setGeometry(QRect(20, 370, 251, 16));
        label_15->setFont(font);
        checkIgnoreHigher = new QCheckBox(SettingsDialog);
        checkIgnoreHigher->setObjectName(QStringLiteral("checkIgnoreHigher"));
        checkIgnoreHigher->setGeometry(QRect(20, 390, 321, 17));
        checkIgnoreSeventh = new QCheckBox(SettingsDialog);
        checkIgnoreSeventh->setObjectName(QStringLiteral("checkIgnoreSeventh"));
        checkIgnoreSeventh->setGeometry(QRect(20, 410, 321, 17));

        retranslateUi(SettingsDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), SettingsDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SettingsDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(SettingsDialog);
    } // setupUi

    void retranslateUi(QDialog *SettingsDialog)
    {
        SettingsDialog->setWindowTitle(QApplication::translate("SettingsDialog", "Settings", 0));
        label->setText(QApplication::translate("SettingsDialog", "Set weights for different bass approaches:", 0));
        label_2->setText(QApplication::translate("SettingsDialog", "Chord approach", 0));
        label_3->setText(QApplication::translate("SettingsDialog", "Scale approach", 0));
        label_4->setText(QApplication::translate("SettingsDialog", "Chromatic approach", 0));
        label_5->setText(QApplication::translate("SettingsDialog", "Same approach (repeat previous note)", 0));
        label_6->setText(QApplication::translate("SettingsDialog", "Half notes", 0));
        label_7->setText(QApplication::translate("SettingsDialog", "Quarter notes", 0));
        label_8->setText(QApplication::translate("SettingsDialog", "Eighth notes", 0));
        label_9->setText(QApplication::translate("SettingsDialog", "Sixteenth notes", 0));
        label_10->setText(QApplication::translate("SettingsDialog", "Beat level", 0));
        pushButton->setText(QApplication::translate("SettingsDialog", "Default values", 0));
        label_11->setText(QApplication::translate("SettingsDialog", "Choose which corpora to use for inferring", 0));
        label_12->setText(QApplication::translate("SettingsDialog", "Song structure", 0));
        label_13->setText(QApplication::translate("SettingsDialog", "Chord progressions", 0));
        checkBox->setText(QApplication::translate("SettingsDialog", "Rock study (T. de Clercq, D. Temperley, 400 songs)  ", 0));
        checkBox_2->setText(QApplication::translate("SettingsDialog", "Reference segmentations (M. Levy, M. Sandler, 60 songs)", 0));
        checkBox_3->setText(QApplication::translate("SettingsDialog", "iRb jazz collection (Y. Broze, D. Shanahan, cca 600 songs)", 0));
        checkBox_4->setText(QApplication::translate("SettingsDialog", "The Imaginary Book (R. Keller, cca 1500 songs)", 0));
        label_14->setText(QApplication::translate("SettingsDialog", "No approach (let ring)", 0));
        label_15->setText(QApplication::translate("SettingsDialog", "Miscellaneous", 0));
        checkIgnoreHigher->setText(QApplication::translate("SettingsDialog", "ignore harmonies higher than seventh", 0));
        checkIgnoreSeventh->setText(QApplication::translate("SettingsDialog", "ignore harmonies higher than and including seventh", 0));
    } // retranslateUi

};

namespace Ui {
    class SettingsDialog: public Ui_SettingsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTINGSDIALOG_H
