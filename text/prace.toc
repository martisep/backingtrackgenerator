\select@language {czech}
\contentsline {chapter}{\IeC {\'U}vod}{3}{chapter*.2}
\contentsline {section}{C\IeC {\'\i }le pr\IeC {\'a}ce}{3}{section*.3}
\contentsline {section}{Motivace}{3}{section*.4}
\contentsline {section}{Struktura pr\IeC {\'a}ce}{3}{section*.5}
\contentsline {chapter}{\numberline {1}Existuj\IeC {\'\i }c\IeC {\'\i } \IeC {\v r}e\IeC {\v s}en\IeC {\'\i }}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Backing-tracky nahran\IeC {\'e} ve studiu re\IeC {\'a}ln\IeC {\'y}mi muzikanty}{5}{section.1.1}
\contentsline {section}{\numberline {1.2}Backing-tracky vygenerovan\IeC {\'e} po\IeC {\v c}\IeC {\'\i }ta\IeC {\v c}em}{5}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Band-in-a-Box}{6}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Impro-visor}{6}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}iReal Pro}{7}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}ChordPulse}{7}{subsection.1.2.4}
\contentsline {subsection}{\numberline {1.2.5}ChordPad}{8}{subsection.1.2.5}
\contentsline {chapter}{\numberline {2}Metody algoritmick\IeC {\'e} hudebn\IeC {\'\i } kompozice}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Gramatiky}{9}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}L-syst\IeC {\'e}my}{10}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}Znalostn\IeC {\'\i } syst\IeC {\'e}my}{11}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Znalostn\IeC {\'\i } syst\IeC {\'e}my a~evolu\IeC {\v c}n\IeC {\'\i } algoritmy}{11}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Spl\IeC {\v n}ov\IeC {\'a}n\IeC {\'\i } podm\IeC {\'\i }nek}{11}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Markovovy \IeC {\v r}et\IeC {\v e}zce}{12}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Skryt\IeC {\'e} Markovovy modely}{13}{subsection.2.3.1}
\contentsline {section}{\numberline {2.4}Neuronov\IeC {\'e} s\IeC {\'\i }t\IeC {\v e}}{13}{section.2.4}
\contentsline {section}{\numberline {2.5}Evolu\IeC {\v c}n\IeC {\'\i } algoritmy}{14}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Algoritmy s~automatickou hodnot\IeC {\'\i }c\IeC {\'\i } funkc\IeC {\'\i }}{15}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Algoritmy s~interaktivn\IeC {\'\i } hodnot\IeC {\'\i }c\IeC {\'\i } funkc\IeC {\'\i }}{15}{subsection.2.5.2}
\contentsline {section}{\numberline {2.6}Sob\IeC {\v e}podobnost a~frakt\IeC {\'a}ly}{16}{section.2.6}
\contentsline {chapter}{\numberline {3}Volba metodologie}{17}{chapter.3}
\contentsline {section}{Shrnut\IeC {\'\i }}{18}{section*.10}
\contentsline {chapter}{\numberline {4}Algoritmy inference bezkontextov\IeC {\'y}ch gramatik bez u\IeC {\v c}itele}{19}{chapter.4}
\contentsline {section}{\numberline {4.1}ABL (Alignment-Based Learning)}{19}{section.4.1}
\contentsline {section}{\numberline {4.2}ADIOS (Automatic DIstillation of Structure)}{20}{section.4.2}
\contentsline {section}{\numberline {4.3}EMILE (Entity Modelling Intelligent Learning Engine)}{20}{section.4.3}
\contentsline {section}{\numberline {4.4}PCFG-BCL}{21}{section.4.4}
\contentsline {section}{\numberline {4.5}Diskuze v\IeC {\'y}b\IeC {\v e}ru algoritmu}{21}{section.4.5}
\contentsline {chapter}{\numberline {5}Algoritmus PCFG-BCL}{22}{chapter.5}
\contentsline {section}{\numberline {5.1}Reprezentace gramatiky}{22}{section.5.1}
\contentsline {section}{\numberline {5.2}Hlavn\IeC {\'\i } f\IeC {\'a}ze algoritmu}{22}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Nalezen\IeC {\'\i } nov\IeC {\'e} AND-OR skupiny technikou biclusteringu}{23}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}P\IeC {\v r}ipojen\IeC {\'\i } nov\IeC {\v e} nalezen\IeC {\'y}ch AND netermin\IeC {\'a}l\IeC {\r u} pod ji\IeC {\v z} existuj\IeC {\'\i }c\IeC {\'\i } OR symboly}{25}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Postprocessing}{27}{subsection.5.2.3}
\contentsline {chapter}{\numberline {6}Pou\IeC {\v z}it\IeC {\'e} korpusy}{29}{chapter.6}
\contentsline {section}{\numberline {6.1}Korpusy zachycuj\IeC {\'\i }c\IeC {\'\i } strukturu skladby}{29}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Reference Segmentations Corpus}{29}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Rock Study Corpus}{29}{subsection.6.1.2}
\contentsline {section}{\numberline {6.2}Korpusy akordov\IeC {\'y}ch posloupnost\IeC {\'\i }}{30}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Imaginary Book}{30}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}iRb Corpus}{31}{subsection.6.2.2}
\contentsline {chapter}{\numberline {7}Generov\IeC {\'a}n\IeC {\'\i } rytmick\IeC {\'e}ho doprovodu}{32}{chapter.7}
\contentsline {section}{\numberline {7.1}Generov\IeC {\'a}n\IeC {\'\i } bic\IeC {\'\i }ch}{32}{section.7.1}
\contentsline {section}{\numberline {7.2}Generov\IeC {\'a}n\IeC {\'\i } klav\IeC {\'\i }rn\IeC {\'\i }ho doprovodu}{33}{section.7.2}
\contentsline {section}{\numberline {7.3}Generov\IeC {\'a}n\IeC {\'\i } basov\IeC {\'e} linky}{33}{section.7.3}
\contentsline {chapter}{\numberline {8}Program\IeC {\'a}torsk\IeC {\'a} dokumentace}{36}{chapter.8}
\contentsline {section}{\numberline {8.1}Prost\IeC {\v r}ed\IeC {\'\i }}{36}{section.8.1}
\contentsline {section}{\numberline {8.2}Obecn\IeC {\'a} struktura}{36}{section.8.2}
\contentsline {section}{\numberline {8.3}Reprezentace skladby}{36}{section.8.3}
\contentsline {section}{\numberline {8.4}Reprezentace gramatiky}{38}{section.8.4}
\contentsline {section}{\numberline {8.5}Reprezentace korpusu a~souvisej\IeC {\'\i }c\IeC {\'\i } t\IeC {\v r}\IeC {\'\i }dy}{39}{section.8.5}
\contentsline {section}{\numberline {8.6}Implementace algoritmu PCFG-BCL}{40}{section.8.6}
\contentsline {subsection}{\numberline {8.6.1}Proces inference gramatik}{41}{subsection.8.6.1}
\contentsline {section}{\numberline {8.7}Generov\IeC {\'a}n\IeC {\'\i } rytmick\IeC {\'e}ho aran\IeC {\v z}m\IeC {\'a}}{42}{section.8.7}
\contentsline {subsection}{\numberline {8.7.1}Generov\IeC {\'a}n\IeC {\'\i } bic\IeC {\'\i }ch a klav\IeC {\'\i }rn\IeC {\'\i }ho doprovodu}{43}{subsection.8.7.1}
\contentsline {subsection}{\numberline {8.7.2}Generov\IeC {\'a}n\IeC {\'\i } basov\IeC {\'e} linky}{43}{subsection.8.7.2}
\contentsline {section}{\numberline {8.8}P\IeC {\v r}ehr\IeC {\'a}v\IeC {\'a}n\IeC {\'\i } MIDI}{45}{section.8.8}
\contentsline {section}{\numberline {8.9}GUI}{45}{section.8.9}
\contentsline {chapter}{Z\IeC {\'a}v\IeC {\v e}r}{46}{chapter*.16}
\contentsline {section}{Mo\IeC {\v z}nosti dal\IeC {\v s}\IeC {\'\i }ho v\IeC {\'y}voje}{46}{section*.17}
\contentsline {chapter}{Seznam pou\IeC {\v z}it\IeC {\'e} literatury}{47}{chapter*.18}
\contentsline {chapter}{P\IeC {\v r}\IeC {\'\i }loha \numberline {A}Obsah CD}{53}{Appendix.a.A}
\contentsline {chapter}{P\IeC {\v r}\IeC {\'\i }loha \numberline {B}U\IeC {\v z}ivatelsk\IeC {\'a} dokumentace}{54}{Appendix.a.B}
\contentsline {section}{\numberline {B.1}Instalace}{54}{section.a.B.1}
\contentsline {section}{\numberline {B.2}Hlavn\IeC {\'\i } okno}{54}{section.a.B.2}
\contentsline {section}{\numberline {B.3}Dialog nastaven\IeC {\'\i } programu}{54}{section.a.B.3}
\contentsline {section}{\numberline {B.4}Pr\IeC {\'a}ce s~programem}{55}{section.a.B.4}
