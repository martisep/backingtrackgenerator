/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionSettings;
    QAction *actionInfer_grammars_from_corpora;
    QAction *actionSave_to_MIDI;
    QWidget *centralWidget;
    QPushButton *generateBtn;
    QListView *chordsView;
    QComboBox *keyComboBox;
    QLabel *labelKey;
    QLabel *labelGenre;
    QComboBox *genreComboBox;
    QLabel *labelTempo;
    QSpinBox *tempoSpinBox;
    QPushButton *playBtn;
    QSpinBox *repeatSpinBox;
    QLabel *label;
    QLabel *label_2;
    QMenuBar *menuBar;
    QMenu *menuMenu;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(870, 560);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(870, 560));
        MainWindow->setMaximumSize(QSize(870, 560));
        QIcon icon;
        icon.addFile(QStringLiteral("generator.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        actionSettings = new QAction(MainWindow);
        actionSettings->setObjectName(QStringLiteral("actionSettings"));
        actionInfer_grammars_from_corpora = new QAction(MainWindow);
        actionInfer_grammars_from_corpora->setObjectName(QStringLiteral("actionInfer_grammars_from_corpora"));
        actionSave_to_MIDI = new QAction(MainWindow);
        actionSave_to_MIDI->setObjectName(QStringLiteral("actionSave_to_MIDI"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        generateBtn = new QPushButton(centralWidget);
        generateBtn->setObjectName(QStringLiteral("generateBtn"));
        generateBtn->setGeometry(QRect(340, 10, 211, 41));
        QFont font;
        font.setPointSize(11);
        font.setBold(true);
        font.setWeight(75);
        font.setKerning(true);
        generateBtn->setFont(font);
        generateBtn->setFlat(false);
        chordsView = new QListView(centralWidget);
        chordsView->setObjectName(QStringLiteral("chordsView"));
        chordsView->setGeometry(QRect(30, 80, 811, 421));
        chordsView->setFocusPolicy(Qt::NoFocus);
        chordsView->setStyleSheet(QLatin1String("QListView{\n"
"	padding: 0;\n"
"}\n"
"QListView::item {\n"
"    border: 1px solid black;\n"
"	font: 20pt bold \"Arial Black\";\n"
"	width: 108px;\n"
"    padding: 10px;\n"
"}\n"
"QListView::item::selected{\n"
"	color: black;\n"
"	background-color: #ddf;\n"
"}"));
        chordsView->setFrameShape(QFrame::StyledPanel);
        chordsView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        chordsView->setAlternatingRowColors(false);
        chordsView->setFlow(QListView::LeftToRight);
        chordsView->setProperty("isWrapping", QVariant(true));
        keyComboBox = new QComboBox(centralWidget);
        keyComboBox->setObjectName(QStringLiteral("keyComboBox"));
        keyComboBox->setGeometry(QRect(30, 30, 71, 22));
        labelKey = new QLabel(centralWidget);
        labelKey->setObjectName(QStringLiteral("labelKey"));
        labelKey->setGeometry(QRect(30, 10, 47, 13));
        labelGenre = new QLabel(centralWidget);
        labelGenre->setObjectName(QStringLiteral("labelGenre"));
        labelGenre->setGeometry(QRect(130, 10, 47, 13));
        genreComboBox = new QComboBox(centralWidget);
        genreComboBox->setObjectName(QStringLiteral("genreComboBox"));
        genreComboBox->setGeometry(QRect(130, 30, 81, 22));
        labelTempo = new QLabel(centralWidget);
        labelTempo->setObjectName(QStringLiteral("labelTempo"));
        labelTempo->setGeometry(QRect(240, 10, 47, 13));
        tempoSpinBox = new QSpinBox(centralWidget);
        tempoSpinBox->setObjectName(QStringLiteral("tempoSpinBox"));
        tempoSpinBox->setGeometry(QRect(240, 30, 61, 22));
        tempoSpinBox->setMaximum(300);
        tempoSpinBox->setValue(170);
        playBtn = new QPushButton(centralWidget);
        playBtn->setObjectName(QStringLiteral("playBtn"));
        playBtn->setGeometry(QRect(580, 10, 81, 41));
        QFont font1;
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setWeight(75);
        playBtn->setFont(font1);
        QIcon icon1;
        icon1.addFile(QStringLiteral("play.png"), QSize(), QIcon::Normal, QIcon::Off);
        playBtn->setIcon(icon1);
        repeatSpinBox = new QSpinBox(centralWidget);
        repeatSpinBox->setObjectName(QStringLiteral("repeatSpinBox"));
        repeatSpinBox->setGeometry(QRect(730, 20, 51, 22));
        repeatSpinBox->setMinimum(1);
        repeatSpinBox->setMaximum(300);
        repeatSpinBox->setValue(1);
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(680, 20, 47, 21));
        QFont font2;
        font2.setPointSize(10);
        label->setFont(font2);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(790, 20, 47, 21));
        label_2->setFont(font2);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 870, 21));
        menuMenu = new QMenu(menuBar);
        menuMenu->setObjectName(QStringLiteral("menuMenu"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        mainToolBar->setMovable(false);
        mainToolBar->setFloatable(false);
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuMenu->menuAction());
        menuMenu->addAction(actionInfer_grammars_from_corpora);
        menuMenu->addAction(actionSave_to_MIDI);
        menuMenu->addAction(actionSettings);

        retranslateUi(MainWindow);

        keyComboBox->setCurrentIndex(3);
        genreComboBox->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Backing track generator", 0));
        actionSettings->setText(QApplication::translate("MainWindow", "Settings...", 0));
        actionInfer_grammars_from_corpora->setText(QApplication::translate("MainWindow", "Infer grammars", 0));
        actionSave_to_MIDI->setText(QApplication::translate("MainWindow", "Save to MIDI", 0));
        generateBtn->setText(QApplication::translate("MainWindow", "Generate backing-track", 0));
        keyComboBox->clear();
        keyComboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "A", 0)
         << QApplication::translate("MainWindow", "A#/Bb", 0)
         << QApplication::translate("MainWindow", "B", 0)
         << QApplication::translate("MainWindow", "C", 0)
         << QApplication::translate("MainWindow", "C#/Db", 0)
         << QApplication::translate("MainWindow", "D", 0)
         << QApplication::translate("MainWindow", "D#/Eb", 0)
         << QApplication::translate("MainWindow", "E", 0)
         << QApplication::translate("MainWindow", "F", 0)
         << QApplication::translate("MainWindow", "F#/Gb", 0)
         << QApplication::translate("MainWindow", "G", 0)
         << QApplication::translate("MainWindow", "G#/Ab", 0)
        );
        labelKey->setText(QApplication::translate("MainWindow", "Key", 0));
        labelGenre->setText(QApplication::translate("MainWindow", "Genre", 0));
        genreComboBox->clear();
        genreComboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "rock", 0)
         << QApplication::translate("MainWindow", "swing", 0)
         << QApplication::translate("MainWindow", "blues", 0)
        );
        labelTempo->setText(QApplication::translate("MainWindow", "Tempo", 0));
        playBtn->setText(QApplication::translate("MainWindow", "Play", 0));
        label->setText(QApplication::translate("MainWindow", "Repeat", 0));
        label_2->setText(QApplication::translate("MainWindow", "times", 0));
        menuMenu->setTitle(QApplication::translate("MainWindow", "Menu", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
